from django.shortcuts import render

# Create your views here.


def ventas_v(request):
    modulo = request.GET.get('module','')
    print(modulo)
    request.session['modulo'] = modulo
    return render(request, "ventas/ventas_v.html")
