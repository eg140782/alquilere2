var cuotas_generadas = false;
var oc = [];
var tabla = null;

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}



$(document).ready(function() {
    const edit = $('#edit').val();

    $('.desa').each(function() {
        $( this ).prop( "disabled", true );
    });
  

    $('.cambio').autoNumeric('init', {
        mDec: '0',
        aSep: '.',
        aDec: ','
    });
    $('.auto').autoNumeric('init', {
        mDec: '2',
        aSep: '.',
        aDec: ','
    });

    $('.totales').autoNumeric('init', {
        mDec: '2',
        aSep: '.',
        aDec: ','
    });
    if (edit != "1") {
        $('.totales').each(function () {
            $(this).val(0);
        });
    } else {
        console.log("entra")
        var total = 0;
        $('.monto_cuota').each(function() {
          console.log("asd");
          var actual = parseFloat($( this ).val().split('.').join('').split(',').join('.'));
          if (isNaN(actual) == true)
            actual = 0;
          total += actual;
        });
        var a_total = total.toString().split('.')
        if (a_total.length > 1)
            $("#to_cuotas").val(((parseInt(a_total[0])).toLocaleString('en')).toString().split(',').join('.') + "," + a_total[1]);
        else
            $("#to_cuotas").val(((parseInt(a_total[0])).toLocaleString('en')).toString().split(',').join('.'));
    }
    $('#id_main-factura_fisica').inputmask("999-999-9999999");  //static mask
    $('#id_main-busca_oc').inputmask("9999/9999");  //static mask

    tabla = $('#tabla').DataTable( {


        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "No se encontraron registros - disculpe",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "decimal":        "",
            "emptyTable":     "No hay registros disponibles en la tabla",
            "infoFiltered":   "(Filtrando de _MAX_ registros)",
            "infoPostFix":    "",
            "thousands":      ",",
            "loadingRecords": "Cargando...",
            "processing":     "Procesando...",
            "search":         "Buscar:",
            "paginate": {
                "first":      "Primero",
                "last":       "Último",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "aria": {
                "sortAscending":  ": Ordenar en forma ascendente",
                "sortDescending": ": Ordenar en forma descendente"
            }
        },
        "searching": false,
        "info": false,
        "paging": false,
        columnDefs:[{
            targets: "_all",
            sortable: false
        }],
    });



    $("#id_main-busca_oc").enterKey(function() {
        console.log("hola mundo");
        $('#mensaje_buscador').html('');
        var proveedor = $('#id_main-proveedor').val();

        if ($(this).val() != "" && $(this).val() != null){
            if (proveedor != null){
                console.log(proveedor);
                console.log($(this).val());
                console.log(oc);
                if (jQuery.inArray( $(this).val(), oc ) != -1){
                    alert("Ya se encuentra cargada dicha O.C.");
                    return;
                }
                var mymodal = $('#myModal1');
                mymodal.find('.modal-body').html("Por favor aguarde..");
                // mymodal.modal('show');
                // $('#myModal1').modal('show');
                var data = {
                    oc_nro: $(this).val(),
                    proveedor: proveedor,
                };
                var $crf_token = readCookie('csrftoken');
                $.ajax({
                    method: 'POST',
                    url: '/App/compras/facturas/proveedor/busca/OC',
                    headers: {"X-CSRFToken": $crf_token, "Content-Type": "application/json; charset=UTF-8"},
                    contentType: 'application/json',
                    data: JSON.stringify(data),
                    processData: false,
                }).done(
                    function (data){
                        console.log("data", data)
                        // if (data.message == "OK"){
                        //     // mymodal.hide();
                        //     // mymodal.modal('hide');
                        //     // $('.modal-body').modal('hide');
                        //     // $('#myModal1').modal('hide');
                        //     if (data.codigo == 0) {
                        //         $('#mensaje_buscador').html('<div class="alert alert-danger alert-dismissible fade in" role="alert">\n' +
                        //             data.mensaje + '</div>');
                        //     }else if (data.codigo == 1){
                        //         oc.push(data.data[0].orden_nombre);
                        //         for (var x = 0; x < data.data.length; x++){
                        //             if (data.data[x].cantidad != "0.000"){
                        //                 $('#t_conceptos').find('.add-row').click();
                        //                 $('.delete-row').hide();
                        //                 var fila = $('#id_conceptos-TOTAL_FORMS').val();
                        //                 $("[name='conceptos-" + (fila - 1).toString() + "-orden']").val(data.data[x].orden);
                        //                 $("[name='conceptos-" + (fila - 1).toString() + "-orden_nombre']").val(data.data[x].orden_nombre);
                        //                 $("[name='conceptos-" + (fila - 1).toString() + "-item']").val(data.data[x].item_numero);
                        //                 $("[name='conceptos-" + (fila - 1).toString() + "-item_nombre']").val(data.data[x].item_nombre);
                        //                 $("[name='conceptos-" + (fila - 1).toString() + "-monto']").val(data.data[x].precio_unitario);
                        //                 $("[name='conceptos-" + (fila - 1).toString() + "-cantidad_maxima']").val(data.data[x].cantidad);
                        //                 var cantidad = data.data[x].cantidad.toString().split('.')
                        //                 if (cantidad.length > 1) {
                        //                     $("[name='conceptos-" + (fila - 1).toString() + "-cantidad']").val((
                        //                         (parseInt(cantidad[0])).toLocaleString('en')
                        //                     ).toString().split(',').join('.') + "," + cantidad[1]);
                        //                     $("[name='conceptos-" + (fila - 1).toString() + "-cantidad_maxima']").val((
                        //                         (parseInt(cantidad[0])).toLocaleString('en')
                        //                     ).toString().split(',').join('.') + "," + cantidad[1]);
                        //                 } else {
                        //                     $("[name='conceptos-" + (fila - 1).toString() + "-cantidad']").val((
                        //                         (parseInt(cantidad[0])).toLocaleString('en')
                        //                     ).toString().split(',').join('.'));
                        //                     $("[name='conceptos-" + (fila - 1).toString() + "-cantidad_maxima']").val((
                        //                         (parseInt(cantidad[0])).toLocaleString('en')
                        //                     ).toString().split(',').join('.'));
                        //                 }
                        //                 var monto_unitario = data.data[x].precio_unitario.toString().split('.')
                        //                 console.log(data.data[x].precio_unitario);
                        //                 console.log(monto_unitario);
                        //                 if (monto_unitario.length > 1)
                        //                     $("[name='conceptos-" + (fila - 1).toString() + "-monto_unitario']").val((
                        //                         (parseInt(monto_unitario[0])).toLocaleString('en')
                        //                     ).toString().split(',').join('.') + "," + monto_unitario[1]);
                        //                 else
                        //                     $("[name='conceptos-" + (fila - 1).toString() + "-monto_unitario']").val((
                        //                         (parseInt(monto_unitario[0])).toLocaleString('en')
                        //                     ).toString().split(',').join('.'));

                        //                 $('.auto').autoNumeric('destroy');
                        //                 $('.auto').autoNumeric('init', {
                        //                     mDec: '2',
                        //                     aSep: '.',
                        //                     aDec: ','
                        //                 });
                        //                 var campo = $("[name='conceptos-" + (fila - 1).toString() + "-monto_unitario']")
                        //                 subtotal(campo[0]);
                        //                 sumar_totales();

                        //             }
                        //         }
                        //     }
                        // }else{
                        //     mymodal.find('.modal-body').html("Se ha producido un error, verifique los valores ingresados y posteriormente informar el departamento de informática");
                        //     mymodal.modal('show');

                        // }
                    });
            }else {
                alert("No se ha seleccionado proveedor o sede");
                return;
            }

        }
    });




});

$.fn.enterKey = function (fnc) {
    return this.each(function () {
        $(this).keypress(function (ev) {
            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            if (keycode == '13') {
                fnc.call(this, ev);
            }
        })
    })
}



var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

function sumar_totales(){
    var total = 0;
    $('.total').each(function() {
      var actual = parseFloat($( this ).val().split('.').join('').split(',').join('.'));
      if (isNaN(actual) == true)
        actual = 0;
      total += actual;
    });
    var a_total = total.toFixed(2).toString().split('.')
    if (a_total.length > 1)
        $('#id_main-saldo_fake').val(((parseInt(a_total[0])).toLocaleString('en')).toString().split(',').join('.')+","+a_total[1]);
    else
        $('#id_main-saldo_fake').val(((parseInt(a_total[0])).toLocaleString('en')).toString().split(',').join('.'));
}
function impuesto(a){
    console.log("holaaaaaa", a.value)
    if (a.value != null || a.value != ""){
        var total = parseFloat($("[name='conceptos-" + a.name.split('-')[1] + "-monto']").val().split('.').join("").split(',').join('.'));
        var resultado = null;
        if (a.value == 0)
            resultado = 0;
        else if (a.value == 5)
            resultado = (total/parseFloat(21)).toFixed(2);
        else if (a.value == 10)
            resultado = (total/parseFloat(11)).toFixed(2);
        if (resultado != null) {
            var a_total = resultado.toString().split('.')
            if (a_total.length > 1)
                $("[name='conceptos-" + a.name.split('-')[1] + "-monto_impuesto']").val(((parseInt(a_total[0])).toLocaleString('en')).toString().split(',').join('.') + "," + a_total[1]);
            else
                $("[name='conceptos-" + a.name.split('-')[1] + "-monto_impuesto']").val(((parseInt(a_total[0])).toLocaleString('en')).toString().split(',').join('.'));
        }
    }
    var total_5 = 0.000;
    var impuesto_5 = 0;
    var gravada_5 = 0;
    var total_10 = 0.000;
    var impuesto_10 = 0;
    var gravada_10 = 0;
    var total_0 = 0.000;
    $('.impuestos').each(function() {
        console.log("ggggg")
        var valor = $('#'+$(this)[0].id).find(":selected").text();
        if ($(this).val() == "5"){
            total_5 = parseFloat(parseFloat(total_5)+parseFloat($(this).parent().prev().children(0).val().split('.').join('').split(',').join('.'))).toFixed(2);
        } else if ($(this).val() == "10"){
            total_10 = parseFloat(parseFloat(total_10)+parseFloat($(this).parent().prev().children(0).val().split('.').join('').split(',').join('.'))).toFixed(2);
        } else if ($(this).val() == "0") {
            total_0 = parseFloat(parseFloat(total_0)+parseFloat($(this).parent().prev().children(0).val().split('.').join('').split(',').join('.'))).toFixed(2);
        }
    });
    impuesto_5 = (total_5/parseFloat(21)).toFixed(2);
    gravada_5 = (total_5-impuesto_5).toFixed(2)
    impuesto_10 = (total_10/parseFloat(11)).toFixed(2);
    gravada_10 = (total_10-impuesto_10).toFixed(2)
    var a_total_5 = total_5.toString().split('.')
    if (a_total_5.length > 1)
        $("#id_main-monto_total5").val(((parseInt(a_total_5[0])).toLocaleString('en')).toString().split(',').join('.') + "," + a_total_5[1]);
    else
        $("#id_main-monto_total5").val(((parseInt(a_total_5[0])).toLocaleString('en')).toString().split(',').join('.'));
    var a_impuesto_5 = impuesto_5.toString().split('.')
    if (a_impuesto_5.length > 1)
        $("#id_main-monto_impuesto5").val(((parseInt(a_impuesto_5[0])).toLocaleString('en')).toString().split(',').join('.') + "," + a_impuesto_5[1]);
    else
        $("#id_main-monto_impuesto5").val(((parseInt(a_impuesto_5[0])).toLocaleString('en')).toString().split(',').join('.'));
    var a_gravada_5 = gravada_5.toString().split('.')
    if (a_gravada_5.length > 1)
        $("#id_main-monto_gravado5").val(((parseInt(a_gravada_5[0])).toLocaleString('en')).toString().split(',').join('.') + "," + a_gravada_5[1]);
    else
        $("#id_main-monto_gravado5").val(((parseInt(a_gravada_5[0])).toLocaleString('en')).toString().split(',').join('.'));
    var a_total_10 = total_10.toString().split('.')
    if (a_total_10.length > 1)
        $("#id_main-monto_total10").val(((parseInt(a_total_10[0])).toLocaleString('en')).toString().split(',').join('.') + "," + a_total_10[1]);
    else
        $("#id_main-monto_total10").val(((parseInt(a_total_10[0])).toLocaleString('en')).toString().split(',').join('.'));
    var a_impuesto_10 = impuesto_10.toString().split('.')
    if (a_impuesto_10.length > 1)
        $("#id_main-monto_impuesto").val(((parseInt(a_impuesto_10[0])).toLocaleString('en')).toString().split(',').join('.') + "," + a_impuesto_10[1]);
    else
        $("#id_main-monto_impuesto").val(((parseInt(a_impuesto_10[0])).toLocaleString('en')).toString().split(',').join('.'));
    var a_gravada_10 = gravada_10.toString().split('.')
    if (a_gravada_10.length > 1)
        $("#id_main-monto_gravado").val(((parseInt(a_gravada_10[0])).toLocaleString('en')).toString().split(',').join('.') + "," + a_gravada_10[1]);
    else
        $("#id_main-monto_gravado").val(((parseInt(a_gravada_10[0])).toLocaleString('en')).toString().split(',').join('.'));
    var a_total_0 = total_0.toString().split('.')
    if (a_total_0.length > 1)
        $("#id_main-monto_exento").val(((parseInt(a_total_0[0])).toLocaleString('en')).toString().split(',').join('.') + "," + a_total_0[1]);
    else
        $("#id_main-monto_exento").val(((parseInt(a_total_0[0])).toLocaleString('en')).toString().split(',').join('.'));
    sumar_totales();
}

function subtotal(a){
    console.log("subtotal: ", a)
    var a_name = a.name.split('-');

    var nombre_campo = a_name[a_name.length-1];
    if (nombre_campo == "monto_unitario"){
        var costo = $('#'+a.id).val().split('.').join("").split(',').join('.');
        var cantidad = $('#'+a.id).parent().next().children(0).val().split('.').join("").split(',').join('.');
        if (cantidad != "" && costo != ""){
            var total = parseFloat(parseFloat(cantidad).toFixed(3)*parseFloat(costo).toFixed(3)).toFixed(3);
            var a_total = total.toString().split('.')
            if (a_total.length > 1)
                $('#'+a.id).parent().next().next().next().next().children(0).val(((parseInt(a_total[0])).toLocaleString('en')).toString().split(',').join('.')+","+a_total[1]);
            else
                $('#'+a.id).parent().next().next().next().next().children(0).val(((parseInt(a_total[0])).toLocaleString('en')).toString().split(',').join('.'));
        }
    } else {
        var cantidad_maxima = $('#'+a.id).next().val().split('.').join("").split(',').join('.');
        var cantidad = $('#'+a.id).val().split('.').join("").split(',').join('.');
        var costo = $('#'+a.id).parent().prev().children(0).val().split('.').join("").split(',').join('.');
        if (parseFloat(cantidad_maxima) < parseFloat(cantidad)){
            var a_cantidad = cantidad_maxima.toString().split('.')
            if (a_cantidad.length > 1)
                $('#'+a.id).val(((parseInt(a_cantidad[0])).toLocaleString('en')).toString().split(',').join('.')+","+a_cantidad[1]);
            else
                $('#'+a.id).val(((parseInt(a_cantidad[0])).toLocaleString('en')).toString().split(',').join('.'));
            alert("El valor ingresado supera la cantidad maxima "+cantidad_maxima);
            return;
        }
        if (cantidad != "" && costo != ""){
            var total = parseFloat(parseFloat(cantidad).toFixed(3)*parseFloat(costo).toFixed(3)).toFixed(3);
            var a_total = total.toString().split('.')
            if (a_total.length > 1)
                $('#'+a.id).parent().next().next().next().children(0).val(((parseInt(a_total[0])).toLocaleString('en')).toString().split(',').join('.')+","+a_total[1]);
            else
                $('#'+a.id).parent().next().next().next().children(0).val(((parseInt(a_total[0])).toLocaleString('en')).toString().split(',').join('.'));
        }
    }
    impuesto($('#'+a.id).parent().next().next().next().next().children(0)[0]);
}





$(document).bind('change', function (e) {
    var names = e.target.name.split('-');
    console.log(names);
    if (names[1] == "proveedor") {
        proveedor = parseInt(e.target.value);
        console.log("profgdfgfdgdfg:_ ", proveedor)
        $.ajax({
            method: 'GET',
            url: '/App/compras/facturas/ordenes/' + proveedor+'/' ,
            headers: {"Content-Type": "application/json; charset=UTF-8"},
            contentType: 'application/json',
            processData: false,
        }).done(
            function (data){
                tabla.clear().draw();
                // <input id="'+data.data[a].orden+'" value="'+data.data[a].orden+'" class="agregar form-control" type="checkbox" onclick="Item_nuevo(val=\''+data.data[a].orden+'\',this)">
                for (var a = 0; a<data.data.length; a++){
                    tabla.row.add( [
                        '<td> <button type="button" class="guardar btn btn-rounded btn-info mb-3" onclick="Item_nuevo(val=\''+data.data[a].orden +"-"+data.data[a].periodo+'\')" style="color: white;" id="agregar-'+data.data[a].orden+'">Agregar Orden '+data.data[a].orden+'</button></td >',
                       
                        '<td>'+ data.data[a].orden +'/'+ data.data[a].periodo + '</td> ',
                        '<td>'+ data.data[a].fecha + '</td> ',
                        '<td>'+ data.data[a].total + '</td> ',

                    ] ).draw( false );

                }
            }
            
        )
    }else if (names[2] == "porcentaje_impuesto") {
        impuesto(e.target);
   }
})

function Item_nuevo(val){
    console.log(val);
    var orden = val.split('-');
    console.log(orden[0]);

  /*   var button = document.querySelector(".guardar");
    console.log(button);
    button.disabled = true; */
    // console.log()
    var mymodal = $('#myModal1');
    mymodal.find('.modal-body').html("Por favor aguarde..");
    $( ".guardar" ).prop( "disabled", true );
    var data = {
        oc_nro: orden[0],
        periodo: orden[1],
        proveedor: proveedor,
    };
    var $crf_token = readCookie('csrftoken');
    $.ajax({
        method: 'POST',
        url: '/App/compras/facturas/proveedor/busca/OC',
        headers: {"X-CSRFToken": $crf_token, "Content-Type": "application/json; charset=UTF-8"},
        contentType: 'application/json',
        data: JSON.stringify(data),
        processData: false,

        }).done(
            function (data){
                
                console.log("orden detalle:_ ", data)
                if (data.message == "OK"){

                    if (data.codigo == 0) {
                        $('#mensaje_buscador').html('<div class="alert alert-danger alert-dismissible fade in" role="alert">\n' +
                            data.mensaje + '</div>');
                    }else if (data.codigo == 1){
                        console.log("orden detalle:_ ", data)
                        oc.push(data.data[0].orden_nombre);
                        for (var x = 0; x < data.data.length; x++){
                            if (data.data[x].cantidad != "0.000"){
                                console.log("orden detalle:_ ", data)
                                $('#t_conceptos_nuevo').find('.add-row').click();
                                $('.delete-row').hide();
                                var fila = $('#id_conceptos-TOTAL_FORMS').val();
                                $("[name='conceptos-" + (fila - 1).toString() + "-orden']").val(data.data[x].orden);
                                $("[name='conceptos-" + (fila - 1).toString() + "-orden_nombre']").val(data.data[x].orden_nombre);
                                $("[name='conceptos-" + (fila - 1).toString() + "-item_producto']").val(data.data[x].item_numero);
                                $("[name='conceptos-" + (fila - 1).toString() + "-item_nombre']").val(data.data[x].item_nombre);
                                $("[name='conceptos-" + (fila - 1).toString() + "-monto_unitario']").val(data.data[x].precio_unitario);
                                $("[name='conceptos-" + (fila - 1).toString() + "-cantidad']").val(data.data[x].cantidad);
                                $("[name='conceptos-" + (fila - 1).toString() + "-monto']").val(data.data[x].precio_unitario);

                                var cantidad = data.data[x].cantidad.toString().split('.')
                                if (cantidad.length > 1) {
                                    $("[name='conceptos-" + (fila - 1).toString() + "-cantidad']").val((
                                        (parseInt(cantidad[0])).toLocaleString('en')
                                    ).toString().split(',').join('.') + "," + cantidad[1]);
                                    $("[name='conceptos-" + (fila - 1).toString() + "-cantidad_maxima']").val((
                                        (parseInt(cantidad[0])).toLocaleString('en')
                                    ).toString().split(',').join('.') + "," + cantidad[1]);
                                } else {
                                    $("[name='conceptos-" + (fila - 1).toString() + "-cantidad']").val((
                                        (parseInt(cantidad[0])).toLocaleString('en')
                                    ).toString().split(',').join('.'));
                                    $("[name='conceptos-" + (fila - 1).toString() + "-cantidad_maxima']").val((
                                        (parseInt(cantidad[0])).toLocaleString('en')
                                    ).toString().split(',').join('.'));
                                }
                                var monto_unitario = data.data[x].precio_unitario.toString().split('.')
                                console.log(data.data[x].precio_unitario);
                                console.log(monto_unitario);
                                if (monto_unitario.length > 1)
                                    $("[name='conceptos-" + (fila - 1).toString() + "-monto_unitario']").val((
                                        (parseInt(monto_unitario[0])).toLocaleString('en')
                                    ).toString().split(',').join('.') + "," + monto_unitario[1]);
                                else
                                    $("[name='conceptos-" + (fila - 1).toString() + "-monto_unitario']").val((
                                        (parseInt(monto_unitario[0])).toLocaleString('en')
                                    ).toString().split(',').join('.'));

                                $('.auto').autoNumeric('destroy');
                                $('.auto').autoNumeric('init', {
                                    mDec: '2',
                                    aSep: '.',
                                    aDec: ','
                                });
                                var campo = $("[name='conceptos-" + (fila - 1).toString() + "-monto_unitario']")
                                subtotal(campo[0]);    
                                sumar_totales();
                                $("#limpiar").prop( "disabled", false );
                            }
                        }
                    }
                }else{
                    mymodal.find('.modal-body').html("Se ha producido un error, verifique los valores ingresados y posteriormente informar el departamento de informática");
                    mymodal.modal('show');

                }
                
            }
        )


}

function limpiar_detalles(){
    console.log("hiiiii")
    $("#limpiar").prop( "disabled", true );
    $('#t_conceptos_nuevo').find('.delete-row').click();
    $('#t_cuotas').find('.delete-row').click();
    $(".guardar").prop( "disabled", false );
    $("#id_main-saldo_fake").val(0);
    $('.totales').val(0);
    $('#to_cuotas').val(0);
    cuotas_generadas = false
}


function nro_timbradoadd(e){
    var proveedor = $("#id_main-proveedor").val();
    var timbrado = $(e).val();
    console.log(proveedor, $(e).val())
    if (proveedor != "" || nro != "" ){
        if (timbrado != ""){
            $.ajax({
                method: 'GET',
                url: '/compras/facturas/fecha_timbrado/'+ timbrado + '/' + proveedor,
                headers: {"Content-Type": "application/json; charset=UTF-8"},
                contentType: 'application/json',
                processData: false,
                contentType: false
            }).done(
                function (data) {
                    console.log(data.fecha)
                    $('#id_main-nrotimbrado_vencimiento').val(data.fecha);
                });
        }else{
            alert("Debe ingresar el numero de timbrado.")
            return;
        }
    }else{
        alert("Debe seleccionar el Proveedor")
        return;
    }
    
}


function gen_cuotas(){
    if (cuotas_generadas == true){
        alert("Ya se han generado las cuotas.");
        return;
    }
    var monto = $('#id_main-saldo_fake').val().split('.').join('').split(',').join('.');
    if (monto == '' || monto == null || monto == "0"){
        alert("La factura no tiene cargado items, para calcular las cuotas.");
        return;
    }
    var cuotas = $('#id_main-cantidad_cuotas').val()
    if (cuotas == '' || cuotas == null){
        alert("No se ingreso la cantidad de cuotas.");
        return;
    }
    var dias = $('#id_main-dias_entre_cuotas').val();
    console.log(dias);
    if (dias == '' || dias == null){
        alert("No se ingreso la cantidad de dias entre cada cuota.");
        return;
    }
    $('#to_cuotas').val($('#id_main-saldo_fake').val());
    var monto_cuota = parseFloat(parseFloat(monto)/parseFloat(cuotas)).toFixed(2);
    var fecha = new Date($('#id_main-fecha').val());
    console.log("fecha hoy: ", fecha);
    for (var a = 1; a <= parseInt(cuotas); a++){
        $('#t_cuotas').find('.add-row').click();
        //$("#t_cuotas td").css("position","relative");
        $('.delete-row').hide();
        var fila = $('#id_cuotas-TOTAL_FORMS').val();
        $("[name='cuotas-" + (fila - 1).toString() + "-cuota']").val(a);
        var cdates = new Date();
        cdates.setDate(cdates.getDate() + a * parseInt(dias));
        var fecha_nuevas = moment(cdates).format('YYYY-MM-DD');
        console.log(fecha_nuevas);

        var a_monto_cuota = monto_cuota.toString().split('.')
        console.log("a_monto_cuota: ", a_monto_cuota)
        if (a_monto_cuota.length > 1){
            $("[name='cuotas-" + (fila - 1).toString() + "-monto']").val(((parseInt(a_monto_cuota[0])).toLocaleString('en')).toString().split(',').join('.')+","+a_monto_cuota[1]);
            $("[name='cuotas-" + (fila - 1).toString() + "-saldo']").val(((parseInt(a_monto_cuota[0])).toLocaleString('en')).toString().split(',').join('.')+","+a_monto_cuota[1]);
        } else{
            $("[name='cuotas-" + (fila - 1).toString() + "-monto']").val(((parseInt(a_monto_cuota[0])).toLocaleString('en')).toString().split(',').join('.'));
            $("[name='cuotas-" + (fila - 1).toString() + "-saldo']").val(((parseInt(a_monto_cuota[0])).toLocaleString('en')).toString().split(',').join('.'));
        }
        $('.auto').autoNumeric('destroy');
        $('.auto').autoNumeric('init', {
            mDec: '2',
            aSep: '.',
            aDec: ','
        });
        cuotas_generadas = true;
        $("[name='cuotas-" + (fila - 1).toString() + "-fecha_vencimiento']").val(fecha_nuevas);

    }
}


function factor_cambio(a){
    if (a == "GS")
        $('#id_main-factor_cambio').val("1");
    
}


function saldo(a){
    $("[name='cuotas-" + a.name.split('-')[1] + "-saldo']").val(a.value);
    var total = 0;
    $('.monto_cuota').each(function() {
      var actual = parseFloat($( this ).val().split('.').join('').split(',').join('.'));
      if (isNaN(actual) == true)
        actual = 0;
      total += actual;
    });
    var a_total = total.toString().split('.')
    if (a_total.length > 1)
        $("#to_cuotas").val(((parseInt(a_total[0])).toLocaleString('en')).toString().split(',').join('.') + "," + a_total[1]);
    else
        $("#to_cuotas").val(((parseInt(a_total[0])).toLocaleString('en')).toString().split(',').join('.'));
}


function guardar(){
    if ($('#id_main-fecha').val() == "") {
        alert("No se ingresó una fecha válida.");
        return;
    }
    if ($('#id_main-empresa').find(":selected").text() == "---------") {
        alert("No se seleccionó el tipo de empresa.");
        return;
    }
    if ($('#id_main-moneda').find(":selected").text() == "---------") {
        alert("No se seleccionó la moneda de la factura.");
        return;
    } else {
        if ($('#id_main-factor_cambio').val() == ""){
            alert("No se ingresó el factor de cambios.");
            return;
        }
    }
    if ($('#id_main-forma_pago').find(":selected").text() == "---------") {
        alert("No se seleccionó forma de pago.");
        return;
    }
    if ($('#id_main-tipo_de_factura').find(":selected").text() == "---------") {
        alert("No se seleccionó tipo de factura.");
        return;
    }
    if ($('#id_main-factura_fisica').val() == ""){
        alert("No se ingresó el número de la factura.");
        return;
    }
    if ($('#id_main-nrotimbrado').val() == ""){
        alert("No se ingresó el timbrado de la factura.");
        return;
    }
    if ($('#id_main-nrotimbrado_vencimiento').val() == ""){
        alert("No se ingresó la fecha del timbrado de la factura.");
        return;
    }
    //comprobar que el monto de las cuotas coincida con el monto total
    //comprobar que tenga cuotas
    //comprobar que tenga detalles en conceptos
    //comprobar que los detalles tengan impuesto
    var detalles = $('.inline.conceptos').length
    var cuotas = $('.inline.cuotas').length
    if (detalles == 0){
        alert("No se han cargado detalles a la factura.");
        return;
    }
    if (cuotas == 0){
        alert("No se han cargado cuotas a la factura.");
        return;
    }
    var total = 0;
    $('.monto_cuota').each(function() {
      var actual = parseFloat($( this ).val().split('.').join('').split(',').join('.'));
      if (isNaN(actual) == true)
        actual = 0;
      total += actual;
    });
    var total_saldo = parseFloat($('#id_main-saldo_fake').val().split('.').join('').split(',').join('.')).toFixed(1);
    console.log(total.toFixed(1));
    console.log(total_saldo);
    if (total.toFixed(1) != total_saldo){
        alert("La suma de los montos de las cuotas no coincide con el saldo.");
        return;
    }
    var impuesto = false;
    $('.impuestos').each(function() {
        if ($('#'+$(this)[0].id).find(":selected").text() == "---------" && $(this).parent().prev().children(0).val() != "0,00"){
            impuesto = true;
        }
    });
    if (impuesto == true){
        alert("Algunos detalles de la factura no tienen seleccionado el porcentaje de impuesto.");
        return;
    }
    $('.auto').each(function() {
        $(this).val($(this).val().split('.').join('').split(',').join('.'))
    });
    $('.cambio').each(function() {
        $(this).val($(this).val().split('.').join('').split(',').join('.'))
    });
    $('.totales').each(function() {
        $(this).val($(this).val().split('.').join('').split(',').join('.'))
    });
    $('.desa').each(function() {
      $( this ).prop( "disabled", false );
    });
    $('#formulario').submit();
}
