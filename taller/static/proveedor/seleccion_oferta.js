function verificar_check(a){
    $( '.'+a.className.split(' ')[0] ).each(function( index ) {
        if (a.id != $(this)[0].id){
            $('#'+$(this)[0].id).prop('checked', false);
        }
    });
}

function containsObject(obj, list) {
    var i;
    for (i = 0; i < list.length; i++) {
        if (list[i].presupuesto === obj.presupuesto) {
            return true;
        }
    }
    return false;
}

function returnObject(presupuesto,list) {
    var i;
    for (i = 0; i < list.length; i++) {
        if (list[i].presupuesto === presupuesto) {
            return i;
        }
    }
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function guardar(){

    var mymodal = $('#myModal');
        mymodal.find('.modal-body').html("Por favor aguarde..");
        mymodal.modal('show');

    var body = [];
    $( '.opcion' ).each(function( index ) {
        if ($( this )[0].checked == true) {
            var datos = $( this )[0].id.split('-');
            var presupuesto = datos[1];
            var detalle = datos[3];
            var c_data = {
                presupuesto: presupuesto,
                detalle: [],
            };
            c_data.detalle.push(detalle);
            if (containsObject(c_data,body) == true){
                var indice = returnObject(presupuesto,body);
                body[indice].detalle.push(detalle);
            } else {
                body.push(c_data);
            }
        }
    });
    if (body.length > 0) {
        var mymodal = $('#myModal');
        mymodal.find('.modal-body').html("Por favor aguarde..");
        mymodal.modal('show');

        var $crf_token = readCookie('csrftoken');
        $.ajax({
            method: 'POST',
            url: '/App/compras/solicitud/definicionec/',
            headers: {"X-CSRFToken": $crf_token, "Content-Type": "application/json; charset=UTF-8"},
            contentType: 'application/json',
            data: JSON.stringify(body),
            processData: false,
            contentType: false
        }).done(
            function (data) {
                console.log(data.message)
                if (data.message == "OK") {
                    mymodal.modal('hide');
                    window.location.href = '/App/compras/solicitud/listado/';
                } else {
                    mymodal.find('.modal-body').html("Oh se ha producido un error!, Comuniquese con el administrador");
                    mymodal.modal('show');
                }
            });
    } else {
        var mymodal = $('#myModal');
        mymodal.find('.modal-body').html("No se han seleccionado opciones..");
        mymodal.find('.modal-header').empty();
        mymodal.find('.modal-header').append("<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>")
        mymodal.modal('show');
    }
}
