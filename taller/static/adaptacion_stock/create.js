function add_items(){
    var operacion = $('#id_operacion').val();


    var ubicacion =  $('#id_ubicacion').val();

    var add = $("#detalle_tabla").find('.add-row');
    console.log(add.parent().children().eq(-2).children().eq(1).children().eq(0));


    add.parent().children().eq(-2).children().eq(0).children().eq(0).html("");

    if (ubicacion != ''){
        if (operacion != ''){
            var request = $.ajax({
                type: "GET",
                url: '/App/stock/adaptacion/verificacion/'+ubicacion+'/'+operacion,
                headers: {"Content-Type": "application/json; charset=UTF-8"},
                contentType: 'application/json',
                processData: false,
                data: {
                     "detalle": add.parent().children().eq(-2).children().eq(0).children().eq(0).children().eq(0),
                },
            });
            request.done(function(response){
                if (response.detalle){
                    if (operacion == "Alta"){
                        $('#column_id').hide();
                        $('.column_2').hide();

                        add.parent().children().eq(-2).children().eq(1).children().eq(0).hide();
                        add.parent().children().eq(-2).children().eq(0).children().eq(0).html(response.detalle);
                        add.parent().children().eq(-2).children().eq(0).children().eq(0).trigger("change");
                    }else{
                        $('#column_id').show();
                        $('.column_2').show();
                        add.parent().children().eq(-2).children().eq(1).children().eq(0).show();
                        add.parent().children().eq(-2).children().eq(0).children().eq(0).html(response.detalle);
                        add.parent().children().eq(-2).children().eq(0).children().eq(0).trigger("change");
                    }
                } else {

                    add.parent().children().eq(-2).children().eq(0).children().eq(0).html("<option value='' selected='selected'>---------</option>");
                    add.parent().children().eq(-2).children().eq(0).children().eq(0).trigger("change");
                }
            });
        }else{
            alert("Complete el campo de operacion");
            return;
        }

    }else{
        alert("Complete el campo de Deposito");
        return;
    }
};


function add_stock(){
    var ope = $('#id_operacion').val();

    var fila = $('#id_detalle-TOTAL_FORMS').val();

    var item = $("[name='detalle-" + (fila - 1).toString() + "-item']").val();


    var ubicacion =  $('#id_ubicacion').val();
        // console.log(deposito);
    if (ope == "Alta"){
        console.log("nou");
    }else{
        if (item != '' && ubicacion != ''){
            console.log(item);
            console.log(ubicacion);
            $.ajax({
            method: 'GET',
            url: '/App/stock/movimiento/item_catidad/' + ubicacion +'/'+ item,
            headers: {"Content-Type": "application/json; charset=UTF-8"},
            contentType: 'application/json',

            }).done(
                 function (data){
                     $("[name='detalle-" + (fila - 1).toString() + "-stock_dispobible']").val(data.data);
                 }

            );


        }
    }



}




function verificar_cantidad(e){
    var total_stock = 0;
    var total_cantidad = 0;
    var ope = $('#id_operacion').val();
    var seleccionado = e;
    console.log(seleccionado);

    $('.stock').each(function() {
        if ($(this).parent().parent().parent().parent().is(":visible") == true){
            var actual = parseFloat($( this ).val().split('.').join('').split(',').join('.'));
            if (isNaN(actual) == true) actual = 0;
            total_stock = actual;
        };
    });


    $('.cantidad').each(function() {
        if ($(this).parent().parent().parent().parent().is(":visible") == true){
            var actual = parseFloat($( this ).val().split('.').join('').split(',').join('.'));
            if (isNaN(actual) == true) actual = 0;
            total_cantidad = actual;
        };
    });
    var tstock = total_stock.toString().split('.');
    var tcantidad = total_cantidad.toString().split('.');
    if (ope == "Alta"){
        console.log("nou");

    }else{
        if (parseInt(tcantidad) > parseInt(tstock)){
            alert("Atención!! La cantidad introducida no  puede ser mayor a la cantidad del stock disponible.");
            $(e).val(0);
            return;

        }else{
            console.log("pasa");
        }
    }





}


function save(){
    $('.auto').each(function () {
        $(this).val($(this).val().split('.').join('').split(',').join('.'));
    });
    $('.cantidad').each(function () {
        $(this).val($(this).val().split('.').join('').split(',').join('.'));
    });

    $('.stock').each(function () {
        $(this).val($(this).val().split('.').join('').split(',').join('.'));
    });
    $('#formulario').submit();
};
