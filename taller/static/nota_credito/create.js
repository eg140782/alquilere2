
var tabla = null;



$(document).bind('change', function (e) {
    var names = e.target.name.split('-');
    var proveedor = '';
    console.log(names)
    if (names[1] == "proveedor"){
        console.log("hola");
        proveedor = e.target.value;
        console.log(proveedor);
        $.ajax({
            method: 'GET',
            url: '/App/compras/nota/credito/ordenes/' + proveedor+'/' ,
            headers: {"Content-Type": "application/json; charset=UTF-8"},
            contentType: 'application/json',
            processData: false,
        }).done(
            function (data){
                tabla.clear().draw();
                // <input id="'+data.data[a].orden+'" value="'+data.data[a].orden+'" class="agregar form-control" type="checkbox" onclick="Item_nuevo(val=\''+data.data[a].orden+'\',this)">
                for (var a = 0; a<data.data.length; a++){
                    tabla.row.add( [
                        '<td> <button type="button" class="guardar btn btn-rounded btn-danger mb-3" onclick="Item_nuevo(val=\''+data.data[a].orden+'\')" style="color: white;" id="agregar-'+data.data[a].orden+'">Agregar '+data.data[a].orden+'</button></td >',
                       
                        '<td>'+ data.data[a].orden + '</td> ',
                        '<td>'+ data.data[a].fecha + '</td> ',
                        '<td>'+ data.data[a].total + '</td> ',

                    ] ).draw( false );

                }
            }
        );

    }
});



function Item_nuevo(val){
    console.log(val);
  /*   var button = document.querySelector(".guardar");
    console.log(button);
    button.disabled = true; */
    console.log()
    $( ".guardar" ).prop( "disabled", true );

    $.ajax({
        method: 'GET',
        url: '/App/compras/nota/credito/ordenes/agregar/'+ val+'/' ,
        headers: {"Content-Type": "application/json; charset=UTF-8"},
        contentType: 'application/json',
        processData: false

        }).done(
            function (data){
                console.log(data)
                for (var a = 0; a<data.data.length; a++){
                    $('#detalle_tabla').find('.add-row').click();
                    var fila = $('#id_detalle-TOTAL_FORMS').val();
                    console.log(fila)
                    $("[name='detalle-" + (fila - 1).toString() + "-orden']").val(data.data[a].orden);
                    $("[name='detalle-" + (fila - 1).toString() + "-item']").val(data.data[a].item_detalles);
                    $("[name='detalle-" + (fila - 1).toString() + "-item_num']").val(data.data[a].item_num);
                    $("[name='detalle-" + (fila - 1).toString() + "-cantidad']").val(data.data[a].cantidad);
                    $("[name='detalle-" + (fila - 1).toString() + "-monto_unitario']").val(data.data[a].monto);
                    $("[name='detalle-" + (fila - 1).toString() + "-total']").val(data.data[a].total_item);
                    $('#detalle_tabla').find('.delete-row').hide();
                    $('#id_orden_num').val(data.data[a].orden);
                    $("[name='detalle-" + (fila - 1).toString() + "-orden']").prop( "disabled", true );
                    $("[name='detalle-" + (fila - 1).toString() + "-item']").prop( "disabled", true );
                    $("[name='detalle-" + (fila - 1).toString() + "-item_num']").prop( "disabled", true );
                    $("[name='detalle-" + (fila - 1).toString() + "-cantidad']").prop( "disabled", true );
                    $("[name='detalle-" + (fila - 1).toString() + "-monto_unitario']").prop( "disabled", true );
                    $("[name='detalle-" + (fila - 1).toString() + "-total']").prop( "disabled", true );
                    $("#limpiar").prop( "disabled", false );
                    calcular_total();
                }
                
            }
        )


}



function limpiar_detalles(){
    console.log("hiiiii")
    $("#limpiar").prop( "disabled", true );
    $('#detalle_tabla').find('.delete-row').click();
    $(".guardar").prop( "disabled", false );
    $("#id_mains-total").val(0);
}



function calcular_total(){
    var total = 0;


    $('.total').each(function() {
          if ($(this).parent().parent().parent().parent().is(":visible") == true){
            var actual = parseFloat($( this ).val().split('.').join('').split(',').join('.'));
            if (isNaN(actual) == true) actual = 0;
            total += actual;
        };
    });
    var a_total = total.toString().split('.');
    console.log(a_total);
    if (a_total.length > 1){
        $("#id_mains-total").val(((parseInt(a_total[0])).toLocaleString('en')).toString().split(',').join('.') + "," + a_total[1]);

    } else{
        $("#id_mains-total").val(((parseInt(a_total[0])).toLocaleString('en')).toString().split(',').join('.'));

    }



};


function save(){
    var proveedor = $("#id_mains-proveedor").val();
    var nota = $("#id_mains-numero_credito_fisico").val();
    if (proveedor != ''){

        if (nota != ''){

            $(".form-control").removeAttr('disabled');
            $('.auto').each(function () {
                $(this).val($(this).val().split('.').join('').split(',').join('.'));
            });

            $('#formulario').submit();


        }else{
            console.log("falta nota");
            alert("El campo numero credito fisico es requerido");
            return;
        }

    }else{
        console.log("falta proveedor");
        alert("El campo tipo proveedor es requerido");
        return;

    }

}
