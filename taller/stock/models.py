from django.db import models
from compras.models import *
# Create your models here.
from django.utils import timezone

class Ubicaciones(models.Model):
    nombre = models.CharField(max_length=500, null=True, blank=True, verbose_name="Ubicación")
    departamento = models.ForeignKey(Departamentos, verbose_name="Departamentos",  on_delete=models.PROTECT, null=True,
                                   blank=True)
    estado = models.CharField(max_length=1, default="A")
    creado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                   blank=True, related_name='cc')
    modificado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                       blank=True, related_name='rrrr')
    creado_el = models.DateTimeField(null=True, blank=True)
    modificado_el = models.DateTimeField(null=True, blank=True)
    # class Meta:
    #     managed = False
    #     db_table = 'core_depositos'

    def __str__(self):
        return str(self.nombre) 

class Recepcionproducto(models.Model):
    orden = models.ForeignKey(OrdenCompra, on_delete=models.PROTECT)
    numero = models.IntegerField()
    fecha = models.DateField()
    
    class Meta:
        managed = False
        db_table = 'v_recepcion'  

class Lote(models.Model):
    costo_item = models.DecimalField(max_digits=17, decimal_places=2, null=True, blank=True)
    fecha_lote = models.DateField(null=True, blank=True)
    fecha_vencimiento = models.DateField(null=True, blank=True)
    estado = models.CharField(max_length=20, null=True, blank=True)
    item = models.ForeignKey(Item, on_delete=models.PROTECT,null=True, blank=True)


class Stock(models.Model):
    ubicacion = models.ForeignKey(Ubicaciones, on_delete=models.PROTECT,null=True, blank=True)
    lote = models.ForeignKey(Lote, on_delete=models.PROTECT,null=True, blank=True)
    item = models.ForeignKey(Item, on_delete=models.PROTECT,null=True, blank=True, related_name="articulo")
    cantidad = models.DecimalField(max_digits=20, decimal_places=3)
    stock = models.IntegerField(null=True)
    operacion = models.CharField(max_length=50, null=True, blank=True)
    referencia = models.IntegerField(null=True)
    usuario = models.ForeignKey(User, on_delete=models.PROTECT,null=True, blank=True)
    fecha_insercion = models.DateTimeField(null=True, blank=True)

def trxStock(origen, item, cantidad, operacion, referencia, usuario, destino=None):
    print ("cantidad", type(cantidad))
    # ajuste baja, entrega de utiles, movimiento baja
    #if operacion == "NUEVO AJUSTE BAJA" or operacion == "NUEVA ENTREGA UTILES" or operacion == "NUEVO MOVIMIENTO BAJA":
    if operacion == "NUEVO MOVIMIENTO BAJA" or operacion == "SOLICITUD APROBADA BAJA":
        print ("Entro 1")
        cantidad = int(cantidad)
        while cantidad > 0:
            s = Stock.objects.filter(item=item, ubicacion=origen, stock__gt=0, lote__estado='A').order_by('lote_id','-fecha_insercion')
            print (s)
            if s:
                stock = s[0].stock
            else:
                stock = 0

            parcial = 0

            if cantidad > stock:
                parcial = stock
            else:
                parcial = cantidad

            xs = Stock.objects.filter(item=item, ubicacion=origen, lote__estado='A').order_by('-fecha_insercion')
            if xs:
                xstock = xs[0].stock
            else:
                xstock = 0
            print ("parcial: ", type(parcial))

            nstock = xstock - parcial

            Stock.objects.create(ubicacion=s[0].ubicacion, item=s[0].item, lote=s[0].lote, cantidad=parcial, stock=nstock, usuario=usuario,
                                 fecha_insercion=timezone.now(), referencia=referencia, operacion=operacion)

            cantidad -= parcial
            lote = Lote()
            lote.item = item
            lote.fecha_lote = timezone.now().date()
            lote.fecha_vencimiento = s[0].lote.fecha_vencimiento
            lote.costo_item = s[0].lote.costo_item
            lote.estado = 'A'
            lote.save()
            xs = Stock.objects.filter(item=item, ubicacion=destino, lote__estado='A').order_by('-fecha_insercion')
            if xs:
                xstock = xs[0].stock
            else:
                xstock = 0
            xstock += parcial
            if operacion == "NUEVO MOVIMIENTO BAJA":
                Stock.objects.create(ubicacion=destino, item=item, lote=lote, cantidad=parcial, stock=xstock,
                                     usuario=usuario,
                                     fecha_insercion=timezone.now(), referencia=referencia, operacion='NUEVO MOVIMIENTO ALTA')
            elif operacion == "SOLICITUD APROBADA BAJA":
                print( "Entro 2")
                Stock.objects.create(ubicacion=destino, item=item, lote=lote, cantidad=parcial, stock=xstock,
                                     usuario=usuario,
                                     fecha_insercion=timezone.now(), referencia=referencia,
                                     operacion='SOLICITUD APROBADA ALTA')
    elif operacion == "NUEVO AJUSTE BAJA":
        while cantidad > 0:
            s = Stock.objects.filter(item=item, ubicacion=origen, stock__gt=0, lote__estado='A').order_by('lote_id',
                                                                                                         '-fecha_insercion')
            if s:
                stock = s[0].stock
            else:
                stock = 0

            parcial = 0

            if cantidad > stock:
                parcial = stock
            else:
                parcial = cantidad

            xs = Stock.objects.filter(item=item, ubicacion=origen, lote__estado='A').order_by('-fecha_insercion')
            if xs:
                xstock = xs[0].stock
            else:
                xstock = 0

            nstock = xstock - parcial

            Stock.objects.create(ubicacion=s[0].ubicacion, item=s[0].item, lote=s[0].lote, cantidad=parcial, stock=nstock,
                                 usuario=usuario,
                                 fecha_insercion=timezone.now(), referencia=referencia, operacion=operacion)
            cantidad -= parcial
    elif operacion == "NUEVO AJUSTE ALTA":
        s = Stock.objects.filter(item=item, ubicacion=origen, lote__estado='A').order_by('-lote_id', '-fecha_insercion')
        lote = Lote()
        lote.item = item
        lote.fecha_lote = timezone.now().date()
        lote.fecha_vencimiento = None
        lote.costo_item = s[0].lote.costo_item
        lote.estado = 'A'
        lote.save()
        xs = Stock.objects.filter(item=item, ubicacion=origen, lote__estado='A').order_by('-fecha_insercion')
        if xs:
            xstock = xs[0].stock
        else:
            xstock = 0
        xstock += cantidad
        Stock.objects.create(ubicacion=origen, item=item, lote=lote, cantidad=cantidad, stock=xstock,
                             usuario=usuario,
                             fecha_insercion=timezone.now(), referencia=referencia, operacion=operacion)
    elif operacion == "ANULAR MOVIMIENTO":
        movimientos = Stock.objects.filter(item=item,
                                           operacion__in=['NUEVO MOVIMIENTO BAJA', 'NUEVO MOVIMIENTO ALTA'],
                                           referencia=referencia)
        for mo in movimientos:
            xs = Stock.objects.filter(item=mo.item, ubicacion=mo.ubicacion, lote__estado='A').order_by('-fecha_insercion')
            if xs:
                xstock = xs[0].stock
            else:
                xstock = 0

            if mo.operacion == 'NUEVO MOVIMIENTO BAJA':
                Stock.objects.create(ubicacion=mo.ubicacion, item=mo.item, lote=mo.lote, cantidad=mo.cantidad, stock=xstock+mo.cantidad,
                                     usuario=usuario,
                                     fecha_insercion=timezone.now(), referencia=referencia,
                                     operacion='ANULAR MOVIMIENTO')
            elif mo.operacion == 'NUEVO MOVIMIENTO ALTA':
                Stock.objects.create(ubicacion=mo.ubicacion, item=mo.item, lote=mo.lote, cantidad=mo.cantidad, stock=xstock-mo.cantidad,
                                     usuario=usuario,
                                     fecha_insercion=timezone.now(), referencia=referencia,
                                     operacion='ANULAR MOVIMIENTO')
    elif operacion == "ANULAR SOLICITUD":
        movimientos = Stock.objects.filter(item=item,
                                           operacion__in=['SOLICITUD APROBADA BAJA', 'SOLICITUD APROBADA ALTA'],
                                           referencia=referencia)
        for mo in movimientos:
            xs = Stock.objects.filter(item=mo.item, ubicacion=mo.ubicacion, lote__estado='A').order_by('-fecha_insercion')
            if xs:
                xstock = xs[0].stock
            else:
                xstock = 0

            if mo.operacion == 'SOLICITUD APROBADA BAJA':
                Stock.objects.create(ubicacion=mo.ubicacion, item=mo.item, lote=mo.lote, cantidad=mo.cantidad, stock=xstock+mo.cantidad,
                                     usuario=usuario,
                                     fecha_insercion=timezone.now(), referencia=referencia,
                                     operacion='ANULAR SOLICITUD')
            elif mo.operacion == 'SOLICITUD APROBADA ALTA':
                Stock.objects.create(ubicacion=mo.ubicacion, item=mo.item, lote=mo.lote, cantidad=mo.cantidad, stock=xstock-mo.cantidad,
                                     usuario=usuario,
                                     fecha_insercion=timezone.now(), referencia=referencia,
                                     operacion='ANULAR SOLICITUD')
    elif operacion == "ANULAR AJUSTE ALTA":
        movimientos = Stock.objects.filter(item=item,
                                           operacion__in=['NUEVO AJUSTE ALTA'],
                                           referencia=referencia)
        for mo in movimientos:
            xs = Stock.objects.filter(item=mo.item, ubicacion=mo.ubicacion, lote__estado='A').order_by('-fecha_insercion')
            if xs:
                xstock = xs[0].stock
            else:
                xstock = 0

            Stock.objects.create(ubicacion=mo.ubicacion, item=mo.item, lote=mo.lote, cantidad=mo.cantidad, stock=xstock-mo.cantidad,
                                     usuario=usuario,
                                     fecha_insercion=timezone.now(), referencia=referencia,
                                     operacion=operacion)

class Movimiento_stock(models.Model):
    ubicacion_origen = models.ForeignKey(Ubicaciones, on_delete=models.PROTECT,null=True, blank=True, related_name="origen")
    ubicacion_destino = models.ForeignKey(Ubicaciones, on_delete=models.PROTECT,null=True, blank=True, related_name="destino")
    fecha = models.DateField(null=True, blank=True)
    observacion = models.CharField(max_length=300)
    creado_el = models.DateTimeField(null=True, blank=True)
    creado_por = models.ForeignKey(User, on_delete=models.PROTECT,null=True, blank=True, related_name="creado")
    modificado_el = models.DateTimeField(null=True, blank=True)
    modificado_por = models.ForeignKey(User, on_delete=models.PROTECT,null=True, blank=True, related_name="modificado")
    estado = models.CharField(max_length=1, default='A')

    def save(self, *args, **kwargs):
        if self.pk:
            detalles = Movimiento_stock_detalles.objects.filter(movimiento_stock=self)
            for d in detalles:
                trxStock(self.ubicacion_origen,
                         d.item,
                         d.cantidad,
                         'ANULAR MOVIMIENTO',
                         self.pk,
                         self.creado_por,
                         self.ubicacion_destino)
        super(Movimiento_stock, self).save(*args, **kwargs)

    # class Meta:
    #     managed = False
    #     db_table = 'core_movimientos_stock'


class Movimiento_stock_detalles(models.Model):
    item = models.ForeignKey(Item, on_delete=models.PROTECT,null=True, blank=True)
    movimiento_stock =  models.ForeignKey(Movimiento_stock, on_delete=models.PROTECT,null=True, blank=True)
    cantidad = models.IntegerField(null=True, blank=True)
    observacion = models.CharField(max_length=300)

    def save(self, *args, **kwargs):
        if not self.pk:
            trxStock(self.movimiento_stock.ubicacion_origen,
                     self.item,
                     self.cantidad,
                     'NUEVO MOVIMIENTO BAJA',
                     self.movimiento_stock.pk,
                     self.movimiento_stock.creado_por,
                     self.movimiento_stock.ubicacion_destino)
        super(Movimiento_stock_detalles, self).save(*args, **kwargs)


class InventarioRecepcion(models.Model):
    item = models.ForeignKey(Item, on_delete=models.PROTECT,  null=True, blank=True)
    estado = models.CharField(max_length=1, null=True, blank=True)
    insertado_el = models.DateTimeField(null=True, blank=True)
    insertado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True)
    referencia = models.IntegerField(null=True, blank=True)
    cantidad = models.DecimalField(max_digits=20, decimal_places=3)
    restantes = models.IntegerField(null=True)

class Ajuste_movimiento(models.Model):
    ubicacion = models.ForeignKey(Ubicaciones, on_delete=models.PROTECT,null=True, blank=True)
    fecha = models.DateField(null=True, blank=True)
    observacion = models.CharField(max_length=200)
    estado = models.CharField(max_length=1, null=True, blank=True)
    operacion = models.CharField(max_length=200, null=True, blank=True)
    creado_por = models.ForeignKey(User, on_delete=models.PROTECT,null=True, blank=True)
    creado_el = models.DateTimeField(null=True, blank=True)
    modificado_el = models.DateTimeField(null=True, blank=True)
    modificado_por = models.ForeignKey(User, on_delete=models.PROTECT,null=True, blank=True, related_name="modificado_id")

    def save(self, *args, **kwargs):
        if self.pk:
            detalles = Ajuste_stock_detalle.objects.filter(ajustestock=self)
            for d in detalles:
                if self.operacion == "Baja":
                    trxStock(self.ubicacion,
                             d.item,
                             d.cantidad,
                             'ANULAR AJUSTE BAJA',
                             self.pk,
                             self.creado_por,
                             None)
                else:
                    trxStock(self.ubicacion,
                             d.item,
                             d.cantidad,
                             'ANULAR AJUSTE ALTA',
                             self.pk,
                             self.creado_por,
                             None)
        super(Ajuste_movimiento, self).save(*args, **kwargs)

    # class Meta:
    #     managed = False
    #     db_table = 'core_ajuste_stock'


class Ajuste_stock_detalle(models.Model):
    ajustestock = models.ForeignKey(Ajuste_movimiento, on_delete=models.PROTECT,null=True, blank=True)
    item = models.ForeignKey(Item, on_delete=models.PROTECT,null=True, blank=True)
    cantidad = models.IntegerField(null=True, blank=True)
    observacion = models.CharField(max_length=200)

    def save(self, *args, **kwargs):
        if not self.pk:
            if self.ajustestock.operacion == 'Baja':
                trxStock(self.ajustestock.ubicacion,
                         self.item,
                         self.cantidad,
                         'NUEVO AJUSTE BAJA',
                         self.ajustestock.pk,
                         self.ajustestock.creado_por,
                         None)
            else:
                trxStock(self.ajustestock.ubicacion,
                     self.item,
                     self.cantidad,
                     'NUEVO AJUSTE ALTA',
                     self.ajustestock.pk,
                     self.ajustestock.creado_por,
                     None)
        super(Ajuste_stock_detalle, self).save(*args, **kwargs)
    
class Periodo_inventario(models.Model):
    periodo = models.IntegerField(null=True)
    estado = models.CharField(max_length=1, null=True, blank=True)
    creado_el = models.DateTimeField(null=True, blank=True)
    creado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True, related_name="creado_inventario")
    modificado_el = models.DateTimeField(null=True, blank=True)
    modificado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True, related_name="modificado_inventario")

    # class Meta:
    #     managed = False
    #     db_table = 'core_periodo_inventario'

    def __str__(self):
        return str(self.periodo)
    
class ItemInventario(models.Model):
    item = models.ForeignKey(Item, on_delete=models.PROTECT, null=True, blank=True, related_name="articulo_inventario")
    recinto = models.ForeignKey(Ubicaciones, on_delete=models.PROTECT, null=True, blank=True)
    visible = models.CharField(max_length=1, null=True, blank=True)
    estado = models.CharField(max_length=20, null=True, blank=True)
    periodo = models.ForeignKey(Periodo_inventario, on_delete=models.PROTECT, null=True, blank=True)
    cantidad = models.IntegerField(null=True)
    anho_adquisicion = models.IntegerField(null=True)
    codigo_inventario = models.CharField(max_length=500, null=True, blank=True)
    descripcion = models.CharField(max_length=1000, null=True, blank=True)
    referencia = models.IntegerField(null=True)
    insertado_el = models.DateTimeField(null=True, blank=True)
    insertado_por = models.ForeignKey(User,on_delete=models.PROTECT, null=True, blank=True, related_name="creado_inventario_item")
    modificado_el = models.DateTimeField(null=True, blank=True)
    modificado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True, related_name="modificado_inventario_item")
    modificado_visible_por = models.ForeignKey(User, on_delete=models.PROTECT,  null=True, blank=True, related_name="visible_inventario_item")
    modificado_visible_el = models.DateTimeField(null=True, blank=True)
    categoria = models.ForeignKey(CategoriasStock, on_delete=models.PROTECT, null=True, blank=True, verbose_name="Categoria")
    motivo_baja = models.CharField(max_length=50, null=True, blank=True)


