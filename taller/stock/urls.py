from django.urls import path, include
from stock.views import *
from stock.api import *

urlpatterns = [
    
        path('stock/', stock_v, name='stock'), 

        ###Ubicaciones
        path('ubicacion/listado/',UbicacionListView.as_view(), name='UbicacionListView'),
        path('ubicacion/nuevo/',UbicacionCreateView.as_view(), name='UbicacionCreateView'),
        path('ubicacion/editar/<int:pk>', UbicacionUpdateView.as_view(), name='UbicacionUpdateView'),
        path('ubicacion/eliminar/<int:pk>', UbicaciondeleteView.as_view(), name='UbicaciondeleteView'),


        #Recepcion producto
        path('recepcion/listado/',ProductosRecepcionList, name='ProductosRecepcionList'),
        path('recepcion/crear/<int:id>', ProductosRecepcionCreate, name='ProductosRecepcionCreate'),

        #Moviemiento stock
        path('movimiento/listado/',MSListado, name='MSListado'),
        path('movimiento/create/',MSCreateUpdate, name='MSCreateUpdate'),
        path('movimiento/ubicacion/<str:ubicacion>',datos_items, name='datos_items'),
        path('movimiento/item_catidad/<str:ubicacion>/<int:item>',verificar_cantidad, name='verificar_cantidad'),
        path('movimiento/detail/<int:pk>',MSDetalle.as_view(), name='MSDetalle'),
        path('movimiento/delete/<int:pk>',MSDelete, name='MSDelete'),

        ###Adaptacion de Stock
        path('adaptacion/listado/',AdaptacionStock, name='AdaptacionStock'),
        path('adaptacion/verificacion/<str:ubicacion>/<str:operacion>',ajuste_stock, name='ajuste_stock'),
        path('adaptacion/create/',ASCreateUpdate, name='ASCreateUpdate'),
        path('adaptacion/detail/<int:pk>',ASDetalle.as_view(), name='ASDetalle'),
        path('adaptacion/delete/<int:pk>',ASDelete, name='ASDelete'),

        ###periodo
        path('periodo/listado/',PeriodoInventarioListView.as_view(), name='PeriodoList'),
        path('periodo/create/',PeriodoInventarioCreateView.as_view(), name='PeriodoCreate'),
        path('periodo/update/<int:pk>',PeriodoInventarioUpdate.as_view(), name='PeriodoUpdate'),
        path('periodo/aviso/',aviso_inventario, name='aviso_inventario'),
        path('periodo/delete/<int:pk>',PeriodoInventariodelete.as_view(), name='PeriodoDelete'),
        path('periodo/activar/<int:pk>',Activar_periodo, name='Activar_periodo'),
        path('periodo/deactivar/<int:pk>',estado_inactivar, name='estado_inactivar'),


]       
