from rest_framework.response import Response
from django.http import JsonResponse, HttpResponse
from rest_framework import status
from django.contrib.auth.models import User, Permission, Group
from rest_framework.views import APIView
from rest_framework.decorators import api_view, permission_classes
from rest_framework.parsers import JSONParser
from rest_framework.permissions import AllowAny
from django.utils import timezone
from stock.models import *
from rest_framework import permissions

@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
def datos_items(request, ubicacion):
    print( "entro")
    options = '<option value="" selected="selected">Seleccione </option>'
    print( ubicacion)
    ubicaciones = Ubicaciones.objects.filter(id=ubicacion)
    print( ubicaciones)
   
    k = Stock.objects.all()
    print(k)
    stock_nc = Stock.objects.filter(ubicacion=ubicaciones[0]).values_list('item', flat=True)
    print(stock_nc)
    articulos = Item.objects.filter(id__in=stock_nc)
    for detalle in articulos:
        options += '<option value="%s" >%s</option>' % (
                detalle.pk,
                detalle.nombre_corto
        )

    response = {}
    response['detalle'] = options
    return JsonResponse(response)


@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
def verificar_cantidad(request, ubicacion, item):
    print( "deposito: ", ubicacion)
    print( "item: ", item)
    consulta = Stock.objects.filter(item=item, ubicacion=ubicacion, lote__estado='A').order_by('fecha_insercion')

    if consulta:
        print( "stock: ", consulta[0].stock)
        verificacion_stock = consulta[0].stock
        return Response({"data": verificacion_stock})
    else:
        return Response({"data": 0 })

@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
def ajuste_stock(request, ubicacion, operacion):
    print (operacion)
    print( "entro")
    options = '<option value="" selected="selected">Seleccione </option>'
    print(ubicacion)
    ubicaciones = Ubicaciones.objects.filter(id=ubicacion)
    print("ggggg: ", ubicaciones)
    if operacion == 'Alta':
        stock = Stock.objects.filter(ubicacion=ubicaciones[0]).values_list('item_id', flat=True)
        print(stock)
    else:
        stock = Stock.objects.filter(ubicacion=ubicaciones[0]).values_list('item_id', flat=True)

    articulos = Item.objects.filter(pk__in=stock)
    print(articulos)
    for detalle in articulos:
        options += '<option value="%s" >%s</option>' % (
            detalle.pk,
            detalle.nombre_corto
        )

    response = {}
    response['detalle'] = options
    return JsonResponse(response)