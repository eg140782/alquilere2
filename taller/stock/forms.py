from django.contrib.auth.models import Group, User

from django import forms
from stock.models import *
from django.forms.formsets import BaseFormSet

class UbicacionForm(forms.ModelForm):
    class Meta: 
        model = Ubicaciones
        exclude = {'estado', 'creado_por', 'modificado_por', 'creado_el', 'modificado_el', 'grupoorganizacional'}
        fields = {'nombre', 'departamento'}
        widgets = {
            'nombre' : forms.TextInput(attrs={'class': 'form-control'}),
            'departamento' : forms.Select(attrs={'class': 'form-control'}),
        }

    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)     
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class':'form-control'
            })

class RecepcionFormset(BaseFormSet):
    def clean(self):
        """
        Adds validation to check that no two links have the same anchor or URL
        and that all links have both an anchor and URL.
        """
        if any(self.errors):
            return

class RecepcionForm(forms.Form):
    item = forms.CharField(widget=forms.HiddenInput())
    item_nombre = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control item'}))
    cantidad = forms.IntegerField(widget=forms.TextInput(attrs={'class': 'form-control auto item'}))


class Movimientostockcabeceraform(forms.ModelForm):


    class Meta:
        model = Movimiento_stock
        exclude = {
            'fecha', 'creado_el', 'creado_por', 'modificado_el', 'modificado_por', 'estado'
        }
        widgets = {


            'ubicacion_origen': forms.Select(attrs={'class': 'form-control'}),
            'ubicacion_destino': forms.Select(attrs={'class': 'form-control'}),
            'observacion': forms.Textarea(attrs={'class': 'form-control ', 'cols': 20, 'rows': 5}),
        }

class Movimientostockdetalleform(forms.ModelForm):
    class Meta:
        model = Movimiento_stock_detalles
        exclude = ()

        widgets = {
            'item': forms.Select(attrs={'class': 'form-control', 'onchange': 'add_stock()'}),
            'cantidad': forms.TextInput(attrs={'class': 'form-control cantidad auto', 'onfocusout': 'verificar_cantidad(this);'}),

            'observacion': forms.TextInput(attrs={'class': 'form-control '}),
        }

    stock_dispobible = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control stock auto', 'readonly': 'readonly'}),required=False)

tipo_ajuste = (
    ('', '------------------'),
    ("Alta", "Alta"),
    ("Baja", "Baja"),
)

class ASform(forms.ModelForm):
    operacion = forms.ChoiceField(choices=tipo_ajuste, widget=forms.Select(attrs={'class': 'form-control', 'onchange': 'add_items();'}))
    class Meta:
        model = Ajuste_movimiento
        exclude = {
            'fecha', 'creado_el', 'creado_por', 'estado', 'modificado_el', 'modificado_por'
        }
        widgets = {


            'ubicacion': forms.Select(attrs={'class': 'form-control'}),
            'observacion': forms.Textarea(attrs={'class': 'form-control ', 'cols': 20, 'rows': 5}),
        }

class ASdetalleform(forms.ModelForm):

    class Meta:
        model = Ajuste_stock_detalle
        exclude = ()
        widgets = {
            'item': forms.Select(attrs={'class': 'form-control', 'onchange': 'add_stock()'}),
            'cantidad': forms.TextInput(attrs={'class': 'form-control cantidad', 'onfocusout': 'verificar_cantidad(this);'}),
            'observacion': forms.TextInput(attrs={'class': 'form-control '}),
        }

    stock_dispobible = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control stock auto', 'readonly': 'readonly'}),required=False)



class periodoform(forms.ModelForm):
    class Meta:
        model = Periodo_inventario
        exclude = {
            'creado_el', 'creado_por', 'modificado_el', 'modificado_por', 'estado'
        }
        widgets = {

            'periodo': forms.TextInput(attrs={'class': 'form-control ', 'type':'number', 'min':'2023', 'max':'2025'}),

        }