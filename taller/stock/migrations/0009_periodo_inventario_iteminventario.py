# Generated by Django 5.0 on 2024-03-30 23:56

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('compras', '0025_ordendetallecompra_recepcionado'),
        ('stock', '0008_ajuste_movimiento_ajuste_stock_detalle'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Periodo_inventario',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('periodo', models.IntegerField(null=True)),
                ('estado', models.CharField(blank=True, max_length=1, null=True)),
                ('creado_el', models.DateTimeField(blank=True, null=True)),
                ('modificado_el', models.DateTimeField(blank=True, null=True)),
                ('creado_por', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='creado_inventario', to=settings.AUTH_USER_MODEL)),
                ('modificado_por', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='modificado_inventario', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ItemInventario',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('visible', models.CharField(blank=True, max_length=1, null=True)),
                ('estado', models.CharField(blank=True, max_length=20, null=True)),
                ('cantidad', models.IntegerField(null=True)),
                ('anho_adquisicion', models.IntegerField(null=True)),
                ('codigo_inventario', models.CharField(blank=True, max_length=500, null=True)),
                ('descripcion', models.CharField(blank=True, max_length=1000, null=True)),
                ('referencia', models.IntegerField(null=True)),
                ('insertado_el', models.DateTimeField(blank=True, null=True)),
                ('modificado_el', models.DateTimeField(blank=True, null=True)),
                ('modificado_visible_el', models.DateTimeField(blank=True, null=True)),
                ('motivo_baja', models.CharField(blank=True, max_length=50, null=True)),
                ('categoria', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='compras.categoriasstock', verbose_name='Categoria')),
                ('insertado_por', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='creado_inventario_item', to=settings.AUTH_USER_MODEL)),
                ('item', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='articulo_inventario', to='compras.item')),
                ('modificado_por', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='modificado_inventario_item', to=settings.AUTH_USER_MODEL)),
                ('modificado_visible_por', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='visible_inventario_item', to=settings.AUTH_USER_MODEL)),
                ('recinto', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='stock.ubicaciones')),
                ('periodo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='stock.periodo_inventario')),
            ],
        ),
    ]
