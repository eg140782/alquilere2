from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic
from stock.models import *
from datetime import datetime
from stock.forms import * 
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.forms.formsets import formset_factory
from django.forms.models import inlineformset_factory
# Create your views here.
from django.utils import timezone
import pytz


def stock_v(request):
    modulo = request.GET.get('module','')
    print(modulo)
    request.session['modulo'] = modulo
    return render(request, "stock/stock_v.html")




class UbicacionListView(LoginRequiredMixin, generic.ListView):
    model = Ubicaciones
    template_name = 'ubicacion/ubicacion_list.html'
    login_url = 'login'
    queryset = Ubicaciones.objects.all().order_by('id')
    context_object_name = "obj"



class UbicacionCreateView(LoginRequiredMixin, generic.CreateView):
    model = Ubicaciones
    form_class = UbicacionForm
    login_url = 'login'
    success_url = reverse_lazy('UbicacionListView')
    template_name = 'ubicacion/ubicacion_create.html'


    def form_valid(self, DepositosForm):
        object = DepositosForm.save(commit=False)
        object.estado = 'A'
        object.creado_por = self.request.user
        object.creado_el = timezone.now()
        object.save()
        return HttpResponseRedirect(self.success_url)
    

class UbicacionUpdateView(LoginRequiredMixin, generic.UpdateView):
    model = Ubicaciones
    form_class = UbicacionForm
    login_url = 'login'
    success_url = reverse_lazy('UbicacionListView')
    template_name = 'ubicacion/ubicacion_create.html'


    def form_valid(self, UbicacionForm):
        object = UbicacionForm.save(commit=False)
        object.usuario_modificacion = self.request.user
        object.fecha_modificacion = timezone.now()
        object.save()
        return HttpResponseRedirect(self.success_url)
    

class UbicaciondeleteView(LoginRequiredMixin, generic.DeleteView):
    template_name = 'ubicacion/ubicacion_delete.html'
    context_object_name = "obj"
    success_url = reverse_lazy("UbicacionListView")
    login_url = 'login'
    model = Ubicaciones
    def delete(self,  request, pk, *args, **kwargs):
        print("entro prro")
        self.object = self.get_object()
        self.object.estado = "I"
        self.object.usuario_modificacion = self.request.user
        self.object.fecha_modificacion = timezone.now()
        self.object.save()
        print("Guardo")   
        return HttpResponseRedirect(self.success_url)
    



def ProductosRecepcionList(request):
    productos = Recepcionproducto.objects.all()

    return render(request, "recepcion/recepcion.html",{'obj':productos})


def ProductosRecepcionCreate(request, id=None):
    print(id)
    grupo = get_grupo_usuario(request.user)
    ubicacion = Ubicaciones.objects.filter(estado="A")
    cabecera = OrdenCompra.objects.get(id=id)
    print(cabecera)
    detalles =OrdenDetalleCompra.objects.filter(orden=id)
    print(detalles)
    formset_activity = formset_factory(form=RecepcionForm, formset=RecepcionFormset, extra=0)
    fecha_today = timezone.now().date()
    print (fecha_today)
    if request.method == "POST":
        formset = formset_activity(request.POST, request.FILES, prefix='nesteds')
        print (formset.errors)
        print ("entro")
        if formset.is_valid():
            ubicacion = request.POST.get('deposito')
            print( "ubicacion", ubicacion, len(formset), formset.total_form_count())
            

            for c in formset:
                
                print("entra al for")
                print(c['item'].value())
                item = c['item'].value()
                print (item)
                print( type(item))
                obj_item = Item.objects.get(pk=int(item))
                print ("item_seleccionado: ", item)
                cantidad = c['cantidad'].value()
                print ("cantidad_seleccionado: ", cantidad, obj_item.stock)
                if obj_item.stock:

                    filter_compra_id = OrdenCompra.objects.filter(id=id)
                    for a in filter_compra_id:
                        print("entraaaaaa")
                        print(a.id)
                        k = a.id

                        vencimiento = None
                        lote = Lote()
                        # lote.costo_item = costo.precio()
                        lote.fecha_lote = fecha_today
                        lote.fecha_vencimiento = vencimiento
                        lote.estado = 'A'
                        lote.item_id = int(item)
                        lote.save()
                        print( "guardo lote")
                        registro_last = Lote.objects.last()
                        print ("ultimo: ", registro_last)

                        stock = Stock()
                        stock.deposito_id = int(ubicacion)
                        stock.lote_id = registro_last.id
                        stock.item_id = int(item)
                        stock.cantidad = cantidad
                        stock.stock = cantidad
                        stock.operacion = 'RECEPCION'
                        stock.referencia = id
                        stock.fecha_insercion = fecha_today
                        stock.usuario_id = request.user.id
                        stock.save()

                        print( "guardo stock")
                        if obj_item.activo_fijo:
                            inventario = InventarioRecepcion()
                            inventario.referencia = id
                            inventario.insertado_el = fecha_today
                            inventario.insertado_por = request.user
                            inventario.item_id = int(item)
                            inventario.cantidad = cantidad
                            inventario.restantes = cantidad
                            inventario.estado = "A"
                            inventario.save()
                            print( "guardo inventario")
                        
                        detalle = OrdenDetalleCompra.objects.filter(orden=id, item= int(c['item'].value())).update(recepcionado=True)
                        print(detalle)
                        print("guarda detalle")

                        
                        
                    solicitud = SolicitudCompra.objects.get(pk=cabecera.solicitud.id)
                    solicitud.estado = "RLR"  # RECIBIDO
                    solicitud.modificado_por = request.user
                    solicitud.modificado_el = timezone.now()
                    solicitud.save()
                    estado = SolicitudAprobacion()
                    estado.solicitud = solicitud
                    estado.usuario = request.user
                    estado.estado = "RLR"  # RECIBIDO
                    estado.creado_el = timezone.now()
                    estado.save()

            return HttpResponseRedirect('/App/stock/recepcion/listado/')

    else:
        formset = formset_activity(initial=[{'cantidad': x.cantidad, 'item': x.item.id, 'item_nombre': x.item_nombre}
                                            for x in detalles], prefix='nesteds')


    return render(request, "recepcion/recepcion_create.html",{
                                                            'request': request,
                                                              'ubicacion':ubicacion,
                                                              'cabecera':cabecera,
                                                              'formset':formset
                                                            })



def MSListado(request):
    desde = request.GET.get('desde', None)
    hasta = request.GET.get('hasta', None)
    destino = request.GET.get('deposito_destino', None)


    if not desde:
        desde = timezone.now().date().strftime('%Y-%m-%d')
    if not hasta:
        hasta = timezone.now().date().strftime('%Y-%m-%d')

    # form = Filtrolistado(initial={'desde': desde, 'hasta': hasta})
    grupo = get_grupo_usuario(request.user)
    # depositos = Deposito.objects.filter(grupoorganizacional=grupo)
    # destino_list = Deposito.objects.all()



    if request.user.is_superuser:
        if not destino:
            movimientos = Movimiento_stock.objects.filter(fecha__range=[desde, hasta], estado='A').order_by('-id')
        else:
            movimientos = Movimiento_stock.objects.filter(fecha__range=[desde, hasta], deposito_destino=destino, estado='A').order_by('-id')
    # else:
    #     if not destino:
    #         movimientos = Movimiento_stock.objects.filter(deposito_origen__in=depositos, fecha__range=[desde, hasta], estado='A').order_by('-id')
    #         # movimientos = Movimiento_stock.objects.filter(deposito_origen__in=depositos, estado='A').order_by('-id')

    #     else:
    #         movimientos = Movimiento_stock.objects.filter(deposito_origen__in=depositos, fecha__range=[desde, hasta], estado='A', deposito_destino=destino).order_by('-id')
    #         # movimientos = Movimiento_stock.objects.filter(deposito_origen__in=depositos,  estado='A', deposito_destino=destino).order_by('-id')

    return render(request, "MS/movimiento_list.html",
                              {
                                #   'form':form,
                                  'movimientos': movimientos,
                                #   'destino': destino_list
                              })


def MSCreateUpdate(request, pk=None):

    grupo = get_grupo_usuario(request.user)
    ubicaciones = Ubicaciones.objects.all()
    fecha_today = timezone.now().date()
    print( timezone.now())
    cabecera = Movimiento_stock()
    cabecera.fecha = timezone.now().date()
    cabecera.creado_el = timezone.now().strftime("%Y-%m-%d %H:%M:%S")
    cabecera.creado_por = request.user


    detallemovimiento =inlineformset_factory(Movimiento_stock, Movimiento_stock_detalles,
                    form=Movimientostockdetalleform, extra=1, can_delete=True)

    if request.method == 'POST':
        form = Movimientostockcabeceraform(request.POST, instance=cabecera)
        formset = detallemovimiento(request.POST, instance=cabecera, prefix='detalle')
        print( form.errors)
        print( formset.errors)
        if form.is_valid() and formset.is_valid():
            form.save()
            print( "form")
            formset.save()
            print( "formset")



            return HttpResponseRedirect('/App/stock/movimiento/listado/')
    else:
        form = Movimientostockcabeceraform(instance=cabecera)
        formset = detallemovimiento(instance=cabecera, prefix='detalle')

    return render(request, "MS/movimiento_create.html",
                              {
                                'form': form,
                                'formset': formset,
                                'request': request,
                                'ubicaciones': ubicaciones

                              }
                              )



class MSDetalle(LoginRequiredMixin,  generic.DetailView): 
    template_name = 'MS/movimiento_detalle.html'
    context_object_name = "obj"
    success_url = reverse_lazy('MSListado')
    login_url = 'login'
    model = Movimiento_stock 

    def get_context_data(self, **kwargs):
        context = super(MSDetalle, self).get_context_data(**kwargs)
        context["detalles"] = Movimiento_stock_detalles.objects.filter(movimiento_stock=self.object)
        return context


# class MSDelete(LoginRequiredMixin, generic.DeleteView):
#     template_name = 'MS/movimiento_delete.html'
#     context_object_name = "obj"
#     success_url = reverse_lazy("MSListado")
#     login_url = 'login'
#     model = Movimiento_stock
#     def delete(self,  request, pk, *args, **kwargs):
#         self.object = self.get_object()
#         self.object.estado = 'I'
#         self.object.modicado_por = self.request.user
#         self.object.modificado_el = timezone.now()
#         self.object.save()
   
#         return HttpResponseRedirect(self.success_url)
    
def MSDelete(request, pk=None):
	main = Movimiento_stock.objects.get(id=pk)

	if request.method == 'POST':
		main.estado = 'I'
		main.modificado_por = request.user
		main.modificado_el = timezone.now()
		main.save()
		return HttpResponseRedirect(reverse_lazy('MSListado'))
	
	data_context = {'request': request,
				 	'object':main}

	return render(request, 'MS/movimiento_delete.html', data_context)


def AdaptacionStock(request):
    desde = request.GET.get('desde', None)
    hasta = request.GET.get('hasta', None)
    tz = pytz.timezone('America/Asuncion')
    fecha_hora = timezone.now().astimezone(tz)
    if not desde:
        desde = timezone.now().astimezone(tz).strftime('%Y-%m-%d')
    if not hasta:
        hasta = timezone.now().astimezone(tz).strftime('%Y-%m-%d')

    listado = Ajuste_movimiento.objects.filter(fecha__range=[desde, hasta], estado='A').order_by('-id')


    return render(request, "AS/adaptacion_listado.html",
                              {
                                'listado':listado,
                              })


def ASCreateUpdate(request, pk=None):
    tz = pytz.timezone('America/Asuncion')
    fecha_hora = timezone.now().astimezone(tz)
    print(fecha_hora)
    ubicaciones = Ubicaciones.objects.all()
    cabecera = Ajuste_movimiento()
    cabecera.fecha = fecha_hora.date()
    cabecera.creado_el = timezone.now().astimezone(tz).strftime("%Y-%m-%d %H:%M:%S")
    cabecera.creado_por = request.user
    cabecera.estado = 'A'

    detalleajustemovimiento = inlineformset_factory(Ajuste_movimiento, Ajuste_stock_detalle,
                                              form=ASdetalleform, extra=1, can_delete=True)

    if request.method == 'POST':
        form = ASform(request.POST, instance=cabecera)
        formset = detalleajustemovimiento(request.POST, instance=cabecera, prefix='detalle')
        print( form.errors)
        print( formset.errors)
        if form.is_valid() and formset.is_valid():
            print( "entro")
            form.save()
            print( "form")
            formset.save()
            print( "formset")



            return HttpResponseRedirect('/App/stock/adaptacion/listado/')
    else:
        form = ASform(instance=cabecera)
        formset = detalleajustemovimiento(instance=cabecera, prefix='detalle')

    return render(request, "AS/AS_create.html",
                              {
                                  'form': form,
                                  'formset': formset,
                                  'request': request,
                                  'ubicaciones':ubicaciones

                              }
                              )

class ASDetalle(LoginRequiredMixin,  generic.DetailView): 
    template_name = 'AS/AS_detalle.html'
    context_object_name = "obj"
    success_url = reverse_lazy('AdaptacionStock')
    login_url = 'login'
    model = Ajuste_movimiento 

    def get_context_data(self, **kwargs):
        context = super(ASDetalle, self).get_context_data(**kwargs)
        context["detalles"] = Ajuste_stock_detalle.objects.filter(ajustestock=self.object)
        return context
    


    

def ASDelete(request, pk=None):
	main = Ajuste_movimiento.objects.get(id=pk)

	if request.method == 'POST':
		main.estado = 'I'
		main.modificado_por = request.user
		main.modificado_el = timezone.now()
		main.save()
		return HttpResponseRedirect(reverse_lazy('AdaptacionStock'))
	
	data_context = {'request': request,
				 	'object':main}

	return render(request, 'AS/AS_delete.html', data_context)


class PeriodoInventarioListView(LoginRequiredMixin, generic.ListView):
    model = Periodo_inventario
    template_name = 'periodo_inventario/periodo_list.html'
    login_url = 'login'
    queryset = Periodo_inventario.objects.all().order_by('id')
    context_object_name = "obj"

class PeriodoInventarioCreateView(LoginRequiredMixin, generic.CreateView):
    model = Periodo_inventario
    form_class = periodoform
    login_url = 'login'
    success_url = reverse_lazy('PeriodoList')
    template_name = 'periodo_inventario/periodo_create.html'


    def form_valid(self, periodoform):
        self.object = periodoform.save(commit=False)
        tz = pytz.timezone('America/Asuncion')
        fecha_hora = timezone.now().astimezone(tz)
        print(fecha_hora)
        self.object.creado_por = self.request.user
        self.object.creado_el = fecha_hora.strftime("%Y-%m-%d %H:%M:%S")
        self.object.estado = 'I'
        self.object.save()

        return HttpResponseRedirect(self.success_url)
    

class PeriodoInventarioUpdate(LoginRequiredMixin, generic.UpdateView):
    model = Periodo_inventario
    form_class = periodoform
    login_url = 'login'
    success_url = reverse_lazy('PeriodoList')
    template_name = 'periodo_inventario/periodo_create.html'
    context_object_name = "obj"


    def form_valid(self, periodoform):
        self.object = periodoform.save(commit=False)
        tz = pytz.timezone('America/Asuncion')
        fecha_hora = timezone.now().astimezone(tz)
        self.object.modificado_por = self.request.user
        self.object.modificado_el = fecha_hora.strftime("%Y-%m-%d %H:%M:%S")
        self.object.save()
        print( "guardo, actualizo")

        return HttpResponseRedirect(self.success_url)


def aviso_inventario(request):

    return render(request, 'alerta/aviso_periodo.html',
                              {
                                
                                })


   
class PeriodoInventariodelete(LoginRequiredMixin, generic.DeleteView):
    template_name = 'periodo_inventario/periodo_delete.html'
    context_object_name = "obj"
    success_url = reverse_lazy("PeriodoList")
    login_url = 'login'
    model = Periodo_inventario
    def delete(self,  request, pk, *args, **kwargs):
        print("entro prro")
        consulta = ItemInventario.objects.filter(periodo=self.get_object())
        print(consulta)
        if consulta:
            return HttpResponseRedirect('/App/stock/periodo/aviso/')

        self.object = self.get_object()
        self.object.delete()
        print("Guardo")   
        return HttpResponseRedirect(self.success_url)
     


def Activar_periodo(request, pk):
    print(pk)
    c = Periodo_inventario.objects.get(pk=pk)
    verificacion = Periodo_inventario.objects.filter(estado="A")
    if len(verificacion) > 0:
        print( "existe  un periodo activo actual, debe de inactivar primero el periodo para habilitar otro")
        periodo = verificacion.values_list('periodo')[0][0]
        estado = verificacion.values_list('estado')[0][0]
        print( estado)
        return render(request, "periodo_inventario/aviso_activo.html",
                                  {
                                      'estado':estado,
                                      'periodo':periodo
                                  }
                                  )
    else:
        print( "Activa Periodo nuevo")
        c.estado = "A"
        c.save()
        print( "Activo")

        return HttpResponseRedirect('/App/stock/periodo/listado/')


def estado_inactivar(request, pk):
    c = Periodo_inventario.objects.get(pk=pk)
    c.estado = "I"
    c.save()
    return HttpResponseRedirect('/App/stock/periodo/listado/')