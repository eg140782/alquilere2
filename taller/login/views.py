from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic
from datetime import datetime
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from login.forms import * 
from django.contrib.auth.models import User, Permission, Group
from django.utils import timezone
from django.shortcuts import redirect
# Create your views here.
def modulos(request):
    
    return render(request, "base/menu_modulos.html")

def login_v(request):
    modulo = request.GET.get('module','')
    print(modulo)
    request.session['modulo'] = modulo
    return render(request, "login/login_v.html")


class UsuarioListar(LoginRequiredMixin, generic.ListView):
    login_url = 'login'
    model = User
    template_name = 'usuarios/usuario_listar.html'


class UsuarioCrear(LoginRequiredMixin, generic.CreateView):
    model = User
    form_class = UsuarioCrearModelForm
    success_url = reverse_lazy('listado_usuario')
    template_name = 'usuarios/usuario_crear.html'
    login_url = 'login'


    def form_valid(self, UsuarioCrearModelForm):
        object = UsuarioCrearModelForm.save(commit=False)
        object.set_password(object.password)
        object.save()
        return HttpResponseRedirect(self.success_url)

class UsuarioModificar(LoginRequiredMixin, generic.UpdateView):
    model = User
    login_url = 'login'
    form_class = UsuarioModificarModelForm
    success_url = reverse_lazy('listado_usuario')
    template_name = 'usuarios/usuario_update.html'

    def form_valid(self, UsuarioModificarModelForm):
        object = UsuarioModificarModelForm.save(commit=False)
        object.set_password(object.password)
        object.save()
        return HttpResponseRedirect(self.success_url)
    
def EstadoUsuario(request, pk, estado):
    print(estado)
    print(pk)
    usuario = User.objects.get(pk=pk)
    if estado == "A":
        usuario.is_active = True
        print("if")
    else:
        usuario.is_active = False
        print("else")

    usuario.save()
    return redirect('listado_usuario')


def PermisosUsuarios(request,pk):
    usuario = User.objects.get(pk=pk)
    permisos_usuario = usuario.user_permissions.all()
    permisos = []

    aux1 = Permission.objects.all()

    for b in aux1:
        if b not in permisos_usuario:
            permisos.append(b)

    return render(request,
                 'usuarios/usuario_permisos.html',
                  {
                    'permisos': permisos,
                    'permisos_usuario': permisos_usuario,
                    'usuario': usuario,
                  })

class GrupoListar(LoginRequiredMixin, generic.ListView):
    login_url = 'login'
    model = Group
    template_name = 'grupos/grupo_listar.html'


class GrupoUsuarioCrear(LoginRequiredMixin, generic.CreateView):
    model = Group
    login_url = 'login'
    form_class = GrupoForm
    success_url = reverse_lazy('GrupoListar')
    template_name = 'grupos/grupo_crear.html'


    def form_valid(self, GrupoForm):
        object = GrupoForm.save(commit=False)
        object.save()
        return HttpResponseRedirect(self.success_url)

class GrupoUsuarioUpdate(LoginRequiredMixin, generic.UpdateView):
    model = Group
    form_class = GrupoForm
    login_url = 'login'
    success_url = reverse_lazy('GrupoListar')
    template_name = 'grupos/grupo_crear.html'


    def form_valid(self, GrupoForm):
        object = GrupoForm.save(commit=False)
        object.save()
        return HttpResponseRedirect(self.success_url)
    

class GrupousuarioDelete(LoginRequiredMixin, generic.DeleteView):
    template_name = 'grupos/grupo_delete.html'
    context_object_name = "obj"
    success_url = reverse_lazy("GrupoListar")
    login_url = 'login'
    model = Group
    def delete(self,  request, pk, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        return HttpResponseRedirect(self.success_url)


def PermisosGrupos(request,pk):
    grupos = Group.objects.get(pk=pk)
    permisos_grupo= grupos.permissions.all()
    permisos = []

    aux1 = Permission.objects.all()

    for b in aux1:
        if b not in permisos_grupo:
            permisos.append(b)

    return render(request,
                 'grupos/grupo_permisos.html',
                  {
                    'permisos': permisos,
                    'permisos_grupo': permisos_grupo,
                    'grupos': grupos,
                  })


def PermisosGruposusuarios(request,pk):
    grupos = Group.objects.get(pk=pk)
    usuarios_grupo= User.objects.filter(groups=grupos)
    usuarios = []

    aux1 = User.objects.filter(is_active=True)

    for b in aux1:
        if b not in usuarios_grupo:
            usuarios.append(b)

    return render(request,
                 'grupos/grupousuario_permisos.html',
                  {
                    'usuarios': usuarios,
                    'usuarios_grupo': usuarios_grupo,
                    'grupos': grupos,
                  })