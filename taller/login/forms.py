from django.contrib.auth.models import Group, User

from django import forms


class UsuarioCrearModelForm(forms.ModelForm):
    class Meta:
        model = User
        exclude = {'is_staff', 'is_active', 'date_joined' , 
                'objects','last_login', 'is_superuser', 
                'groups', 'user_permissions'}
        fields = {'username', 'password', 
            'first_name', 'last_name','email'}
        widgets = {
            'password': forms.PasswordInput(),
        }
    field_order = {'username', 'first_name', 'last_name'}


class UsuarioModificarModelForm(forms.ModelForm):
    class Meta:
        model = User
        exclude = {'is_staff', 'is_active', 'date_joined', 'objects',
                   'last_login', 'is_superuser', 'groups', 'user_permissions'}
        fields = ['username', 'password', 'first_name', 'last_name','email']
        widgets = {
            'password': forms.PasswordInput(),
        }


class GrupoForm(forms.ModelForm):
    class Meta:
        model = Group
        exclude = {'permissions'}

