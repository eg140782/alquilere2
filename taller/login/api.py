from django.contrib.auth.models import Group, User, Permission
from login.models import *
from rest_framework import status
from rest_framework.parsers import JSONParser
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view, permission_classes
from rest_framework import permissions

class UsuariosPermisos(APIView):
    permission_classes = [AllowAny]
    parser_classes = [JSONParser]

    @staticmethod
    def post(request):
        if request.user.is_superuser:
            print( "hola")
            usuario = User.objects.get(pk=request.data['usuario'])
            usuario.user_permissions.through.objects.filter(user=usuario).delete()
            for a in request.data['permisos']:
                permiso = Permission.objects.get(pk=a)
                usuario.user_permissions.add(permiso)
            usuario.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        

class GruposPermisos(APIView):
    permission_classes = [AllowAny]
    parser_classes = [JSONParser]

    @staticmethod
    def post(request):
        if request.user.is_superuser:
            consulta = request.data['grupo']

            print( "hola", consulta)
            grupo = Group.objects.get(pk=request.data['grupo'])
            Group.permissions.through.objects.filter(group=grupo).delete()
            for a in request.data['permisos']:
                permiso = Permission.objects.get(pk=a)
                grupo.permissions.add(permiso)
            grupo.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        
class GruposUsuariosPermisos(APIView):
    permission_classes = [AllowAny]
    parser_classes = [JSONParser]

    @staticmethod
    def post(request):
        if request.user.is_superuser:
            consulta = request.data['grupo']

            print( "hola", consulta)
            grupo = Group.objects.get(pk=request.data['grupo'])
            User.groups.through.objects.filter(group=grupo).delete()
            for a in request.data['usuarios']:
                usuario = User.objects.get(pk=a)
                usuario.groups.add(grupo)
                usuario.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
