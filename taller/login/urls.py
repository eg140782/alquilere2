from django.urls import path, include
from django.contrib.auth import views as auth_views
from login.views import *
from login.api import *

urlpatterns = [
        path('modulos/', modulos, name='menu_modulos'), 
        path('login_menu/', login_v, name='login_v'), 
        path('login/',auth_views.LoginView.as_view(template_name='base/login.html'), name='login'),
        path('logout/',auth_views.LogoutView.as_view(template_name='base/login.html'), name='logout'),

        path('usuario/listado/',UsuarioListar.as_view(), name='listado_usuario'),
        path('usuario/nueva/', UsuarioCrear.as_view(), name='nuevo_usuario'),
        path('usuario/editar/<int:pk>', UsuarioModificar.as_view(), name='modificar_usuario'),
        path('usuario/estado/<int:pk>/<str:estado>', EstadoUsuario, name='estado_usuario'),
        path('usuario/permisos/<int:pk>',PermisosUsuarios, name='permisos_usuario'),
        path('usuario/permisos/', UsuariosPermisos.as_view(), name="permisos_usuario_post"),

        #####grupos de usuarios
        path('grupo/listado/',GrupoListar.as_view(), name='GrupoListar'),
        path('grupo/nuevo/', GrupoUsuarioCrear.as_view(), name='GrupoUsuarioCrear'),
        path('grupo/editar/<int:pk>', GrupoUsuarioUpdate.as_view(), name='GrupoUsuarioUpdate'),
        path('grupo/eliminar/<int:pk>', GrupousuarioDelete.as_view(), name='GrupousuarioDelete'),
        path('grupo/permisos/<int:pk>',PermisosGrupos, name='PermisosGrupos'),
        path('grupo/permisos_api/', GruposPermisos.as_view(), name="permisos_grupo_post"),
        path('grupo/permisos_usuario/<int:pk>',PermisosGruposusuarios, name='PermisosGruposusuarios'),
        path('grupo/permisos_usuario_api/', GruposUsuariosPermisos.as_view(), name="GruposUsuariosPermisos"),
    
]       
