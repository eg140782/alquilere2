# Generated by Django 4.2.5 on 2023-09-21 02:29

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('compras', '0004_solicitudcompra_solicitudcompradetalle_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='GrupoOrganizacional',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=250)),
                ('estado', models.CharField(default='A', max_length=1)),
                ('creado_el', models.DateTimeField(blank=True, null=True)),
                ('modificado_el', models.DateTimeField(blank=True, null=True)),
                ('creado_por', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='grupoorganizacional_creado_por', to=settings.AUTH_USER_MODEL)),
                ('modificado_por', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='grupoorganizacional_modificado_por', to=settings.AUTH_USER_MODEL)),
                ('responsable', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='responsable_grupo', to=settings.AUTH_USER_MODEL)),
                ('usuarios', models.ManyToManyField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
