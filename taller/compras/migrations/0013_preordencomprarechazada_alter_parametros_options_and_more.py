# Generated by Django 4.2.5 on 2023-12-05 00:49

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('compras', '0012_ordencompra_ordendetallecompra'),
    ]

    operations = [
        migrations.CreateModel(
            name='PreOrdenCompraRechazada',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField(blank=True, null=True)),
                ('total', models.DecimalField(blank=True, decimal_places=3, default=0, max_digits=20, null=True)),
                ('aprobacion_ec', models.BooleanField(blank=True, null=True)),
                ('aprobacion_ec_el', models.DateTimeField(blank=True, null=True)),
                ('aprobacion_p', models.BooleanField(blank=True, null=True)),
                ('aprobacion_p_el', models.DateTimeField(blank=True, null=True)),
                ('procesado', models.BooleanField(blank=True, null=True)),
                ('motivo', models.CharField(max_length=500)),
                ('rechazado_el', models.DateTimeField(blank=True, null=True)),
                ('presupuesto', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='compras.presupuestocompra')),
                ('rechazado_por', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='preordencomprarechazada_rechazado_por', to=settings.AUTH_USER_MODEL)),
                ('solicitud', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='compras.solicitudcompra')),
            ],
        ),
        migrations.AlterModelOptions(
            name='parametros',
            options={},
        ),
        migrations.CreateModel(
            name='PreOrdenDetalleCompraRechazada',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('presupuestodetalle_id', models.IntegerField()),
                ('item_nombre', models.CharField(blank=True, max_length=50, null=True)),
                ('cantidad', models.DecimalField(decimal_places=3, max_digits=20)),
                ('precio_unitario', models.DecimalField(decimal_places=3, max_digits=20)),
                ('precio_unitario_extranjera', models.DecimalField(blank=True, decimal_places=3, default=0, max_digits=20, null=True)),
                ('moneda', models.CharField(blank=True, choices=[('GS', 'GUARANIES'), ('DL', 'DOLARES')], max_length=10, null=True)),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='compras.item')),
                ('preorden', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='compras.preordencomprarechazada')),
            ],
        ),
    ]
