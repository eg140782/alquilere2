from django.urls import path, include
from compras.views import *
from compras.autocomplete import * 
from compras.api import *
urlpatterns = [
    path('compras/', compras_v, name='compras'), 

    ###Marcas
    path('marcas/listado/',MarcasView.as_view(), name='MarcasView'),
    path('marcas/nueva/', MarcasCrear.as_view(), name='MarcasCrear'),
    path('marcas/editar/<int:pk>', MarcasUpdate.as_view(), name='MarcasUpdate'),
    path('marcas/delet/<int:pk>', marca_delete.as_view(), name="marca_delete"),
    path('marcas/activar/<int:pk>/', activar_estado, name="activar_estado"),
    path('marcas/inactivar/<int:pk>/', inactivar_estado, name="inactivar_estado"),
    path('marcas/autocomplete/',MarcaAutocomplete.as_view(), name='MarcaAutocomplete'),

    #####proveedores
    path('proveedor/listado/',ProveedoresView.as_view(), name='ProveedoresView'),
    path('proveedor/nuevo/', ProveedorCreateUpdate, name='ProveedorCreateUpdate'),
    path('proveedor/update/<int:pk>', ProveedorCreateUpdate, name='ProveedorUpdate'),
    path('proveedor/aviso/', aviso_proveedor, name='aviso_proveedor'),
    path('proveedor/delete/<int:pk>', Proveedordelete.as_view(), name='Proveedordelete'),
    path('proveedor/detail/<int:pk>', ProveedorDetalle.as_view(), name='ProveedorDetalle'),
    # path('proveedor/timbrado/<int:pk>', TimbradoProveedor, name='TimbradoProveedor'),
    path('cuentas/banco/autocomplete/',BancoAutocomplete.as_view(), name='BancoAutocomplete'),


    ###Productos
    path('producto/listado/',ItemList.as_view(), name='ItemList'),
    path('producto/nuevo/', ItemsCrear.as_view(), name='ItemsCrear'),
    path('producto/editar/<int:pk>', ItemUpdate.as_view(), name='ItemUpdate'),
    path('producto/delete/<int:pk>', Itemdelete.as_view(), name='Itemdelete'),
    path('producto/detail/<int:pk>', ItemDetalle.as_view(), name='ItemDetalle'),
    path('categoria/categoria/autocomplete/',CategoriaAutocomplete.as_view(), name='CategoriaAutocomplete'),

    ##Categoria
    path('categoria/listado/',CategoriaList.as_view(), name='CategoriaList'),
    path('categoria/nueva/', CategoriaCrear.as_view(), name='CategoriaCrear'),
    path('categoria/editar/<int:pk>', CategoriaUpdate.as_view(), name='CategoriaUpdate'),
    path('categoria/delet/<int:pk>', Categoriadelete.as_view(), name="Categoriadelete"),
    path('categoria/alerta/', aviso_categoria, name="aviso_categoria"),

    ##Departamentos
    path('departamento/listado/',DepartamentoListar.as_view(), name='DepartamentoListar'),
    path('departamento/crear/',DepartamentoCrear.as_view(), name='DepartamentoCrear'),
    path('departamento/editar/<int:pk>', DepartamentoUpdate.as_view(), name='DepartamentoUpdate'),
    path('departamento/delete/<int:pk>', Departamentodelete.as_view(), name='Departamentodelete'),
    path('departamento/<int:pk>',DepartamentoUsuarios, name='DepartamentoUsuarios'),
    path('departamento/usuarios_api/', DepartamentosUsuarios.as_view(), name="GruposOrganizacionalesUsuarios"),

    ###Roles de usuario
    path('roles/usuario/listado/',RolesListar.as_view(), name='RolesListar'),
    path('roles/usuario/crear/',RolesCrear.as_view(), name='RolesCrear'),
    path('roles/alerta/', aviso_roles, name="aviso_roles"),
    path('roles/usuario/editar/<int:pk>', RolUpdate.as_view(), name='RolUpdate'),
    path('roles/usuario/delet/<int:pk>', Roldelete.as_view(), name="Roldelete"),

    ###solicitud de compras
    path('solicitud/listado/',SolicitudComprasList, name='SolicitudComprasList'),
    path('solicitud/compras/nueva/',SolicitudComprasCreateUpdate, name='SolicitudComprasCreateUpdate'),
    path('solicitud/items/autocomplete/',ItemsComprasAutocomplete.as_view(), name='ItemsComprasAutocomplete'),
    path('solicitud/departamento/autocomplete/',DepartamentoAutocomplete.as_view(), name='DepartamentoAutocomplete'),
    path('solicitudc/detalles/<int:pk>/', detalles_solicitudCompras, name='detalles_solicitudCompras'),
    path('solicitud/aprobacion/encargado/area/<int:id>', RevisionRA, name='RevisionRA'),
    path('solicitud/aprobacion/encargado/compras/<int:id>', RevisionEC, name='RevisionEC'),
    path('solicitud/anular/<int:pk>', AnularSolicitudCompras, name='AnularSolicitudCompras'),
    path('solicitud/anular/confir/<int:pk>', AnularSolicitudComprasConfirmar, name='AnularSolicitudComprasConfirmar'),
    path('solicitud/Aprobacion/rechazo/<str:ids>/<int:solicitud>/<str:accion>', AprobarRechazar, name='AprobarRechazar'),
    path('solicitud/motivo/rechazo/<int:pk>/', Motivorechazo, name='Motivorechazo'),



    ####Presupuestos
    path('solicitud/presupuestos/encargado/compras/<int:id>/', CargaPresupuestosEncargadoComprasView, name='CargaPresupuestosEncargadoComprasView'),
    path('solicitud/registro/presupuestos/encargado/compras/<int:id>/', RegistroPresupuestosEC, name='RegistroPresupuestosEC'),
    path('solicitud/seleccionar/prespuesto/<int:id>/', SeleccionarPresupuestoEC, name='SeleccionarPresupuestoEC'),
    path('solicitud/definicionec/',definicionec, name='definicionec'),
    path('presupuestos/listado/',PresupuestosListView.as_view(), name='PresupuestosListView'),
    path('presupuestos/detalles/<int:pk>', PresupuestosDetalle.as_view(), name='PresupuestosDetalle'),

    

    ##Aprobacion
    path('solicitud/aprobacion/<int:pk>', AprobacionOC, name='AprobacionOC'),


    ###Ordenes de compras
    path('ordenes/listado/codigo/<int:id>', OrdenesListado, name='OrdenesListado'),
    path('ordenes/listado/todos/', OrdenesComprasTodos, name='OrdenesComprasTodos'),
    path('ordenes/anular/<int:id>', OpcionesAnularOrden, name="OpcionesAnularOrden"),
    path('ordenes/generar/<int:id>/<int:presupuesto>', OrdenComprasGenerar, name='OrdenComprasGenerar'),



    #bancos
    path('bancos/listado/',BancosView.as_view(), name='BancosView'),
    path('bancos/nuevo/', BancosCrear.as_view(), name='BancosCrear'),
    path('bancos/editar/<int:pk>', BancosUpdate.as_view(), name='BancosUpdate'),
    path('bancos/eliminar/<int:pk>', Bancosdelete.as_view(), name='Bancosdelete'),


    ###facturas proveedor
    path('facturas/proveedor/listado/',FacturasProveedorList, name='FacturasProveedorList'),
    path('facturas/proveedor/nuevo/',FacturaRecibidasProveedorCreate, name='FacturaRecibidasProveedorCreate'),
    path('facturas/proveedor/edit/<int:pk>/', FacturaRecibidasProveedorCreate, name='FacturaRecibidasUpdate'),
    path('proveedores/autocomplete/',ProveedorComprasAutocomplete.as_view(), name='ProveedorComprasAutocomplete'),
    path('facturas/proveedor/busca/OC', buscar_ordenes, name="buscar_ordenes"),
    path('facturas/ordenes/<int:proveedor>/', listado_ordenes, name="listado_ordenes"),
    path('facturas/proveedor/detalles/<int:pk>', FacturaRecibidaDetalle.as_view(), name="FacturaRecibidaDetalle"),
    path('facturas/proveedor/delete/<int:pk>', FacturaRecibidaPoveedorDelete.as_view(), name="FacturaRecibidaPoveedorDelete"),


    ###cuentasA_pagar
    path('cuentas/pagar/listado/',ListadodeCuentasApagar, name='ListadodeCuentasApagar'),
    path('cuentas/pagar/crear/',CuentasPagarCreate, name='CuentasPagarCreate'),
    path('cuentas/pagar/alerta/',aviso_cuentas_apagar, name='aviso_cuentas_apagar'),
    path('cuentas/pagar/delete/<int:pk>', CuentasPagarDelete.as_view(), name="CuentasPagarDelete"),
    path('cuentas/pagar/detail/<int:pk>', CuentasPagarDetalle.as_view(), name="CuentasPagarDetalle"),

    ###IVA egresos
    path('iva/egresos/listado/',LibroIvaEgresosList, name='LibroIvaEgresosList'),
    path('iva/egresos/alerta/',aviso_libro_iva, name='aviso_libro_iva'),
    path('iva/egresos/create/',LibroIvaEgresosCreate, name='LibroIvaEgresosCreate'),
    path('iva/egresos/delete/<int:pk>', libroIvaDelete.as_view(), name="libroIvaDelete"),
    path('iva/egresos/detail/<int:pk>', libroIvaEgresosDetalle.as_view(), name="libroIvaEgresosDetalle"),


    #Nota de credito
    path('nota/credito/listado/',NotaCreditoList, name='NotaCreditoList'),
    path('nota/credito/nuevo/',NotaCreditoCreate, name='NotaCreditoCreate'),
    path('nota/credito/ordenes/<int:proveedor>/', listado_ordenes, name="listado_ordenes"),
    path('nota/credito/ordenes/agregar/<int:orden>/', listado_ordenes_agregar, name="listado_ordenes_agregar"),
    path('nota/credito/delete/<int:pk>/', NCProveedorDelete.as_view(), name='NCProveedorDelete'),
    path('nota/credito/detail/<int:pk>/', NCProveedorDetalle.as_view(), name='NCProveedorDetalle'),


    #Nota de debito
    path('nota/debito/listado/',NotaDebitoList, name='NotaDebitoList'),
    path('nota/debito/nuevo/',NDCreateUpdate, name='NDCreateUpdate'),
    path('nota/debito/ordenes/<int:proveedor>/', listado_ordenesdebitos, name="listado_ordenesdebitos"),
    path('nota/debito/ordenes/agregar/<int:orden>/', listado_ordenes_agregar_debito, name="listado_ordenes_agregar_debito"),
    path('nota/debito/delete/<int:pk>/', NDProveedorDelete.as_view(), name='NDProveedorDelete'),
    path('nota/debito/detail/<int:pk>/', NDProveedorDetalle.as_view(), name='NDProveedorDetalle'),

]
