from django.db import models

from django.contrib.auth.models import User
from django.db.models import Q, Sum

UNIDAD = (
    ('UNIDAD', 'UNIDAD'),
    ('METRO', 'METRO'),
    ('LITRO', 'LITRO'),
    ('KILO', 'KILO'),
    ('CENTIMETRO', 'CENTIMETRO'),
    ('HORAS', 'HORAS'),
)


EMPRESAS = (
    (1, 'Universidad'),
    (2, 'Instituto'),
)


ROLES = (
    ('EC', 'ENCARGADO DE COMPRA'),
    ('P', 'DIRECCION'),

)

ESTADO = (
    ('', 'APROBADO POR ENCARGADO COMPRA'),
)

FORMA_PAGO = (
    ('CO', 'CONTADO'),
    ('CR15', 'CREDITO 15 DIAS'),
    ('CR30', 'CREDITO 30 DIAS'),
    ('CR45', 'CREDITO 45 DIAS'),
    ('CR60', 'CREDITO 60 DIAS'),
)

DISPONIBILIDAD = (
    ('I', 'INMEDIATA'),
    ('10', '10 DIAS'),
    ('15', '15 DIAS'),
    ('30', '30 DIAS'),
)

VALIDEZ = (
    ('10', '10 DIAS'),
    ('15', '15 DIAS'),
    ('30', '30 DIAS'),
)

MONEDA = (
    ('GS', 'GUARANIES'),
    # ('DL', 'DOLARES'),
)


MONEDA1 = (
    ('GS', 'GUARANIES'),
)
PAGO = (
    ('E', 'EFECTIVO'),
    ('C', 'CHEQUE'),
    ('T', 'TARJETA'),
)

TIPO_FACTURA = (
    ('C', 'CONTADO'),
    ('K', 'CREDITO'),
    # ('L', 'TICKET LEGAL'),
    # ('NC', 'NOTA DE CREDITO'),
    # ('S', 'SIN COMPROBANTE'),
    # ('V', 'BOLETA DE VENTA'),
)

PORC_IMPUESTO = (
    (0, '0'),
    (5, '5'),
    (10, '10'),
)
FORMATO_COMPROBANTE = (
    ('F', 'FISICA'),
    ('V', 'VIRTUAL'),
    ('E', 'ELECTRONICA'),
)
TIPOS_CUENTAS = (
    ('CA', 'CAJA DE AHORRO'),
    ('CC', 'CUENTA CORRIENTE'),
)
SI_NO = (
    ('S', 'SI'),
    ('N', 'NO'),
)

tipo_cierre = (
    ('Parcial', 'Parcial'),
    ('Mensual', 'Mensual'),
)
class CategoriasStock(models.Model):
    nombre = models.CharField(max_length=50)
    nombre_corto = models.CharField(max_length=50)
    creado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                   blank=True, related_name='cate_creado_por')
    modificado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                       blank=True, related_name='categoria_modificado_por')
    creado_el = models.DateTimeField(null=True, blank=True)
    modificado_el = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return str(self.nombre) 


class Marca(models.Model):
    nombre = models.CharField(max_length=50, unique=True)
    estado = models.CharField(max_length=1, default='A')
    creado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                   blank=True, related_name='marca_creado_por')
    modificado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                       blank=True, related_name='marca_modificado_por')
    creado_el = models.DateTimeField(null=True, blank=True)
    modificado_el = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return str(self.nombre) 




class Item(models.Model):
    nombre_corto = models.CharField(max_length=50)
    nombre_largo = models.CharField(max_length=250)
    marca = models.ForeignKey(Marca, on_delete=models.PROTECT)
    imagen = models.ImageField(upload_to='pictures/item/', null=True, blank=True)
    unidad = models.CharField(max_length=20, choices=UNIDAD)
    servicio = models.BooleanField(verbose_name="Es un Servicio")
    stock = models.BooleanField(verbose_name="Mueve Stock")
    stock_minimo = models.IntegerField(default=0)
    estado = models.CharField(max_length=1, default='A')
    creado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                   blank=True, related_name='item_creado_por')
    modificado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                       blank=True, related_name='item_modificado_por')
    creado_el = models.DateTimeField(null=True, blank=True)
    modificado_el = models.DateTimeField(null=True, blank=True)
    stock_maximo = models.IntegerField(default=0)
    activo_fijo = models.BooleanField(verbose_name="Activo Fijo", )
    vencimiento = models.BooleanField(verbose_name="Vencimiento")
    control_cantidad = models.BooleanField(verbose_name="Control de Cantidad")
    utiles = models.BooleanField(verbose_name="Utiles",null=True,
                                   blank=True)
    categoria = models.ForeignKey(CategoriasStock, models.DO_NOTHING, related_name="pc_marca", null=True, blank=True)
    
    
    def __str__(self):
        return str(self.nombre_largo) 
    
    def costo_ultimo(self):
        # el ultimo precio de compra del producto #
        ultimo = OrdenDetalleCompra.objects.filter(item=self).order_by('-pk')
        if len(ultimo):
            ultimo = ultimo[0]
            if ultimo.moneda == "GS":
                return ultimo.precio_unitario
            else:
                return ultimo.presupuestodetalle.precio_unitario_extranjera
        else:
            return 0

    
class Proveedores(models.Model):
    razon_social = models.CharField(max_length=250, null=True)
    ruc = models.IntegerField()
    dv = models.IntegerField()
    direccion = models.CharField(max_length=250, null=True)
    celular1 = models.CharField(max_length=250, null=True)
    celular2 = models.CharField(max_length=250, null=True)
    observaciones = models.CharField(max_length=250, null=True)
    usuario_modificacion = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                       blank=True, related_name='provee')
    fecha_modificacion = models.DateTimeField(null=True)
    usuario_insercion =models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                       blank=True, related_name='proveeinsert')
    fecha_insercion = models.DateTimeField(null=True)
    estado = models.CharField(max_length=1, default='A')
    tipo_empresa = models.CharField(max_length=20, null=True)
    adjuntoCI = models.FileField(upload_to='proveedores/', null=True, blank=True)
    constanciaInscripcion = models.FileField(upload_to='proveedores/', null=True, blank=True)
    presentacionIva = models.FileField(upload_to='proveedores/', null=True, blank=True)
    poderFirmantes = models.FileField(upload_to='proveedores/', null=True, blank=True)
    estatutosSociales = models.FileField(upload_to='proveedores/', null=True, blank=True)
    email2 = models.CharField(max_length=100, null=True, blank=True)
    email3 = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return str(self.razon_social) + ' - ' + str(self.ruc) +'-'+str(self.dv)

class Bancos(models.Model):
    # banco = models.IntegerField(primary_key=True)
    descripcion = models.CharField(max_length=60)
    fecha_cierre = models.DateField()
    ruc = models.CharField(max_length=15)
    estado = models.CharField(max_length=1)
    usuario_modificacion = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                blank=True, related_name='usuarios')
    fecha_modificacion = models.DateTimeField(null=True, blank=True)
    usuario_insercion = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                blank=True, related_name='usuario_insrd')
    fecha_insercion = models.DateTimeField(null=True, blank=True)
    # class Meta:
    #     managed = False
    #     db_table = 'bancos'

    def __str__(self):
        return str(self.descripcion) 
    
class CuentasProveedor(models.Model):
    cabecera = models.ForeignKey(Proveedores, on_delete=models.PROTECT)
    mismo_titular = models.CharField(max_length=2, choices=SI_NO, null=True, blank=True)
    tipo = models.CharField(max_length=20, choices=TIPOS_CUENTAS, null=True, blank=True)
    num_cuenta = models.IntegerField(null=True, blank=True)
    moneda = models.CharField(max_length=10, choices=MONEDA, null=True, blank=True)
    titular = models.CharField(max_length=30, null=True, blank=True)
    num_ci = models.CharField(max_length=30, null=True, blank=True)
    estado = models.CharField(max_length=1, default='A',null=True, blank=True)
    banco = models.ForeignKey(Bancos, on_delete=models.PROTECT,null=True, blank=True)

    def __str__(self):
        return str(self.titular) +' - '+str(self.num_cuenta)
    


class SolicitudCompra(models.Model):
    fecha = models.DateField()
    motivo = models.CharField(max_length=250)
    # adjunto = models.FileField(upload_to='solicitudes/compras/', null=True, blank=True)
    estado = models.CharField(max_length=10, choices=ESTADO)
    creado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                   blank=True, related_name='solicitud_creado_por')
    modificado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                       blank=True, related_name='solicitud_modificado_por')
    creado_el = models.DateTimeField(null=True, blank=True)
    modificado_el = models.DateTimeField(null=True, blank=True)

    def get_emitida(self):
        orden = OrdenCompra.objects.filter(solicitud=self)
        if len(orden) > 0:
            return True
        else:
            return False
    emitida = property(get_emitida)

    def get_falta_ec(self):
        falta = False
        preordenes = PreOrdenCompra.objects.filter(solicitud=self)
        for preorden in preordenes:
            print(preorden.aprobacion_ec, preorden.solicitud)
            if preorden.aprobacion_ec == None:
                falta = True 
        return falta 
    get_falta_ec = property(get_falta_ec)

    def get_falta_p(obj):
        falta = False
        preordenes = PreOrdenCompra.objects.filter(solicitud=obj)
        # monto_vp = ParametrosAdicionales.objects.get(nombre="VICEPRESIDENCIA HASTA")
        for preorden in preordenes:
            # if preorden.aprobacion_p == None and preorden.total > monto_vp.valor_numero:
            if preorden.aprobacion_p == None:
                falta = True
        return falta
        
    get_falta_p = property(get_falta_p)

class Departamentos(models.Model):
    nombre = models.CharField(max_length=250)
    responsable = models.ForeignKey(User, on_delete=models.PROTECT, related_name='responsable_grupo')
    estado = models.CharField(max_length=1, default="A")
    creado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                   blank=True, related_name='grupoorganizacional_creado_por')
    modificado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                       blank=True, related_name='grupoorganizacional_modificado_por')
    creado_el = models.DateTimeField(null=True, blank=True)
    modificado_el = models.DateTimeField(null=True, blank=True)
    usuarios = models.ManyToManyField(User)

    def __str__(self):
        return str(self.nombre)


class SolicitudCompraDetalle(models.Model):
    solicitud = models.ForeignKey(SolicitudCompra, on_delete=models.PROTECT)
    departamento = models.ForeignKey(Departamentos, on_delete=models.PROTECT)
    item = models.ForeignKey(Item, on_delete=models.PROTECT)
    item_nombre = models.CharField(max_length=50, null=True, blank=True)
    cantidad = models.DecimalField(max_digits=20, decimal_places=3)

    def save(self, *args, **kwargs):
        self.item_nombre = self.item.nombre_corto
        super(SolicitudCompraDetalle, self).save(*args, **kwargs)

class SolicitudAprobacion(models.Model):
    solicitud = models.ForeignKey(SolicitudCompra, on_delete=models.PROTECT)
    usuario = models.ForeignKey(User, on_delete=models.PROTECT,)
    estado = models.CharField(max_length=10, choices=ESTADO)
    creado_el = models.DateTimeField(null=True, blank=True)
    comentario = models.CharField(max_length=300, null=True, blank=True)


class RolesUsuarios(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                blank=True, related_name='rol_compra_usuario')
    rol = models.CharField(max_length=2, choices=ROLES)
    estado = models.CharField(max_length=1, default="A")
    creado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                   blank=True, related_name='rolcompra_creado_por')
    modificado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                       blank=True, related_name='rolcompra_modificado_por')
    creado_el = models.DateTimeField(null=True, blank=True)
    modificado_el = models.DateTimeField(null=True, blank=True)
    valor_numero =  models.IntegerField()


class PresupuestoCompra(models.Model):
    fecha = models.DateField(null=True, blank=True)
    solicitud = models.ForeignKey(SolicitudCompra, on_delete=models.PROTECT)
    proveedor = models.ForeignKey(Proveedores, on_delete=models.PROTECT)
    validez = models.CharField(max_length=10, choices=VALIDEZ, null=True, blank=True)
    forma_pago = models.CharField(max_length=10, choices=FORMA_PAGO, null=True, blank=True)
    disponibilidad = models.CharField(max_length=10, choices=DISPONIBILIDAD, null=True, blank=True)
    adjunto = models.FileField(upload_to='presupuestos/compras/', null=True, blank=True)
    # cotizacion = models.IntegerField(null=True, blank=True)
    estado = models.CharField(max_length=10, choices=ESTADO, default="A")
    creado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                   blank=True, related_name='presupuesto_creado_por')
    modificado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                       blank=True, related_name='presupuesto_modificado_por')
    creado_el = models.DateTimeField(null=True, blank=True)
    modificado_el = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return str(self.proveedor)
    # def get_total(self):
    #     total = 0
    #     detalles = PresupuestoDetalleCompra.objects.filter(presupuesto=self)
    #     for detalle in detalles:
    #         total += detalle.precio_unitario * detalle.cantidad
    #     return total
    def moneda_detalle(self):
        c = PresupuestoDetalleCompra.objects.filter(presupuesto = self)
        return c 
    
class PresupuestoDetalleCompra(models.Model):
    presupuesto = models.ForeignKey(PresupuestoCompra, on_delete=models.PROTECT)
    item = models.ForeignKey(Item, on_delete=models.PROTECT)
    # sede = models.ForeignKey(Sede, on_delete=models.PROTECT)
    item_nombre = models.CharField(max_length=50, null=True, blank=True)
    cantidad = models.DecimalField(max_digits=20, decimal_places=3)
    precio_unitario = models.DecimalField(max_digits=20, decimal_places=3)
    precio_unitario_extranjera = models.DecimalField(max_digits=20, decimal_places=3, null=True, blank=True, default=0)
    moneda = models.CharField(max_length=10, choices=MONEDA, null=True, blank=True)

    def save(self, *args, **kwargs):
        self.item_nombre = self.item.nombre_corto
        super(PresupuestoDetalleCompra, self).save(*args, **kwargs)



class PreOrdenCompra(models.Model):
    presupuesto = models.ForeignKey(PresupuestoCompra, on_delete=models.PROTECT)
    solicitud = models.ForeignKey(SolicitudCompra, on_delete=models.PROTECT)
    fecha = models.DateField(null=True, blank=True)
    total = models.DecimalField(max_digits=20, decimal_places=3, null=True, blank=True, default=0)
    aprobacion_ec = models.BooleanField(null=True)
    aprobacion_ec_el = models.DateTimeField(null=True, blank=True)
    aprobacion_p = models.BooleanField(null=True)
    aprobacion_p_el = models.DateTimeField(null=True, blank=True)
    procesado = models.BooleanField(null=True, blank=True)
    estado = models.CharField(max_length=10, choices=ESTADO, default="A")
    creado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                   blank=True, related_name='preordencompra_creado_por')
    modificado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                       blank=True, related_name='preordencompra_modificado_por')
    creado_el = models.DateTimeField(null=True, blank=True)
    modificado_el = models.DateTimeField(null=True, blank=True)


class Parametros(models.Model):
    nombre = models.CharField(max_length=50, null=True, blank=True)
    numero = models.IntegerField()
    secuencia = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'parametros'



class PreOrdenDetalleCompra(models.Model):
    preorden = models.ForeignKey(PreOrdenCompra, on_delete=models.PROTECT)
    presupuestodetalle = models.ForeignKey(PresupuestoDetalleCompra, on_delete=models.PROTECT)
    item = models.ForeignKey(Item, on_delete=models.PROTECT)
    item_nombre = models.CharField(max_length=50, null=True, blank=True)
    cantidad = models.DecimalField(max_digits=20, decimal_places=3)
    precio_unitario = models.DecimalField(max_digits=20, decimal_places=3)
    precio_unitario_extranjera = models.DecimalField(max_digits=20, decimal_places=3, null=True, blank=True, default=0)
    moneda = models.CharField(max_length=10, choices=MONEDA, null=True, blank=True)

    def save(self, *args, **kwargs):
        self.item_nombre = self.item.nombre_corto
        self.preorden.total += self.cantidad * self.precio_unitario
        self.preorden.save()
        super(PreOrdenDetalleCompra, self).save(*args, **kwargs)



class OrdenCompra(models.Model):
    # id = models.IntegerField(primary_key=True)
    presupuesto = models.ForeignKey(PresupuestoCompra, on_delete=models.PROTECT)
    solicitud = models.ForeignKey(SolicitudCompra, on_delete=models.PROTECT)
    periodo = models.IntegerField(null=True, blank=True)
    numero = models.IntegerField(null=True, blank=True)
    fecha = models.DateField(null=True, blank=True)
    total = models.DecimalField(max_digits=20, decimal_places=3)
    aprobacion_ec = models.BooleanField(null=True)
    aprobacion_ec_el = models.DateTimeField(null=True, blank=True)
    aprobacion_p = models.BooleanField(null=True)
    aprobacion_p_el = models.DateTimeField(null=True, blank=True)
    estado = models.CharField(max_length=10, choices=ESTADO, default="A")
    creado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                   blank=True, related_name='ordencompra_creado_por')
    modificado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                       blank=True, related_name='ordencompra_modificado_por')
    creado_el = models.DateTimeField(null=True, blank=True)
    modificado_el = models.DateTimeField(null=True, blank=True)


   
    def get_orden_nombre(self):
        orden = str(self.orden.numero)
        periodo = str(self.orden.periodo)
        return orden+"/"+periodo


    def presu_num(self):
        print("self: ", self)
        rum = OrdenCompra.objects.get(solicitud=self.solicitud).presupuesto.id
        return rum
    

    def save(self, *args, **kwargs):
        existe = OrdenCompra.objects.filter(pk=self.pk)
        if len(existe) == 0:
            self.periodo = 2024
            numero = Parametros.objects.get(nombre="orden_numero")
            # numero = OrdenCompra.objects.values_list('numero', flat= True).last()
            print(numero.secuencia)
            self.numero = numero.secuencia + 1
            numero.secuencia = numero.secuencia + 1
            numero.save()

        super(OrdenCompra, self).save(*args, **kwargs)


class OrdenDetalleCompra(models.Model):
    orden = models.ForeignKey(OrdenCompra, on_delete=models.PROTECT)
    presupuestodetalle = models.ForeignKey(PresupuestoDetalleCompra, on_delete=models.PROTECT)
    item = models.ForeignKey(Item, on_delete=models.PROTECT)
    item_nombre = models.CharField(max_length=50, null=True, blank=True)
    cantidad = models.DecimalField(max_digits=20, decimal_places=3)
    precio_unitario = models.DecimalField(max_digits=20, decimal_places=3)
    moneda = models.CharField(max_length=10, choices=MONEDA, null=True, blank=True)
    recepcionado =  models.BooleanField(null=True, blank=True)

    def save(self, *args, **kwargs):
        self.item_nombre = self.item.nombre_corto
        super(OrdenDetalleCompra, self).save(*args, **kwargs)

    def faltante(self):
        detalles_restantes = FacturasProveedorConceptos.objects.filter(orden=self.orden, item_producto=self.item).aggregate(Sum('cantidad'))[
            'cantidad__sum']
        if detalles_restantes is None:
            detalles_restantes = 0
        return self.cantidad - detalles_restantes

    def precio(self):
        print("hhhhhhhhhh: ", self)
        retornar =OrdenDetalleCompra.objects.get(id=self).precio_unitario
        return retornar

    def total(self):
        return (self.precio_unitario*self.cantidad)

    total = property(total)
    
    def centro_costo_solicitud(self):
        return SolicitudCompraDetalle.objects.filter(solicitud_id=self.orden.solicitud, sede_id=self.sede, item_id=self.item, cantidad=self.cantidad)[0].centro_costo.nombre

    centro_costo_solicitud = property(centro_costo_solicitud)


    # def get_totalfactura(self):
    #     totalfacturas = FacturasRecibidasConceptos.objects.filter(orden=self.orden, item_producto=self.item)
    #     if len(totalfacturas)== 0:
    #         return 0
    #     else:
    #         return totalfacturas[0].monto
    # get_totalfactura = property(get_totalfactura)

    # def get_pendientefact(self):
    #     totalfacturas = FacturasRecibidasConceptos.objects.filter(orden=self.orden, item_producto=self.item)
    #     if len(totalfacturas)== 0:
    #         return 0
    #     else:
    #         return (self.precio_unitario*self.cantidad)- totalfacturas[0].monto
    # get_pendientefact = property(get_pendientefact)

    # def get_saldopend(self):
    #     totalfacturas = FacturasRecibidasConceptos.objects.filter(orden=self.orden, item_producto=self.item)
    #     if len(totalfacturas) == 0:
    #         return 0
    #     else:
    #         return totalfacturas[0].factura.saldo

    # get_saldopend = property(get_saldopend)


class PreOrdenCompraRechazada(models.Model):
    # id = models.IntegerField(primary_key=True)
    presupuesto = models.ForeignKey(PresupuestoCompra, on_delete=models.PROTECT)
    solicitud = models.ForeignKey(SolicitudCompra, on_delete=models.PROTECT)
    fecha = models.DateField(null=True, blank=True)
    total = models.DecimalField(max_digits=20, decimal_places=3, null=True, blank=True, default=0)
    aprobacion_ec = models.BooleanField(null=True, blank=True)
    aprobacion_ec_el = models.DateTimeField(null=True, blank=True)
    aprobacion_p = models.BooleanField(null=True, blank=True)
    aprobacion_p_el = models.DateTimeField(null=True, blank=True)
    procesado = models.BooleanField(null=True, blank=True)
    motivo = models.CharField(max_length=500)
    rechazado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                      blank=True, related_name='preordencomprarechazada_rechazado_por')
    rechazado_el = models.DateTimeField(null=True, blank=True)


class PreOrdenDetalleCompraRechazada(models.Model):
    # id = models.IntegerField(primary_key=True)
    preorden = models.ForeignKey(PreOrdenCompraRechazada, on_delete=models.PROTECT)
    presupuestodetalle_id = models.IntegerField()
    item = models.ForeignKey(Item, on_delete=models.PROTECT)
    item_nombre = models.CharField(max_length=50, null=True, blank=True)
    cantidad = models.DecimalField(max_digits=20, decimal_places=3)
    precio_unitario = models.DecimalField(max_digits=20, decimal_places=3)
    precio_unitario_extranjera = models.DecimalField(max_digits=20, decimal_places=3, null=True, blank=True, default=0)
    moneda = models.CharField(max_length=10, choices=MONEDA, null=True, blank=True)

    def save(self, *args, **kwargs):
        self.item_nombre = self.item.nombre_corto
        self.preorden.total += self.cantidad * self.precio_unitario
        self.preorden.save()
        super(PreOrdenDetalleCompraRechazada, self).save(*args, **kwargs)

class FacturasRecibidasProveedor(models.Model):
    factura = models.IntegerField()
    fecha = models.DateField()
    forma_pago = models.CharField(max_length=1, choices=PAGO, null=True, blank=True)
    proveedor = models.ForeignKey(Proveedores, on_delete=models.PROTECT)
    factura_fisica = models.CharField(max_length=15)
    moneda = models.CharField(max_length=2, choices=MONEDA1)
    monto_gravado = models.DecimalField(max_digits=17, decimal_places=2)
    monto_exento = models.DecimalField(max_digits=17, decimal_places=2)
    monto_impuesto = models.DecimalField(max_digits=17, decimal_places=2)
    saldo = models.DecimalField(max_digits=17, decimal_places=2)
    comentario = models.CharField(max_length=250, null=True, blank=True)
    fecha_insercion = models.DateTimeField(null=True, blank=True)
    fecha_modificacion = models.DateTimeField(null=True, blank=True)
    usuario_insercion = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                   blank=True, related_name='fc_creado_por')
    usuario_modificacion = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                   blank=True, related_name='fcm_creado_por')
    # numero_presupuesto = models.IntegerField(null=True, blank=True)
    # fecha_presupuesto = models.DateField(null=True, blank=True)
    tipo_de_factura = models.CharField(max_length=2, choices=TIPO_FACTURA)
    formato_comprobante = models.CharField(max_length=2, choices=FORMATO_COMPROBANTE, blank=True, null=True)
    factor_cambio = models.IntegerField(blank=True, null=True)
    # reposicion = models.ForeignKey(Reposiciones, db_column="reposicion", null=True, blank=True)  # no
    monto_gravado5 = models.DecimalField(max_digits=17, decimal_places=2)
    monto_impuesto5 = models.DecimalField(max_digits=17, decimal_places=2)
    monto_total10 = models.DecimalField(max_digits=17, decimal_places=2)
    monto_total5 = models.DecimalField(max_digits=17, decimal_places=2)
    asiento_generado = models.IntegerField(null=True, blank=True)
    proceso = models.IntegerField(null=True, blank=True)
    responsable = models.IntegerField(null=True, blank=True)  # no
    recibido_docfisico = models.CharField(max_length=1)  # no
    nrotimbrado = models.IntegerField(null=True, blank=True)
    nrotimbrado_vencimiento = models.DateField(null=True, blank=True)
    departamento = models.ForeignKey(Departamentos, on_delete=models.PROTECT, null=True, blank=True)
    fecha_recepcion = models.DateTimeField(null=True, blank=True)
    usuario_recepcion = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                       blank=True, related_name='usuario_recepcion')
    imagen = models.FileField(upload_to='facturas/recibidas/', null=True, blank=True)
    banco = models.ForeignKey(Bancos, null=True, blank=True, on_delete=models.PROTECT)
    numero_documento = models.CharField(max_length=20, null=True, blank=True)


   
    # class Meta:
    #     managed = False
    #     db_table = 'facturas_recibidas'

    # def __unicode__(self):
    #     return unicode(self.factura_fisica) + " - " + unicode(self.proveedor)

    # def save(self, *args, **kwargs):
    #     print (self.nrotimbrado)
    #     print (self.proveedor.id)
    #     exite = TimbradosProveedores.objects.filter(nrotimbrado=self.nrotimbrado, proveedor=self.proveedor.id)
    #     print (exite)
    #     if len(exite) == 0:
    #         print( "entre")
    #         timbrado = TimbradosProveedores()
    #         timbrado.nrotimbrado = self.nrotimbrado
    #         timbrado.proveedor = self.proveedor.id
    #         timbrado.fecha = self.nrotimbrado_vencimiento
    #         timbrado.save()
    #     super(FacturasRecibidas, self).save(*args, **kwargs)


def get_grupo_usuario(user):
    gor = Departamentos.objects.filter(Q(responsable=user) | Q(usuarios=user), estado="A")
    if gor:
        return gor[0]
    return None

def num_factura():
    nume = FacturasRecibidasProveedor.objects.values_list('factura', flat=True).last()
    if nume == None:
        num = 1
    else:
        num = nume+1

    return num

class FacturasProveedorConceptos(models.Model):
    factura = models.ForeignKey(FacturasRecibidasProveedor, on_delete=models.CASCADE, null=True)
    item = models.IntegerField(null=True, blank=True)
    monto = models.DecimalField(max_digits=17, decimal_places=3)
    monto_impuesto = models.DecimalField(max_digits=17, decimal_places=3)
    porcentaje_impuesto = models.IntegerField(choices=PORC_IMPUESTO, null=True, blank=True)
    fecha_insercion = models.DateTimeField(null=True, blank=True)
    fecha_modificacion = models.DateTimeField(null=True, blank=True)
    usuario_insercion = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                       blank=True, related_name='FCD')
    usuario_modificacion = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                       blank=True, related_name='fcdm')
    item_producto = models.ForeignKey(Item, on_delete=models.PROTECT, null=True, blank=True)
    orden = models.ForeignKey(OrdenCompra, on_delete=models.PROTECT, null=True, blank=True)
    cantidad = models.DecimalField(max_digits=20, decimal_places=3, null=True, blank=True)
    monto_unitario = models.DecimalField(max_digits=17, decimal_places=3, null=True, blank=True)
    
    def save(self, *args, **kwargs):
        if not self.item:
            # Obtener la cantidad de ítems ya existentes para la factura actual
            cantidad_items = FacturasProveedorConceptos.objects.filter(factura=self.factura).count()
            self.item = cantidad_items + 1
        super(FacturasProveedorConceptos, self).save(*args, **kwargs)


# def verificar_orden(cod):
#     c = FacturasRecibidasConceptos.objects.filter(factura=cod)
    
#     check = OrdenDetalleCompra.objects.filter(orden=orden)
#     if check:
#         op= True
#     else:
#         op = False
#     return op

class FacturasProveedorDetalle(models.Model):
    factura = models.ForeignKey(FacturasRecibidasProveedor, on_delete=models.CASCADE)
    cuota = models.IntegerField(null=True, blank=True)
    fecha_vencimiento = models.DateField(null=True, blank=True)
    monto = models.DecimalField(max_digits=17, decimal_places=2)
    saldo = models.DecimalField(max_digits=17, decimal_places=2)
    observacion = models.CharField(max_length=250, null=True, blank=True)
    fecha_insercion = models.DateTimeField(null=True, blank=True)
    fecha_modificacion = models.DateTimeField(null=True, blank=True)
    usuario_insercion =models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                       blank=True, related_name='FCDd')
    usuario_modificacion = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                       blank=True, related_name='FCDmmm')
    


class CuentasaPagar(models.Model):
    anho = models.IntegerField(null=True, blank=True)
    mes = models.CharField(max_length=40,null=True, blank=True)
    fecha = models.DateField(null=True, blank=True)
    creado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                   blank=True, related_name='cuentap_creado_por')
    modificado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                       blank=True, related_name='cuentap_modificado_por')
    creado_el = models.DateTimeField(null=True, blank=True)
    modificado_el = models.DateTimeField(null=True, blank=True)
    total_mes=  models.IntegerField(null=True, blank=True)

class CuentasaPagardet(models.Model):
    CuentaPagar = models.ForeignKey(CuentasaPagar, on_delete=models.CASCADE, null=True, blank=True)
    factura = models.ForeignKey(FacturasRecibidasProveedor, on_delete=models.PROTECT,null=True, blank=True)
    proveedor = models.ForeignKey(Proveedores, on_delete=models.PROTECT, null=True, blank=True)
    factura_fisica = models.CharField(max_length=15, null=True, blank=True)
    saldo = models.DecimalField(max_digits=17, decimal_places=2, null=True, blank=True)
    tipo_de_factura = models.CharField(max_length=2, choices=TIPO_FACTURA, null=True, blank=True)
    total = models.DecimalField(max_digits=17, decimal_places=2, null=True, blank=True)
    monto_gravado5 = models.DecimalField(max_digits=17, decimal_places=2, null=True, blank=True)
    monto_impuesto5 = models.DecimalField(max_digits=17, decimal_places=2, null=True, blank=True)
    monto_total10 = models.DecimalField(max_digits=17, decimal_places=2, null=True, blank=True)
    monto_total5 = models.DecimalField(max_digits=17, decimal_places=2, null=True, blank=True)
    orden = models.ForeignKey(OrdenCompra, on_delete=models.PROTECT, null=True, blank=True)
    solicitud = models.ForeignKey(SolicitudCompra, on_delete=models.PROTECT)
    fecha_vencimiento = models.DateField(null=True, blank=True)
    cuota = models.IntegerField(null=True, blank=True)
    observacion = models.CharField(max_length=250, null=True, blank=True)

    # def save(self, *args, **kwargs):
    #     # Obtener la cabecera correspondiente
    #     cabecera = self.CuentaPagar
    #     if cabecera:
    #         # Realizar la consulta para obtener el total de los detalles de la factura
    #         consulta = CuentasaPagardet.objects.filter(CuentaPagar=cabecera).aggregate(Sum('total'))
    #         total_mes = consulta['total__sum'] if consulta['total__sum'] is not None else 0
    #         # Actualizar el campo total_mes en la cabecera
    #         cabecera.total_mes = total_mes
    #         cabecera.save()
    #     super(CuentasaPagardet, self).save(*args, **kwargs)
from django.db.models.signals import post_save
from django.dispatch import receiver

@receiver(post_save, sender=CuentasaPagardet)
def update_total_mes(sender, instance, **kwargs):
    cabecera = instance.CuentaPagar
    if cabecera:
        # Realizar la consulta para obtener el total de los detalles de la factura
        consulta = CuentasaPagardet.objects.filter(CuentaPagar=cabecera).aggregate(Sum('total'))
        total_mes = consulta['total__sum'] if consulta['total__sum'] is not None else 0
        # Actualizar el campo total_mes en la cabecera
        cabecera.total_mes = total_mes
        cabecera.save()



class LibroIvaEgresos(models.Model):
    mes = models.CharField(max_length=40,null=True, blank=True)
    ejercicio_fiscal = models.IntegerField()
    creado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                   blank=True, related_name='ordencompra_creado_poriva')
    modificado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                       blank=True, related_name='ordencompra_modificado_poriva')
    creado_el = models.DateTimeField(null=True, blank=True)
    modificado_el = models.DateTimeField(null=True, blank=True)



class LibroIvaEgresosDetalles(models.Model):
    libro = models.ForeignKey(LibroIvaEgresos, on_delete=models.CASCADE, null=True, blank=True)
    tipoidentificacion = models.IntegerField()
    ruc = models.CharField(max_length=250, null=True)
    razon_social = models.CharField(max_length=250, null=True)
    tipocomprobante = models.CharField(max_length=2, choices=FORMATO_COMPROBANTE, blank=True, null=True)
    nrotimbrado = models.IntegerField(null=True, blank=True)
    factura_fisica = models.CharField(max_length=15)
    tipo_de_factura = models.CharField(max_length=2, choices=TIPO_FACTURA)
    fecha = models.DateField()
    saldo = models.DecimalField(max_digits=17, decimal_places=2)
    monto_gravado = models.DecimalField(max_digits=17, decimal_places=2)
    monto_exento = models.DecimalField(max_digits=17, decimal_places=2)
    monto_impuesto = models.DecimalField(max_digits=17, decimal_places=2)
    monto_gravado5 = models.DecimalField(max_digits=17, decimal_places=2)
    monto_impuesto5 = models.DecimalField(max_digits=17, decimal_places=2)
    monto_total10 = models.DecimalField(max_digits=17, decimal_places=2)
    monto_total5 = models.DecimalField(max_digits=17, decimal_places=2)
    factura = models.ForeignKey(FacturasRecibidasProveedor, on_delete=models.PROTECT, null=True)

def check_libro_iva_compra(factura):
    x = LibroIvaEgresosDetalles.objects.filter(factura_fisica=factura)
    if x:
        c = True
    else:
        c = False
    return c


class NotaCreditoProveedor(models.Model):
    proveedor = models.ForeignKey(Proveedores, on_delete=models.PROTECT, null=True, blank=True)
    fecha = models.DateField(null=True, blank=True)
    numero_credito_fisico = models.IntegerField(null=True, blank=True)
    creado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                   blank=True, related_name='ordencompra_creado_cre')
    modificado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                       blank=True, related_name='ordencompra_modificado_cre')
    creado_el = models.DateTimeField(null=True, blank=True)
    modificado_el = models.DateTimeField(null=True, blank=True)
    recibido_de = models.CharField(max_length=1500, null=True, blank=True)
    total = models.DecimalField(max_digits=17, decimal_places=2)

    def get_ordenes(self):
        orden = NotaCreditoProveedordet.objects.filter(nota_credito=self)
        for orden in orden:
            num = orden.orden 
        return num 
    orden = property(get_ordenes)


class NotaCreditoProveedordet(models.Model):
    nota_credito = models.ForeignKey(NotaCreditoProveedor, on_delete=models.CASCADE, null=True, blank=True)
    orden = models.IntegerField(null=True, blank=True)
    item = models.CharField(max_length=1500, null=True, blank=True)
    item_num = models.IntegerField(null=True, blank=True)
    cantidad = models.IntegerField(null=True, blank=True)
    monto_unitario = models.DecimalField(max_digits=17, decimal_places=2)
    total =  models.DecimalField(max_digits=17, decimal_places=2)

class NotaDebitoProveedor(models.Model):
    proveedor = models.ForeignKey(Proveedores, on_delete=models.PROTECT, null=True, blank=True)
    fecha = models.DateField(null=True, blank=True)
    numero_debito_fisico = models.IntegerField(null=True, blank=True)
    creado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                   blank=True, related_name='ordencompra_creado_deb')
    modificado_por = models.ForeignKey(User, on_delete=models.PROTECT, null=True,
                                       blank=True, related_name='ordencompra_modificado_deb')
    creado_el = models.DateTimeField(null=True, blank=True)
    modificado_el = models.DateTimeField(null=True, blank=True)
    recibido_de = models.CharField(max_length=1500, null=True, blank=True)
    total = models.DecimalField(max_digits=17, decimal_places=2)


class NotaDebitoProveedordet(models.Model):
    nota_debito = models.ForeignKey(NotaDebitoProveedor, on_delete=models.CASCADE, null=True, blank=True)
    orden = models.IntegerField(null=True, blank=True)
    item = models.CharField(max_length=1500, null=True, blank=True)
    item_num = models.IntegerField(null=True, blank=True)
    cantidad = models.IntegerField(null=True, blank=True)
    monto_unitario = models.DecimalField(max_digits=17, decimal_places=2)
    total = models.DecimalField(max_digits=17, decimal_places=2)



