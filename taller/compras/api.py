from compras.models import *
from rest_framework import status
from rest_framework.parsers import JSONParser
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth.models import Group, User, Permission
from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view, permission_classes
from rest_framework import permissions
from django.utils import timezone



class DepartamentosUsuarios(APIView):
    permission_classes = [AllowAny]
    parser_classes = [JSONParser]

    @staticmethod
    def post(request):
        print(request.data['grupo'], request.data['usuarios'])
        grupo = Departamentos.objects.get(pk=request.data['grupo'], estado="A")
        grupo.usuarios.clear()
        #grupo.usuarios.through.objects.filter(id=grupo.id).delete()
        #User.groups.through.objects.filter(group=grupo).delete()
        for a in request.data['usuarios']:
            usuario = User.objects.get(pk=a)
            grupo.usuarios.add(usuario)
            grupo.save()
        return Response(status=status.HTTP_201_CREATED)

def separador_de_miles(numero):
    numero = str(format(float(numero), '.3f'))
    vector = numero.split(".")
    s = vector[0]
    for i in range(len(vector[0]), 0, -3):
        if i == 1 and s[0] == "-":
            s = s[:i] + s[i:]
        else:
            s = s[:i] + "." + s[i:]
    a = s[:len(s) - 1]
    if len(vector) == 2:
        a += "," + vector[1]
    return a


def separador_de_miles_entero(numero):
    numero = str(numero)
    vector = numero.split(".")
    s = vector[0]
    for i in range(len(vector[0]), 0, -3):
        if i == 1 and s[0] == "-":
            s = s[:i] + s[i:]
        else:
            s = s[:i] + "." + s[i:]
    a = s[:len(s) - 1]
    return a


@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def definicionec(request):
    try:
        if request.method == 'POST':
            for dato in request.data:
                presupuesto = PresupuestoCompra.objects.get(pk=dato['presupuesto'])
                preorden = PreOrdenCompra()
                preorden.presupuesto = presupuesto
                preorden.solicitud = presupuesto.solicitud
                preorden.fecha = timezone.now().date()
                preorden.creado_por = request.user
                preorden.creado_el = timezone.now()
                preorden.save()
                for datito in dato['detalle']:
                    detalle = PresupuestoDetalleCompra.objects.get(pk=datito)
                    preordendetalle = PreOrdenDetalleCompra()
                    preordendetalle.preorden = preorden
                    preordendetalle.presupuestodetalle = detalle
                    preordendetalle.item = detalle.item
                    preordendetalle.cantidad = detalle.cantidad
                    if detalle.moneda == "DL":
                        preordendetalle.precio_unitario = detalle.precio_unitario
                        preordendetalle.precio_unitario_extranjera = detalle.precio_unitario_extranjera
                    else:
                        preordendetalle.precio_unitario = detalle.precio_unitario
                        preordendetalle.precio_unitario_extranjera = 0
                    preordendetalle.moneda = detalle.moneda
                    preordendetalle.save()
            if len(request.data) > 0:
                solicitud = PresupuestoCompra.objects.get(pk=dato['presupuesto'])
                solicitud.solicitud.estado = "PAO"  # PENDIENTE DE APROBACION DE ORDEN DE COMPRA
                solicitud.solicitud.modificado_por = request.user
                solicitud.solicitud.modificado_el = timezone.now()
                solicitud.solicitud.save()
                estado = SolicitudAprobacion()
                estado.solicitud = solicitud.solicitud
                estado.usuario = request.user
                estado.estado = 'PSEC'  # PRESUPUESTOS SELECCIONADOS POR ENCARGADO DE COMPRA
                estado.creado_el = timezone.now()
                estado.save()
        return Response({"message": "OK"})
    except:
        return Response({"message": "FAIL"})



@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def buscar_ordenes(request):
    try:
        if request.method == 'POST':
            print("1")
            oc_nro = request.data['oc_nro']
            periodo = request.data['periodo']
            print( 'busqoc', oc_nro)
            print( 'periodo',periodo)
            proveedor = int(request.data['proveedor'])
            orden = OrdenCompra.objects.filter(numero=int(oc_nro), periodo=int(periodo),
                                               presupuesto__proveedor__pk=proveedor, estado="A")
            codigo = None
            mensaje = None
            result = []
            data = []
            print( 'busqoc', oc_nro)
            print( 'ordenCom',orden)
            print( 'proveedore', proveedor)
            print("orden: ", orden)
            if orden:
                orden = orden[0]
                codigo = 1
                mensaje = ""
                detalles = OrdenDetalleCompra.objects.filter(orden=orden)
                print("detalles: ", detalles)
                for detalle in detalles:
                    print ("faltante: ", detalle.faltante())

                    if not detalle.faltante() <= 0:
                        result.append(detalle)
                    x = {
                        "orden":detalle.orden.id,
                        "orden_nombre": oc_nro,
                        "item_numero":detalle.item.id,
                        "item_nombre": detalle.item.nombre_corto,
                        "precio_unitario": detalle.precio_unitario,
                        "monto": detalle.precio_unitario,
                        "cantidad": detalle.cantidad
                      
                    }
                    data.append(x)
                print("salio del for")    
            else:
                codigo = 0
                mensaje = "No se encontro la orden <strong>"+request.data['oc_nro']+"</strong> para el proveedor " \
                          "<strong>" + str(Proveedores.objects.get(proveedor=proveedor)) + "</strong>"
            print (data)
        return Response({"message": "OK", 
                        "codigo": codigo, 
                        "mensaje": mensaje, 
                        "data": data
                        })
    except:
        return Response({"message": "FAIL"})


@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
def listado_ordenes(request, proveedor):
    print("proveedor: ", proveedor)
    verificacion = FacturasProveedorConceptos.objects.filter(factura__proveedor=proveedor)
    orden = []
    for c in verificacion:
        print(c.orden)
        orden.append(c.orden)

    print(orden)
    ordenes = OrdenCompra.objects.filter(presupuesto__proveedor=proveedor, estado="A").exclude(id__in=orden)

    print("orden: ", ordenes)
    data = []
    for a in ordenes:
        a = {
            "orden": a.numero,
            "fecha": a.fecha,
            "total": a.total,
            "periodo": a.periodo
        }
        data.append(a)


    return Response({"data": data})

@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
def listado_ordenes(request, proveedor):
    verificacion = NotaCreditoProveedor.objects.filter(proveedor=proveedor)
    orden = []
    for c in verificacion:
        print(c.orden)
        orden.append(c.orden)

    print(orden)
    ordenes = OrdenCompra.objects.filter(presupuesto__proveedor=proveedor).exclude(numero__in=orden)

    print("orden: ", ordenes)
    data = []
    for a in ordenes:
        a = {
            "orden": a.numero,
            "fecha": a.fecha,
            "total": a.total,
        }
        data.append(a)


    return Response({"data": data})


@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
def listado_ordenes_agregar(request, orden):
    print("cccc: ", orden)
    ordenes = OrdenCompra.objects.get(numero=orden)

    det = OrdenDetalleCompra.objects.filter(orden=ordenes.id)

    print("orden: ", ordenes)
    data = []
    for a in det:
        cafe = {
            "id": a.id,
            "orden": ordenes.numero,
            "item_num": a.item.id,
            "item_detalles": a.item_nombre,
            "cantidad": a.cantidad,
            "monto": a.precio_unitario,
            "total_item": (a.cantidad * a.precio_unitario),

        }
        data.append(cafe)



    return Response({
                        "data": data
                        })



@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
def listado_ordenesdebitos(request, proveedor):
    verificacion = NotaDebitoProveedor.objects.filter(proveedor=proveedor)
    orden = []
    for c in verificacion:
        print(c.orden)
        orden.append(c.orden)

    print(orden)
    ordenes = OrdenCompra.objects.filter(presupuesto__proveedor=proveedor).exclude(numero__in=orden)

    print("orden: ", ordenes)
    data = []
    for a in ordenes:
        a = {
            "orden": a.numero,
            "fecha": a.fecha,
            "total": a.total,
        }
        data.append(a)


    return Response({"data": data})


@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
def listado_ordenes_agregar_debito(request, orden):
    print("cccc: ", orden)
    ordenes = OrdenCompra.objects.get(numero=orden)

    det = OrdenDetalleCompra.objects.filter(orden=ordenes.id)

    print("orden: ", ordenes)
    data = []
    for a in det:
        # if a.id not in data["id"]:
        cafe = {
            "id": a.id,
            "orden": ordenes.numero,
            "item_num": a.item.id,
            "item_detalles": a.item_nombre,
            "cantidad": a.cantidad,
            "monto": a.precio_unitario,
            "total_item": (a.cantidad * a.precio_unitario),

        }
        data.append(cafe)
    # print("vvvvv_. ", data[0])
    # for i in data:
    #     print("vvvv", i)


    return Response({
                        "data": data
                        })