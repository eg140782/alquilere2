from django.shortcuts import render
from cgi import print_arguments
from multiprocessing import context
from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic
from compras.models import *
from datetime import datetime
from compras.forms import * 
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.utils import timezone

from django.forms import inlineformset_factory
from django.forms.formsets import formset_factory
# from compras.api import separador_de_miles, separador_de_miles_entero
from django.db.models import ProtectedError, Sum
from django.db.models import Q
# from stock.models import Deposito, Lote,Stock, InventarioRecepcion
import pytz
from compras.api import *
import pdfkit 
from compras.utils import render_to_pdf
from django.http import HttpResponse
from django.template.loader import render_to_string
import os
# Create your views here.
def compras_v(request):
    modulo = request.GET.get('module','')
    print(modulo)
    request.session['modulo'] = modulo
    return render(request, "compras/compras_v.html")


class MarcasView(LoginRequiredMixin, generic.ListView):
    template_name = "marca/marca_list.html"
    context_object_name = "obj"
    queryset = Marca.objects.all().order_by('-id')
    login_url = 'login'
    model = Marca

class MarcasCrear(LoginRequiredMixin, generic.CreateView):
    model = Marca
    login_url = 'login'
    form_class = Marcas_Form
    success_url = reverse_lazy('MarcasView')
    template_name = 'marca/marca_crear.html'


    def form_valid(self, Marcas_Form):
        object = Marcas_Form.save(commit=False)
        object.estado = 'A'
        object.creado_por = self.request.user
        object.creado_el = timezone.now()
        object.save()
        return HttpResponseRedirect(self.success_url)

class MarcasUpdate(LoginRequiredMixin, generic.UpdateView):
    model = Marca
    login_url = 'login'
    form_class = Marcas_Form
    success_url = reverse_lazy('MarcasView')
    template_name = 'marca/marca_crear.html'


    def form_valid(self, Marcas_Form):
        object = Marcas_Form.save(commit=False)
        object.estado = 'A'
        object.modificado_por = self.request.user
        object.modificado_el = timezone.now()
        object.save()
        return HttpResponseRedirect(self.success_url)


class marca_delete(LoginRequiredMixin, generic.DeleteView):
    template_name = "marca/marca_delete.html"
    context_object_name = "obj"
    success_url = reverse_lazy("MarcasView")
    login_url = 'login'
    model = Marca
    def delete(self,  request, pk, *args, **kwargs):
        print("entro prro")
        self.object = self.get_object()
        self.object.delete()
        print("Guardo")   
        return HttpResponseRedirect(self.success_url)


def activar_estado(request, pk=None):
    success_url = reverse_lazy("MarcasView")
    c = Marca.objects.get(id=pk)
    c.estado = "A"
    c.save()


    return HttpResponseRedirect(success_url)

def inactivar_estado(request, pk=None):
    success_url = reverse_lazy("MarcasView")
    c = Marca.objects.get(id=pk)
    c.estado = "I"
    c.save()
    

    return HttpResponseRedirect(success_url)

class ProveedoresView(LoginRequiredMixin, generic.ListView):
    template_name = "proveedor/proveedor_list.html"
    context_object_name = "obj"
    queryset = Proveedores.objects.filter(estado="A").order_by('-id')
    login_url = 'login'
    model = Proveedores


def ProveedorCreateUpdate(request, pk=None):
    if pk:
        main = Proveedores.objects.get(id=pk)
        detalle = inlineformset_factory(Proveedores, CuentasProveedor,
                                        form=CuentasProveedorForm,
                                         extra=1,  can_delete=True)
    else:
        main = Proveedores()
    
        detalle = inlineformset_factory(Proveedores, CuentasProveedor,
                                        form=CuentasProveedorForm,
                                         extra=1,  can_delete=True)
    if request.method == 'POST':
        form = ProveedorForm(request.POST, request.FILES, instance=main,  prefix='mains')
        formset = detalle(request.POST, instance=main, prefix='detalle')
        print(form.errors)
        print(formset.errors)
        if form.is_valid() and formset.is_valid():
            cabecera = form.save(commit=False)
            if pk:
                cabecera.usuario_modificacion = request.user
                cabecera.fecha_modificacion =timezone.now()
                cabecera.estado ="A"
                cabecera.save()
            else:
                cabecera.usuario_insercion = request.user
                cabecera.fecha_insercion =timezone.now()
                cabecera.estado ="A"
                cabecera.save()
            print("guardo cabecera")
            formset.save() 
            detalles = formset.save(commit=False)
            for detalle in detalles:
                detalle.save()
            print("guardo detalles")
            # print(formset.deleted_objects)
            for obj in formset.deleted_objects:
                print("entra aki")
                obj.delete()
            # print("elimino")

            return HttpResponseRedirect('/App/compras/proveedor/listado/')

    else:
        form = ProveedorForm(instance=main, prefix='mains')
        formset = detalle(instance=main, prefix='detalle') 


    return render(request,'proveedor/proveedor_create.html',
                                  {
                                    'form':form,
                                    'formset':formset

                                        
                                    },
                                  )


def aviso_proveedor(request):
    template_name = "alerta/aviso_proveedor.html"
    return render(request, template_name)

class Proveedordelete(LoginRequiredMixin, generic.DeleteView):
    template_name = 'proveedor/proveedor_delete.html'
    context_object_name = "obj"
    success_url = reverse_lazy('ProveedoresView')
    login_url = 'login'
    model = Proveedores
    def delete(self,  request, pk, *args, **kwargs):
        print("entro prro")
        # check = PresupuestoCompra.objects.filter(proveedor=self.get_object())
        # if check:
        #     reverse_lazy("aviso_proveedor")
        # else:
        self.object = self.get_object()
        self.object.usuario_modificacion = self.request.user
        self.object.fecha_modificacion = timezone.now()
        self.object.estado = "I"
        # self.object.delete()
        self.object.save()
        print("Guardo")   
        return HttpResponseRedirect(self.success_url)



class ProveedorDetalle(LoginRequiredMixin,  generic.DetailView): 
    template_name = 'proveedor/proveedor_detail.html'
    context_object_name = "form"
    success_url = reverse_lazy('ProveedoresView')
    login_url = 'login'
    model = Proveedores

    def get_context_data(self, **kwargs):
        context = super(ProveedorDetalle, self).get_context_data(**kwargs)
        c = CuentasProveedor.objects.filter(cabecera=self.object)

        print(c )
        context["detalles"] = CuentasProveedor.objects.filter(cabecera=self.object)
        return context
    

class ItemList(LoginRequiredMixin, generic.ListView):
    template_name = "producto/item_list.html"
    # context_object_name = "obj"
    # queryset = Item.objects.all().order_by('-id')
    login_url = 'login'
    model = Item

    def get_context_data(self, **kwargs):
        context = super(ItemList, self).get_context_data(**kwargs)
        categoria = self.request.GET.get('categoria', None)
        # hasta = self.request.GET.get('hasta', None)
        print(categoria)
       
        
        queryset = Item.objects.all().order_by('-id')
        if categoria:
            queryset = queryset.filter(categoria=categoria)
            print("hola: ", queryset)
        # if desde and hasta:
        #     desde_ = datetime.strptime(desde, "%Y-%m-%d")
        #     hasta_ = datetime.strptime(hasta, "%Y-%m-%d")
        #     print(desde_, hasta_)
        #     desde_aware = timezone.make_aware(desde, timezone.utc)
        #     hasta_aware = timezone.make_aware(hasta, timezone.utc)
        #     queryset =  Item.objects.filter(Q(creado_el__range=[desde_aware, hasta_aware]))
        
        context['obj'] = queryset
        context['request'] = self.request
        # context['form'] = Filtrolistadoitems
        return context

class ItemsCrear(LoginRequiredMixin, generic.CreateView):
    model = Item
    login_url = 'login'
    form_class = ItemForm
    success_url = reverse_lazy('ItemList')
    template_name = 'producto/Item_nueva.html'


    def form_valid(self, PreItemForm):
        object = PreItemForm.save(commit=False)
        object.creado_por = self.request.user
        object.creado_el = timezone.now()
        object.save()
        return HttpResponseRedirect(self.success_url)


class ItemUpdate(LoginRequiredMixin, generic.UpdateView):
    model = Item
    login_url = 'login'
    form_class = ItemForm
    success_url = reverse_lazy('ItemList')
    template_name = 'producto/Item_nueva.html'

    def form_valid(self, ItemForm):
        object = ItemForm.save(commit=False)
        object.modificado_por = self.request.user
        object.modificado_el =timezone.now()
        object.save()
        return HttpResponseRedirect(self.success_url)

class Itemdelete(LoginRequiredMixin, generic.DeleteView):
    template_name = 'producto/item_delete.html'
    context_object_name = "obj"
    success_url = reverse_lazy("ItemList")
    login_url = 'login'
    model = Item
    def delete(self,  request, pk, *args, **kwargs):
        print("entro prro")
        # check = RolUsuarioCompras.objects.filter(usuario=self.object.usuario,
        #                                          rol=self.object.rol,estado="A")
        # print(check)
        self.object = self.get_object()
        self.object.modificado_por = self.request.user
        self.object.modificado_el = timezone.now()
        self.object.estado = "I"
        # self.object.delete()
        self.object.save()
        print("Guardo")   
        return HttpResponseRedirect(self.success_url)

class ItemDetalle(LoginRequiredMixin,  generic.DetailView): 
    template_name = 'producto/item_detail.html'
    context_object_name = "obj"
    success_url = reverse_lazy('PreItemList')
    login_url = 'login'
    model = Item 


class CategoriaList(LoginRequiredMixin, generic.ListView):
    template_name = "categoria/categoria_list.html"
    context_object_name = "obj"
    queryset = CategoriasStock.objects.all().order_by('-id')
    login_url = 'login'
    model = CategoriasStock



class CategoriaCrear(LoginRequiredMixin, generic.CreateView):
    model = CategoriasStock
    login_url = 'login'
    form_class = Categorias_Form
    success_url = reverse_lazy('CategoriaList')
    template_name = 'categoria/categoria_nueva.html'


    def form_valid(self, Categorias_Form):
        object = Categorias_Form.save(commit=False)
        object.creado_por = self.request.user
        object.creado_el = timezone.now()
        object.save()
        return HttpResponseRedirect(self.success_url)


class CategoriaUpdate(LoginRequiredMixin, generic.UpdateView):
    model = CategoriasStock
    login_url = 'login'
    form_class = Categorias_Form
    success_url = reverse_lazy('CategoriaList')
    template_name = 'categoria/categoria_nueva.html'


    def form_valid(self, Categorias_Form):
        object = Categorias_Form.save(commit=False)
        object.modificado_por = self.request.user
        object.modificado_el = timezone.now()
        object.save()
        return HttpResponseRedirect(self.success_url)


def aviso_categoria(request):
    template_name = "alerta/aviso.html"
    return render(request, template_name)

class Categoriadelete(LoginRequiredMixin, generic.DeleteView):
    template_name = "categoria/categoria_delete.html"
    context_object_name = "obj"
    success_url = reverse_lazy("CategoriaList")
    login_url = 'login'
    model = CategoriasStock
    def delete(self,  request, pk, *args, **kwargs):
        print("entro prro")
        redireccion = reverse_lazy("aviso_categoria")
        consulta = Item.objects.filter(categoria=self.get_object())
        if consulta:
            reverse_lazy("aviso_categoria")
        else:
            self.object = self.get_object()
            self.object.delete()
            print("Guardo")   
            return HttpResponseRedirect(self.success_url)



class DepartamentoListar(LoginRequiredMixin, generic.ListView):
    template_name = 'departamentos/departamento_listar.html'
    context_object_name = "obj"
    queryset = Departamentos.objects.filter(estado="A").order_by('-id')
    login_url = 'login'
    model = Departamentos


class DepartamentoCrear(LoginRequiredMixin, generic.CreateView):
    model = Departamentos
    login_url = 'login'
    form_class = DepartamentoForm
    success_url = reverse_lazy('DepartamentoListar')
    template_name = 'departamentos/departamentos_crear.html'


    def form_valid(self, DepartamentoForm):
        object = DepartamentoForm.save(commit=False)
        object.estado = 'A'
        object.modificado_por = self.request.user
        object.modificado_el = timezone.now()
        object.save()        
        return HttpResponseRedirect(self.success_url)

class DepartamentoUpdate(LoginRequiredMixin, generic.UpdateView):
    model = Departamentos
    login_url = 'login'
    form_class = DepartamentoForm
    success_url = reverse_lazy('DepartamentoListar')
    template_name = 'departamentos/departamentos_crear.html'

    def form_valid(self, DepartamentoForm):
        object = DepartamentoForm.save(commit=False)
        object.estado = 'A'
        object.modificado_por = self.request.user
        object.modificado_el = timezone.now()
        object.save()
        return HttpResponseRedirect(self.success_url)



class Departamentodelete(LoginRequiredMixin, generic.DeleteView):
    template_name = 'departamentos/departamentos_delete.html'
    context_object_name = "obj"
    success_url = reverse_lazy("DepartamentoListar")
    login_url = 'login'
    model = Departamentos
    def delete(self,  request, pk, *args, **kwargs):
        print("entro prro")
        self.object = self.get_object()
        self.object.modificado_por = self.request.user
        self.object.modificado_el = timezone.now()
        self.object.estado = "I"
        # self.object.delete()
        self.object.save()
        print("Guardo")   
        return HttpResponseRedirect(self.success_url)


def DepartamentoUsuarios(request,pk):
    grupo = Departamentos.objects.get(pk=pk, estado="A")
    usuarios_grupo = grupo.usuarios.all()
    usuarios = []

    aux1 = User.objects.filter(is_active=True)
    print(aux1)
    for b in aux1:
        if b not in usuarios_grupo and not b == grupo.responsable:
            usuarios.append(b)

    data_context = {
        'request': request,
        'usuarios': usuarios,
        'usuarios_grupo': usuarios_grupo,
        'grupo': grupo,
    }
    return render(request, 'departamentos/departamentos_usuarios.html', data_context)

def aviso_roles(request):
    template_name = "alerta/aviso_roles.html"
    return render(request, template_name)

class RolesListar(LoginRequiredMixin, generic.ListView):
    template_name = 'roles_usuarios/roles_listar.html'
    context_object_name = "obj"
    queryset = RolesUsuarios.objects.all().order_by('-id')
    login_url = 'login'
    model = RolesUsuarios

class RolesCrear(LoginRequiredMixin, generic.CreateView):
    model = RolesUsuarios
    login_url = 'login'
    form_class = RolesUsuarioForm
    success_url = reverse_lazy('RolesListar')
    template_name = 'roles_usuarios/roles_crear.html'


    def form_valid(self, form):
        # super().form_valid(RolesUsuarioForm)
        
        # check = RolesUsuarios.objects.filter(usuario=self.object.usuario,
        #                                          rol=self.object.rol,estado="A")
        # print(check)
        # if check:    
        #     print("entra k")
        #     return HttpResponseRedirect(reverse_lazy("aviso_roles"))
        # else:
        #     object = RolesUsuarioForm.save(commit=False)
        #     print (self.object.usuario, self.object.rol)
        #     object.creado_por = self.request.user
        #     object.creado_el = timezone.now()
        #     object.save()
        #     return HttpResponseRedirect(self.success_url)
        check = RolesUsuarios.objects.filter(usuario=form.instance.usuario, rol=form.instance.rol, estado="A")
        if check.exists():
            return HttpResponseRedirect(reverse_lazy("aviso_roles"))
        
        # Si no se encontró un rol existente, guarda los datos
        form.instance.creado_por = self.request.user
        form.instance.creado_el = timezone.now()
        return super().form_valid(form)


class RolUpdate(LoginRequiredMixin, generic.UpdateView):
    model = RolesUsuarios
    login_url = 'login'
    form_class = RolesUsuarioForm
    success_url = reverse_lazy('RolesListar')
    template_name = 'roles_usuarios/roles_crear.html'

    def form_valid(self, RolesUsuarioForm):
        object = RolesUsuarioForm.save(commit=False)
        object.modificado_por = self.request.user
        object.modificado_el =timezone.now()
        object.save()
        return HttpResponseRedirect(self.success_url)

class Roldelete(LoginRequiredMixin, generic.DeleteView):
    template_name = 'roles_usuarios/roles_delete.html'
    context_object_name = "obj"
    success_url = reverse_lazy("RolesListar")
    login_url = 'login'
    model = RolesUsuarios
    def delete(self,  request, pk, *args, **kwargs):
        print("entro prro")
        self.object = self.get_object()
        self.object.modificado_por = self.request.user
        self.object.modificado_el = timezone.now()
        self.object.estado = "I"
        # self.object.delete()
        self.object.save()
        print("Guardo")   
        return HttpResponseRedirect(self.success_url)


def SolicitudComprasList(request):
    a_roles = []
    roles = RolesUsuarios.objects.filter(usuario=request.user,estado="A")

    responsable = []
    responsable_ob = Departamentos.objects.filter(responsable=request.user, estado="A")
    for res in responsable_ob:
        responsable.append(res.pk)
    estado = request.GET.get('estado', None)
    print(estado)
    desde = request.GET.get('desde', None)
    hasta = request.GET.get('hasta', None)
    form = FiltrolistadoCompras(initial={'desde': desde, 'hasta': hasta})
    datos = SolicitudCompra.objects.all().exclude(estado="I").order_by('id')

    if desde and hasta:
        datos = datos.filter(fecha__range=[desde, hasta]).exclude(estado="I")
    
    if estado:
        datos = datos.filter(estado=estado).exclude(estado="I")
    
    if len(responsable) >0:
        res = 1
    else:
        res = 0

    user = request.user
    print(user)

    return render(request, "compras_solicitud/lista_solicitudes.html", 
                    {
                        "obj":datos,
                        "responsable": responsable,
                        "usuario":user,
                        "res":res,
                        "roles":roles,
                        "form":form


                    })


def SolicitudComprasCreateUpdate(request, id=None):
    if id:
        main = SolicitudCompra.objects.get(pk=id)
        detalle = inlineformset_factory(SolicitudCompra, SolicitudCompraDetalle,
                                         form=SolicituComprasDetalleForm,
                                         can_delete=True, extra=0)
    else:
        main = SolicitudCompra()
        main.creado_el = timezone.now().strftime('%Y-%m-%d')
        detalle = inlineformset_factory(SolicitudCompra, SolicitudCompraDetalle,
                                                form=SolicituComprasDetalleForm,
                                                can_delete=True, extra=1)

    if request.method == 'POST':
        form = SolicituComprasForm(request.POST, request.FILES, instance=main, prefix='mains')
        formset = detalle(request.POST, request.FILES, instance=main, prefix='nesteds')
        if form.is_valid() and formset.is_valid():
            cabecera = form.save(commit=False)
            cabecera.creado_por = request.user
            cabecera.fecha = timezone.now()
            
            responsable = Departamentos.objects.filter(responsable=request.user, estado="A")
            print("guardo cabecera")
            if len(responsable)>0:
                cabecera.estado = 'PAEC'# PENDIENTE DE APROBACION ENCARGADO DE COMPRA
            else:
                cabecera.estado = 'PAD'  # PENDIENTE DE APROBACION RESPONSABLE DE AREA
            cabecera.save()
            detalles = formset.save(commit=False)
            for detalle in detalles:
                detalle.solicitud = cabecera
                detalle.save()
            for obj in formset.deleted_objects:
                obj.delete()
            print("guardo detalles")

            estado = SolicitudAprobacion()
            estado.solicitud = cabecera
            estado.usuario = request.user
            if not id == None:
                estado.estado = 'MPF'  # MODIFICADO POR FUNCIONARIO
            else:
                estado.estado = 'CPF'# CREADO POR FUNCIONARIO
            estado.creado_el = timezone.now()
            estado.save()
            print("guardo estados")
            return HttpResponseRedirect('/App/compras/solicitud/listado/')

    
    else:
        form = SolicituComprasForm(instance=main, prefix='mains')
        formset = detalle(instance=main, prefix='nesteds')

    return render(request, "compras_solicitud/solicitud_nueva.html", 
                    {
                        'form':form,
                        'formset':formset
                    })



def detalles_solicitudCompras(request, pk=None):
    cabecera = SolicitudCompra.objects.filter(id=pk)
    estados = SolicitudAprobacion.objects.filter(solicitud=pk).order_by('creado_el')
    detalles = SolicitudCompraDetalle.objects.filter(solicitud=pk)
    return render(request,'compras_solicitud/detalle_solicitud.html',
                                  {
                                    'request': request,
                                    'cabecera':cabecera,
                                    'estados':estados,
                                    'detalles':detalles

                                  },
                                )


def RevisionRA(request, id=None):
    print (SolicitudCompra.objects.get(id=id).estado)
    solicitud = SolicitudCompra.objects.get(id=id)
    detalles = SolicitudCompraDetalle.objects.filter(solicitud=solicitud)
    if request.method == 'POST':
        form = AprobacionUnoResponsableAreaForm(request.POST)
        if form.is_valid():
            print(form['motivo'].value())
            if form['aprobado'].value() == "False":
                solicitud = SolicitudCompra.objects.get(id=id)
                solicitud.estado = "PRS" #PENDIENTE DE REVISION DEL SOLICITANTE
                solicitud.modificado_por = request.user
                solicitud.modificado_el = timezone.now()
                solicitud.save()
                estado = SolicitudAprobacion()
                estado.solicitud = solicitud
                estado.usuario = request.user
                estado.comentario = form['motivo'].value()
                estado.estado = 'RRRA'  # RECHAZADO EN REVISION POR RESPONSABLE DEL AREA
                estado.creado_el = timezone.now()
                estado.save()
            else:
                solicitud = SolicitudCompra.objects.get(id=id)

                solicitud.estado = "PAEC"  # PENDIENTE DE APROBACION DE ENCARGADO DE COMPRA
                solicitud.modificado_por = request.user
                solicitud.modificado_el = timezone.now()
                solicitud.save()
                estado = SolicitudAprobacion()
                estado.solicitud = solicitud
                estado.usuario = request.user
                estado.estado = 'ARRA'  # APROBADO EN REVISION POR RESPONSABLE DEL AREA
                estado.comentario = form['motivo'].value()
                estado.creado_el = timezone.now()
                estado.save()
            return HttpResponseRedirect('/App/compras/solicitud/listado/')
    else:
        form = AprobacionUnoResponsableAreaForm()



    return render(request, "compras_solicitud/AprobacionRA.html", 
                    {
                        'solicitud':solicitud,
                        'detalles':detalles,
                        'form':form
                        
                    })


def RevisionEC(request,id=None):
    roles = RolesUsuarios.objects.filter(usuario=request.user, rol="EC", estado="A")
    if len(roles)>0:
        if request.method == 'POST':
            form = AprobacionEncargadoCompraForms(request.POST)
            if form.is_valid():
                if form['aprobado'].value() == "False":
                    solicitud = SolicitudCompra.objects.get(id=id)
                    solicitud.estado = "PRS" #PENDIENTE DE REVISION DEL SOLICITANTE
                    solicitud.modificado_por = request.user
                    solicitud.modificado_el = timezone.now()
                    solicitud.save()
                    estado = SolicitudAprobacion()
                    estado.solicitud = solicitud
                    estado.usuario = request.user
                    estado.comentario = form['motivo'].value()
                    estado.estado = 'RREC'  # RECHAZADO EN REVISION POR ENCARGADO DE COMPRA
                    estado.creado_el = timezone.now()
                    estado.save()
                    # content = {'id': solicitud.pk}
                    # Channel('send_aviso_rechazo').send(content, False)
                else:
                    solicitud = SolicitudCompra.objects.get(id=id)
                    solicitud.estado = "PCP"  # PENDIENTE DE CARGA DE PRESUPUESTOS
                    solicitud.modificado_por = request.user
                    solicitud.modificado_el = timezone.now()
                    solicitud.save()
                    estado = SolicitudAprobacion()
                    estado.solicitud = solicitud
                    estado.usuario = request.user
                    estado.comentario = form['motivo'].value()
                    estado.estado = 'AREC'  # APROBADO EN REVISION POR ENCARGADO DE COMPRA
                    estado.creado_el = timezone.now()
                    estado.save()
                return HttpResponseRedirect('/App/compras/solicitud/listado/')
        else:
            form = AprobacionEncargadoCompraForms()
            solicitud = SolicitudCompra.objects.get(id=id)
            detalles = SolicitudCompraDetalle.objects.filter(solicitud=solicitud)
        return render(request, 'compras_solicitud/aprobacionEncargadoCompras.html',
                                    {
                                        'form': form,
                                        'request': request,
                                        'solicitud': solicitud,
                                        'detalles': detalles
                                    },
                                  )
    else:
        return HttpResponseRedirect('/App/compras/solicitud/listado/')
    


def CargaPresupuestosEncargadoComprasView(request,id=None):
    roles = RolesUsuarios.objects.filter(usuario=request.user, rol="EC", estado="A")
    if len(roles)>0:
        item = request.GET.get('item','')
        current_item = None
        next_item = None
        solicitud = SolicitudCompra.objects.get(id=id)
        print("arrecha: ", solicitud)
        if item == "":
            print("arriba")
            item_obj_1 = SolicitudCompraDetalle.objects.filter(solicitud=solicitud).order_by('id')
            if len(item_obj_1)>0:
                print("arriba")
                current_item = item_obj_1[0]
            if not current_item == None:
                print("abajouuu")
                item_obj_2 = SolicitudCompraDetalle.objects.filter(solicitud=solicitud, pk__gt=current_item.pk).exclude(pk=current_item.pk).order_by('id')
                if len(item_obj_2)>0:
                    next_item = item_obj_2[0]
        else:
            print("abajo")
            item_obj_1 = SolicitudCompraDetalle.objects.filter(solicitud=solicitud, pk=item).order_by('id')
            print (item_obj_1)
            if len(item_obj_1) > 0:
                current_item = item_obj_1[0]
            if not current_item == None:
                item_obj_2 = SolicitudCompraDetalle.objects.filter(solicitud=solicitud, pk__gt=current_item.pk).exclude(pk=current_item.pk).order_by('id')
                print (item_obj_2)
                if len(item_obj_2) > 0:
                    next_item = item_obj_2[0]
        print ("current_item: ", current_item)
        print ("next_item: ", next_item)

        presupuestos_existentes = PresupuestoCompra.objects.filter(solicitud=solicitud)
        print(" jjjj: ", presupuestos_existentes)
        if len(presupuestos_existentes) > 0:
            formset_proveedores = formset_factory(PresupuestoDetalleCompraCustomForm,
                                                  formset=PresupuestoDetalleCompraCustomDetailForm, extra=1, can_delete=True)
        else:
            formset_proveedores = formset_factory(PresupuestoDetalleCompraCustomForm,
                                              formset=PresupuestoDetalleCompraCustomDetailForm, extra=1, can_delete=True)
        if request.method == "POST":
            formset = formset_proveedores(request.POST, prefix='nesteds')
            print("errors: ", formset.errors)
            if formset.is_valid():
                if request.POST.get('action') == "next":
                    for form in formset.deleted_forms:
                        proveedor = Proveedores.objects.filter(proveedor=form['proveedor'].value())
                        if len(proveedor)>0:
                            proveedor = proveedor[0]
                            presupuesto_existente = PresupuestoCompra.objects.filter(solicitud=solicitud, proveedor=proveedor)
                            if len(presupuesto_existente) > 0:
                                PresupuestoDetalleCompra.objects.filter(presupuesto=presupuesto_existente[0], item=current_item.item,
                                                                        cantidad=current_item.cantidad).delete()
                                otros_items = PresupuestoDetalleCompra.objects.filter(presupuesto=presupuesto_existente[0])
                                if len(otros_items) == 0:
                                    presupuesto_existente[0].delete()
                    for form in formset:
                        if not form in formset.deleted_forms:
                            proveedor = Proveedores.objects.filter(id=form['proveedor'].value())
                            if len(proveedor)>0:
                                proveedor = proveedor[0]
                                presupuesto_existente = PresupuestoCompra.objects.filter(solicitud=solicitud, proveedor=proveedor)
                                if len(presupuesto_existente) == 0:
                                    cabecera = PresupuestoCompra()
                                    cabecera.solicitud = solicitud
                                    cabecera.proveedor = proveedor
                                    cabecera.creado_por = request.user
                                    cabecera.creado_el = timezone.now()
                                    cabecera.save()
                                    detalle = PresupuestoDetalleCompra()
                                    detalle.presupuesto = cabecera
                                    detalle.item = current_item.item
                                    detalle.moneda = form['moneda'].value()
                                    detalle.precio_unitario = form['costo_unitario'].value()
                                    detalle.cantidad = form['cantidad'].value()
                                    detalle.save()
                                else:
                                    detalle_existente = PresupuestoDetalleCompra.objects.filter(presupuesto=presupuesto_existente[0], item=current_item.item,
                                                                                                cantidad=current_item.cantidad)
                                    if len(detalle_existente) == 0:
                                        detalle = PresupuestoDetalleCompra()
                                        detalle.presupuesto = presupuesto_existente[0]
                                        detalle.item = current_item.item
                                        detalle.moneda = form['moneda'].value()
                                        detalle.precio_unitario = form['costo_unitario'].value()
                                        detalle.cantidad = form['cantidad'].value()
                                        detalle.save()
                                    else:
                                        detalle_existente[0].moneda = form['moneda'].value()
                                        detalle_existente[0].precio_unitario = form['costo_unitario'].value()
                                        detalle_existente[0].cantidad = form['cantidad'].value()
                                        detalle_existente[0].save()
                                    presupuesto_existente[0].modificado_por = request.user
                                    presupuesto_existente[0].modificado_el = timezone.now()
                                    presupuesto_existente[0].save()
                                print("llega hasta aca")
                    return HttpResponseRedirect('/App/compras/solicitud/presupuestos/encargado/compras/'+str(id)+'/?item='+str(next_item.pk))
                else:
                    for form in formset.deleted_forms:
                        print("form['proveedor'].value(): ", form['proveedor'].value())
                        proveedor = Proveedores.objects.filter(proveedor=form['proveedor'].value())
                        if len(proveedor)>0:
                            proveedor = proveedor[0]
                            presupuesto_existente = PresupuestoCompra.objects.filter(solicitud=solicitud, proveedor=proveedor)
                            if len(presupuesto_existente) > 0:
                                PresupuestoDetalleCompra.objects.filter(presupuesto=presupuesto_existente[0],item=current_item.item).delete()
                                otros_items = PresupuestoDetalleCompra.objects.filter(presupuesto=presupuesto_existente[0])
                                if len(otros_items) == 0:
                                    presupuesto_existente[0].delete()
                    for form in formset:
                        if not form in formset.deleted_forms:
                            proveedor = Proveedores.objects.filter(id=form['proveedor'].value())
                            if len(proveedor)>0:
                                proveedor = proveedor[0]
                                presupuesto_existente = PresupuestoCompra.objects.filter(solicitud=solicitud, proveedor=proveedor)
                                if len(presupuesto_existente) == 0:
                                    cabecera = PresupuestoCompra()
                                    cabecera.solicitud = solicitud
                                    cabecera.proveedor = proveedor
                                    cabecera.creado_por = request.user
                                    cabecera.creado_el = timezone.now()
                                    cabecera.save()
                                    detalle = PresupuestoDetalleCompra()
                                    detalle.presupuesto = cabecera
                                    detalle.item = current_item.item
                                    detalle.moneda = form['moneda'].value()
                                    detalle.precio_unitario = form['costo_unitario'].value()
                                    detalle.cantidad = form['cantidad'].value()
                                    detalle.save()
                                else:
                                    detalle_existente = PresupuestoDetalleCompra.objects.filter(presupuesto=presupuesto_existente[0], item=current_item.item,
                                                                                                cantidad=current_item.cantidad)
                                    if len(detalle_existente) == 0:
                                        detalle = PresupuestoDetalleCompra()
                                        detalle.presupuesto = presupuesto_existente[0]
                                        detalle.item = current_item.item
                                        detalle.moneda = form['moneda'].value()
                                        detalle.precio_unitario = form['costo_unitario'].value()
                                        detalle.cantidad = form['cantidad'].value()
                                        detalle.save()
                                    else:
                                        detalle_existente[0].moneda = form['moneda'].value()
                                        detalle_existente[0].precio_unitario = form['costo_unitario'].value()
                                        detalle_existente[0].cantidad = form['cantidad'].value()
                                        detalle_existente[0].save()
                                    presupuesto_existente[0].modificado_por = request.user
                                    presupuesto_existente[0].modificado_el = timezone.now()
                                    presupuesto_existente[0].save()
                        else:
                            form.delete()
                    return HttpResponseRedirect('/App/compras/solicitud/registro/presupuestos/encargado/compras/' + str(id))
        else:
            if len(presupuestos_existentes) > 0:
                current_items = PresupuestoDetalleCompra.objects.filter(presupuesto__in=presupuestos_existentes, item=current_item.item,
                                                                        cantidad=current_item.cantidad)
                if len(current_items) > 0:
                    ini = []
                    for x in current_items:
                        if x.moneda == 'GS':
                            print("ali")
                            ini.append({
                                'proveedor': x.presupuesto.proveedor.id,
                                'moneda': x.moneda,
                                'costo_unitario': x.precio_unitario,
                                'cantidad': separador_de_miles(x.cantidad)
                            })
                            print(ini)
                    formset = formset_proveedores(prefix='nesteds', initial=ini)
                else:
                    formset = formset_proveedores(prefix='nesteds')
            else:
                print("hola")
                formset = formset_proveedores(prefix='nesteds')
        return render(request,'Presupuestos_compras/Carga_presupuesto_EC.html',
                                  {
                                        'solicitud': solicitud,
                                        'current_item': current_item,
                                        'next_item': next_item,
                                        'formset': formset,
                                        # 'items': itemss
                                    },
                                  )
    else:
        return HttpResponseRedirect('/App/compras/solicitud/listado/')




def RegistroPresupuestosEC(request,id=None):
    roles = RolesUsuarios.objects.filter(usuario=request.user, rol="EC", estado="A")
    if len(roles)>0:
        solicitud = SolicitudCompra.objects.get(id=id)
        presupuestos_existentes = PresupuestoCompra.objects.filter(solicitud=solicitud)
        formset_presupuestos = formset_factory(PresupuestoCompraCustomForm,
                                              formset=PresupuestoCompraCustomDetailForm, extra=0, can_delete=False)
        if request.method == "POST":
            formset = formset_presupuestos(request.POST, request.FILES, prefix='nesteds')
            print (formset.errors)
            if formset.is_valid():
                for form in formset.deleted_forms:
                    presupuesto = PresupuestoCompra.objects.get(solicitud__id=id,
                                                                proveedor_id=form['proveedor_id'].value())
                    presupuesto.delete()
                for form in formset:
                    if not form in formset.deleted_forms:
                        presupuesto = PresupuestoCompra.objects.get(solicitud__id=id, proveedor_id=form['proveedor_id'].value())
                        presupuesto.fecha = form['fecha'].value()
                        presupuesto.validez = form['validez'].value()
                        presupuesto.forma_pago = form['forma_pago'].value()
                        presupuesto.disponibilidad = form['disponibilidad'].value()
                        presupuesto.adjunto = form['adjunto'].value()
                        presupuesto.modificado_por = request.user
                        presupuesto.modificado_el = timezone.now()
                        presupuesto.save()
                        detalles = PresupuestoDetalleCompra.objects.filter(presupuesto=presupuesto)
                        for detalle in detalles:
                            if detalle.moneda == 'DL':
                                detalle.precio_unitario_extranjera = detalle.precio_unitario
                                # cotizacion = int(buscar_cotizacion_diaria(form['fecha'].value())) #### si se va a utilizar se debe de crear otra tabLa de cotizzaciones
                                cotizacion = 0
                                detalle.precio_unitario = detalle.precio_unitario_extranjera*cotizacion
                            else:
                                detalle.precio_unitario_extranjera = 0
                            detalle.save()
                solicitud.estado = "PSO"  # PENDIENTE DE SELECCION DE OFERTA
                solicitud.modificado_por = request.user
                solicitud.modificado_el = timezone.now()
                solicitud.save()
                estado = SolicitudAprobacion()
                estado.solicitud = solicitud
                estado.usuario = request.user
                estado.estado = 'PCEC'  # PRESUPUESTOS CARGADOS POR ENCARGADO DE COMPRA
                estado.creado_el = timezone.now()
                estado.save()
                return HttpResponseRedirect('/App/compras/solicitud/listado/')
        else:
            formset = formset_presupuestos(prefix='nesteds', initial=[{'proveedor_id': x.proveedor.id,
                                                                       'proveedor_nombre': x.proveedor.razon_social,
                                                                       'fecha': x.fecha,
                                                                       'validez': x.validez,
                                                                       'adjunto': x.adjunto,
                                                                      } for x in presupuestos_existentes
                                                                      ])
        return render(request,'Presupuestos_compras/Presupuesto_Encargado_Compras.html',
                                  {
                                        'solicitud': solicitud,
                                        'formset': formset
                                    },
                                )

    else:
        return HttpResponseRedirect('/compras/solicitud/listado/')
    


def SeleccionarPresupuestoEC(request, id=None):
    roles = RolesUsuarios.objects.filter(usuario=request.user, rol="EC", estado="A")
    if len(roles) > 0:
        solicitud = SolicitudCompra.objects.get(id=id)
        # solicitante = Usuario.objects.get(pk=solicitud.creado_por.pk)
        items = SolicitudCompraDetalle.objects.filter(solicitud=solicitud)
        item_preordenes = PreOrdenDetalleCompra.objects.filter(preorden__solicitud=solicitud).values_list('item', flat=True)
        data = []
        proveedores = PresupuestoCompra.objects.filter(solicitud=solicitud).order_by('id'),
        print (items)
        for item in items:
            mejor = PresupuestoDetalleCompra.objects.filter(presupuesto__solicitud=solicitud, item=item.item,
                                                            cantidad=item.cantidad).order_by('precio_unitario')
            print ("mejor", mejor)
            if len(mejor):
                dato = {
                    'item_id' : item.item.pk,
                    'item_nombre': str(item.item) + " - Cant. " + str(item.cantidad),
                    'presupuestos': [],
                    'mejor': mejor[0].id,
                    'poc': 'N',
                }
                if item.item.pk in item_preordenes:
                    dato['poc'] = 'S'
                for proveedor in proveedores[0]:
                    presupuesto = PresupuestoDetalleCompra.objects.filter(presupuesto__proveedor=proveedor.proveedor,
                                                                          presupuesto__solicitud=solicitud,
                                                                          item=item.item, cantidad=item.cantidad).order_by('presupuesto__id')
                    saldos = []
                    mes = timezone.now().month
                    year = timezone.now().year
                    mostrar = False
                    print("presupuesto: ", presupuesto)
                    if len(presupuesto) > 0:
                        concepto = item.item.nombre_largo
                        saldo = None
                        
                        if saldo == None:
                            saldo = 0
                        for a_saldo in saldos:
                            if a_saldo[0] == concepto:
                                saldo -= a_saldo[1]
                        saldos.append([concepto, item.cantidad * item.item.costo_ultimo()])
                        dato['presupuestos'].append([presupuesto[0], saldo, saldo - item.cantidad * item.item.costo_ultimo()])
                    else:
                        dato['presupuestos'].append(None)
                data.append(dato)
        return render(request,'Presupuestos_compras/SeleccionarPresupuestoEC.html',
                                  {
                                    'data': data,
                                    'proveedores': proveedores[0],
                                    'colspan': len(proveedores[0]),
                                    'item_preordenes': item_preordenes,
                                    'mostrar': mostrar,
                                    },
                                )
    else:
        return HttpResponseRedirect('/App/compras/solicitud/listado/')



# class AnularSolicitudCompras(LoginRequiredMixin, generic.DeleteView):
#     template_name = "compras_solicitud/solicitudcompras_anular.html"
#     context_object_name = "obj"
#     success_url = reverse_lazy("SolicitudComprasList")
#     login_url = 'login'
#     model = SolicitudCompra

#     def get_context_data(self, **kwargs):
#         context = super(AnularSolicitudCompras, self).get_context_data(**kwargs)
#         context["detalles"] = SolicitudCompraDetalle.objects.filter(solicitud=self.object)
#         return context

#     def delete(self,  request, pk, *args, **kwargs):
#         self.object = self.get_object()
#         try:
#             self.object.estado = "I"
#             self.object.modificado_por = self.request.user
#             self.object.modificado_el = timezone.now()
#             self.object.save()
#             data = 'El registro ha sido anulado correctamente!'

#         except ProtectedError:
#             data = 'No es posible anular este registro, tiene dependencias con otros registros'
        
#         return render(request,"alerta/aviso_compras.html",
#                                 {
#                                     'data': data,
#                                 },
#                             )

def AnularSolicitudCompras(request, pk=None):
    obj = SolicitudCompra.objects.get(id=pk)
    detalles = SolicitudCompraDetalle.objects.filter(solicitud=pk)


    return render(request,"compras_solicitud/solicitudcompras_anular.html",
                                  {
                                    'request': request,
                                    'obj':obj,
                                    'detalles':detalles

                                  },
                                )


def AnularSolicitudComprasConfirmar(request, pk=None):
    self = SolicitudCompra.objects.get(id=pk)
    self.estado = "I"
    self.modificado_por = request.user
    self.modificado_el = timezone.now()
    self.save()
    return HttpResponseRedirect('/App/compras/solicitud/listado/')


def AprobacionOC(request, pk=None):
    roles = RolesUsuarios.objects.filter(usuario=request.user, rol__in=["EC","P"], estado="A")
    print(roles)
    if len(roles) > 0:
        solicitud = SolicitudCompra.objects.get(pk=pk)
        print(solicitud)
        proordenes = []
        while len(proordenes) == 0:
            for rol in roles:
                if rol.rol == "EC":
                    proordenes = PreOrdenCompra.objects.filter(~Q(procesado=True),
                                                               solicitud=solicitud, aprobacion_ec=None)
                
                if rol.rol == "P":
                    proordenes = PreOrdenCompra.objects.filter(~Q(procesado=True),
                                                               solicitud=solicitud, aprobacion_p=None)
               
                print(proordenes)

            return render(request,'compras_solicitud/Aprobacion_list.html',
                                  {
                                    'solicitud': solicitud,
                                    'proordenes': proordenes,
                                    'request': request,
                                  },
                                )
            
    else:
        return HttpResponseRedirect('/compras/solicitud/listado/')


class PresupuestosListView(LoginRequiredMixin, generic.ListView):
    template_name = "Presupuestos_compras/presupuesto_list.html"
    context_object_name = "obj"
    queryset = PresupuestoCompra.objects.filter(estado="A").order_by('-id')
    login_url = 'login'
    model = PresupuestoCompra


class PresupuestosDetalle(LoginRequiredMixin,  generic.DetailView): 
    template_name = "Presupuestos_compras/presupuesto_detalles.html"
    context_object_name = "obj"
    success_url = reverse_lazy('PresupuestosListView')
    login_url = 'login'
    model = PresupuestoCompra 
    def get_context_data(self, **kwargs):
        context = super(PresupuestosDetalle, self).get_context_data(**kwargs)
        context["detalles"] = PresupuestoDetalleCompra.objects.filter(presupuesto=self.object)
        return context
    




def AprobarRechazar(request, ids, solicitud, accion):
    print("akiii: ",  type(ids), solicitud, accion)
    preordenes = ids.split(',')
    print("ddddd: ", preordenes)
    roles_usuario = RolesUsuarios.objects.filter(usuario=request.user, rol__in=["EC", "P"], estado="A")
    print (roles_usuario)
    # roles = ParametrosAdicionales.objects.filter(nombre__in=nombres)
    for pk in preordenes:
        emitir = False
        print(preordenes, pk)
        if not id == "":
            print ("prueba", pk)
            preorden = PreOrdenCompra.objects.get(pk=pk)

            # solicitante = preorden.solicitud.creado_por
            # mes = timezone.now().month
            # year = timezone.now().year
            #
            # presu_object = Presupuesto.objects.get(periodo=year, departamento=Usuario.objects.get(pk=solicitud.pk)._grupo_organizacional(), aprobado=True)

            for rol in roles_usuario:
                print ("roles_usuario ",rol.rol)
                # for c_rol in roles:
                # print( "roles ", c_rol.comentario, c_rol.valor_caracter)
                # if rol.rol == c_rol.comentario and c_rol.valor_caracter == 'True':
                if rol.rol == "EC":
                    print ("akilll")
                    if preorden.aprobacion_ec == None:
                        if accion == "A":
                            preorden.aprobacion_ec = True
                        else:
                            preorden.aprobacion_ec = False
                        preorden.aprobacion_ec_el = timezone.now()
                        preorden.modificado_por = request.user
                        preorden.modificado_el = timezone.now()
                        preorden.save()
                
                if rol.rol == "P":
                    print("entra aki P ")
                    if preorden.aprobacion_p == None:
                        if accion == "A":
                            preorden.aprobacion_p = True
                            emitir = True
                        else:
                            preorden.aprobacion_p = False
                        preorden.aprobacion_p_el = timezone.now()
                        preorden.modificado_por = request.user
                        preorden.modificado_el = timezone.now()
                        preorden.save()
                print(" preorden.total ",  preorden.total, "rol.valor_numero: ", rol.valor_numero)
                # if preorden.total <= rol.valor_numero and emitir == False and not accion == "R":
                #     emitir = True
                #     print ("APRUEBA Y CARGA O.C.")
        print("emitir", emitir)
        if emitir == True:
            print("emitir", emitir)
            ordencompra = OrdenCompra()
            ordencompra.id = preorden.pk
            ordencompra.presupuesto = preorden.presupuesto
            ordencompra.solicitud = preorden.solicitud
            ordencompra.fecha = timezone.now().date()
            ordencompra.total = preorden.total
            ordencompra.aprobacion_ec = preorden.aprobacion_ec
            ordencompra.aprobacion_ec_el = preorden.aprobacion_ec_el
            ordencompra.aprobacion_p = preorden.aprobacion_p
            ordencompra.aprobacion_p_el = preorden.aprobacion_p_el
            ordencompra.estado = preorden.estado
            ordencompra.creado_por = preorden.creado_por
            ordencompra.modificado_por = preorden.modificado_por
            ordencompra.creado_el = preorden.creado_el
            ordencompra.modificado_el = preorden.modificado_el
            ordencompra.save()
            preordenesdetalles = PreOrdenDetalleCompra.objects.filter(preorden=preorden)
            for preordendetalle in preordenesdetalles:
                ordencompradetalle = OrdenDetalleCompra()
                ordencompradetalle.orden = ordencompra
                ordencompradetalle.presupuestodetalle = preordendetalle.presupuestodetalle
                ordencompradetalle.item = preordendetalle.item
                ordencompradetalle.item_nombre = preordendetalle.item_nombre
                ordencompradetalle.cantidad = preordendetalle.cantidad
                if preordendetalle.moneda == "GS":
                    ordencompradetalle.precio_unitario = preordendetalle.precio_unitario
                else:
                    ordencompradetalle.precio_unitario = preordendetalle.precio_unitario_extranjera
                ordencompradetalle.moneda = preordendetalle.moneda
                ordencompradetalle.save()

            preorden.procesado = True
            preorden.save()
            solicitud_obj = SolicitudCompra.objects.get(pk=preorden.solicitud.pk)
            pr = True
            preordenes = PreOrdenCompra.objects.filter(solicitud=solicitud_obj)
            for a_pre in preordenes:
                if not a_pre.procesado == True:
                    pr = False
            if rol.rol == "P":
                print("pr: ", pr)
                if pr == True:
                    solicitud_obj.estado = "PR"  # PENDIENTE DE RECEPCION
                else:
                    solicitud_obj.estado = "PRP"  # PENDIENTE DE RECEPCION PARCIAL
            else:
                solicitud_obj.estado = "PAD"
                
            solicitud_obj.modificado_el = timezone.now()
            solicitud_obj.save()
            estado = SolicitudAprobacion()
            estado.solicitud = solicitud_obj
            estado.usuario = request.user
            estado.estado = 'OCAEP'  # O.C. APROBADA Y ENVIADA A PROVEEDOR
            estado.creado_el = timezone.now()
            estado.save()
            content = {'pk':ordencompra.pk}
        elif not emitir == True and not accion == "R":
            content = {'pk': preorden.pk}
    if accion == "R":
        solicitud_obj = SolicitudCompra.objects.get(pk=solicitud)
        solicitud_obj.estado = "PSO"  # PENDIENTE DE SELECCION DE MEJOR OFERTA
        solicitud_obj.modificado_el = timezone.now()
        solicitud_obj.modificado_por = request.user
        solicitud_obj.save()
        return HttpResponseRedirect('/App/compras/solicitud/motivo/rechazo/' + int(preordenes[0]) + '/')
    else:
        if len(preordenes) > 1:
            return HttpResponseRedirect('/App/compras/solicitud/aprobacion/'+str(solicitud))
        else:
            return HttpResponseRedirect('/App/compras/solicitud/listado/')



def Motivorechazo(request, id=None):
    if request.method == 'POST':
        form = RechazoPreOrden(request.POST)
        if form.is_valid():
            preorden = PreOrdenCompra.objects.get(pk=id)
            detallespreorden = PreOrdenDetalleCompra.objects.filter(preorden=preorden)
            preordenrechazada = PreOrdenCompraRechazada()
            preordenrechazada.id = preorden.id
            preordenrechazada.presupuesto = preorden.presupuesto
            preordenrechazada.solicitud = preorden.solicitud
            preordenrechazada.fecha = preorden.fecha
            preordenrechazada.total = preorden.total
            preordenrechazada.aprobacion_ec = preorden.aprobacion_ec
            preordenrechazada.aprobacion_ec_el = preorden.aprobacion_ec_el
            preordenrechazada.aprobacion_p = preorden.aprobacion_p
            preordenrechazada.aprobacion_p_el = preorden.aprobacion_p_el
            preordenrechazada.procesado = preorden.procesado
            preordenrechazada.motivo = request.POST.get('motivo', '')
            preordenrechazada.rechazado_por = request.user
            preordenrechazada.rechazado_el = timezone.now()
            preordenrechazada.save()
            for detallepreorden in detallespreorden:
                detallepreordenrechazada = PreOrdenDetalleCompraRechazada()
                detallepreordenrechazada.id = detallepreorden.id
                detallepreordenrechazada.preorden = preordenrechazada
                detallepreordenrechazada.presupuestodetalle_id = detallepreorden.presupuestodetalle.pk
                detallepreordenrechazada.item = detallepreorden.item
                detallepreordenrechazada.item_nombre = detallepreorden.item_nombre
                detallepreordenrechazada.cantidad = detallepreorden.cantidad
                detallepreordenrechazada.precio_unitario = detallepreorden.precio_unitario
                detallepreordenrechazada.precio_unitario_extranjera = detallepreorden.precio_unitario_extranjera
                detallepreordenrechazada.moneda = detallepreorden.moneda
                detallepreordenrechazada.save()
                detallepreorden.delete()
            preorden.delete()
            solicitud = SolicitudCompra.objects.get(pk=preordenrechazada.solicitud.pk)
            estado = SolicitudAprobacion()
            estado.solicitud = solicitud
            estado.usuario = request.user
            estado.estado = 'OCR'  # ORDEN DE COMPRA RECHAZADA
            estado.comentario = request.POST.get('motivo', '')
            estado.creado_el = timezone.now()
            estado.save()
            return HttpResponseRedirect('/App/compras/solicitud/listado/')
    else:
        form = RechazoPreOrden()

    return render(request,'compras_solicitud/RechazoForms.html',
                                  {
                                    'form': form,
                                    'request': request,
                                  },
                                )



def OrdenesListado(request, id=None):
    roles = RolesUsuarios.objects.filter(usuario=request.user, rol__in=["EC","P"], estado="A")
    print(roles)
    if len(roles) > 0:
        queryset = OrdenCompra.objects.filter(solicitud=id)
        print("hola: ",queryset)

        return render(request,'Ordenes_compras/Listado_ordenes.html',
                                  {
                                    'request': request,
                                    'id': id,
                                    'queryset':queryset
                                  },
                                )
    else:
        return HttpResponseRedirect('/App/compras/solicitudcompra/list/')


def OrdenesComprasTodos(request):
    roles = RolesUsuarios.objects.filter(usuario=request.user, rol__in=["EC","P"], estado="A")
    print(roles)
    if len(roles) > 0:
        queryset = OrdenCompra.objects.all()
        print(queryset)

        return render(request,'Ordenes_compras/Ordenes.html',
                                  {
                                    'request': request,
                                    'queryset':queryset
                                  },
                                )
    else:
        return HttpResponseRedirect('/compras/solicitudcompra/list/')
    


def OpcionesAnularOrden(request, id):
    orden = OrdenCompra.objects.get(pk=id)
    form = OpcionesAnularOrdenForm
    if request.method == "POST":
        print("señor")
        form = OpcionesAnularOrdenForm(request.POST)

        orden.estado = "I"
        orden.save()
        preorden = PreOrdenCompra.objects.get(pk=id)
        preorden.estado = "I"
        preorden.save()
        ordenes = OrdenCompra.objects.filter(solicitud=orden.solicitud)
        if len(ordenes) == 1:
            print( "ANULO TODO")
            solicitud = orden.solicitud
            solicitud.estado = "I"
            solicitud.save()
            estado = SolicitudAprobacion()
            estado.solicitud = solicitud
            estado.usuario = request.user
            estado.comentario = form['observacion1'].value()
            estado.estado = "AEC"  # ANULADO POR ENCARGADO DE COMPRA
            estado.creado_el = timezone.now()
            estado.save()
        
        return HttpResponseRedirect('/App/compras/ordenes/listado/codigo/'+str(orden.solicitud.pk))

    return render(request,'Ordenes_compras/Anular_orden.html',
                                  {
                                  'request': request,
                                  'orden': orden,
                                  'form': form

                                  },
                                )



class BancosView(LoginRequiredMixin, generic.ListView):
    template_name = "bancos/banco_list.html"
    context_object_name = "obj"
    queryset = Bancos.objects.filter(estado="A").order_by('-id')
    login_url = 'login'
    model = Bancos


class BancosCrear(LoginRequiredMixin, generic.CreateView):
    model = Bancos
    login_url = 'login'
    form_class = Banco_Form
    success_url = reverse_lazy('BancosView')
    template_name = 'bancos/banco_crear.html'


    def form_valid(self, Banco_Form):
        object = Banco_Form.save(commit=False)
        object.estado = 'A'
        object.usuario_insercion = self.request.user
        object.fecha_insercion = timezone.now()
        object.save()
        return HttpResponseRedirect(self.success_url)


class BancosUpdate(LoginRequiredMixin, generic.UpdateView):
    model = Bancos
    login_url = 'login'
    form_class = Banco_Form
    context_object_name = "obj"
    success_url = reverse_lazy('BancosView')
    template_name = 'bancos/banco_crear.html'


    def form_valid(self, Banco_Form):
        object = Banco_Form.save(commit=False)
        object.usuario_modificacion = self.request.user
        object.fecha_modificacion = timezone.now()
        object.save()
        return HttpResponseRedirect(self.success_url)


class Bancosdelete(LoginRequiredMixin, generic.DeleteView):
    template_name = 'bancos/banco_delete.html'
    context_object_name = "obj"
    success_url = reverse_lazy("BancosView")
    login_url = 'login'
    model = Bancos
    def delete(self,  request, pk, *args, **kwargs):
        print("entro prro")
        self.object = self.get_object()
        self.object.estado = "I"
        self.object.usuario_modificacion = self.request.user
        self.object.fecha_modificacion = timezone.now()
        self.object.save()
        print("Guardo")   
        return HttpResponseRedirect(self.success_url)
    


def OrdenComprasGenerar(request, id=None, presupuesto=None):
    print("holas")
    cabecera = OrdenCompra.objects.get(solicitud=id, presupuesto=presupuesto)
    print(cabecera)
    detalles = OrdenDetalleCompra.objects.filter(orden=cabecera)
    print(detalles)
    data = {'cabecera':cabecera, 'detalles':detalles}
    pdf = render_to_pdf("Ordenes_compras/orden_compras.html",data)
    return HttpResponse(pdf, content_type='application/pdf')


def FacturasProveedorList(request):
    desde = request.GET.get('desde', None)
    hasta = request.GET.get('hasta', None)
    proveedor = request.GET.get('proveedor', None)

    # form = FiltrolistadoFacturasRecibidas(initial={'desde': desde, 'hasta': hasta, 'proveedor': proveedor})
    query_set = FacturasRecibidasProveedor.objects.all().order_by('id')

    # if desde and hasta:
    #     query_set = query_set.filter(fecha__range=[desde, hasta])
    
    # if proveedor:
    #     query_set = query_set.filter(proveedor=proveedor)

    # if proveedor and desde and hasta:
    #     query_set = query_set.filter(proveedor=proveedor,fecha__range=[desde, hasta])
    
    # print(query_set)

    return render(request,
                        'Facturas_Proveedor/facturas_list.html',
                        {
                            'query_set':query_set,
                            # 'form':form
                        }) 


def FacturaRecibidasProveedorCreate(request, pk=None):
    print("hhh: ", request.GET.get('edit',''))
    template_name = 'Facturas_Proveedor/facturas_proveedor_create.html'
    if pk:
        # template_name = 'facturas_recibidas/factura_update.html'
        edit = str(1)
        main = FacturasRecibidasProveedor.objects.get(id=pk)
        print(main.factura, main)

        # check_libro = check_libro_iva_compra(main.fecha.strftime("%Y-%m-%d"))
        # check_orden = check_orden_pago(str(main.factura))
        conceptos = FacturasProveedorConceptos.objects.filter(factura=main)
        for a in conceptos:
            print( a.item_producto)
        cuotas = FacturasProveedorDetalle.objects.filter(factura=main)
        ConceptoFormset = formset_factory(FacturasRecibidasConceptosEditForm, formset=PresupuestoCustomDetailForm, extra=0)
        CuotaFormset = formset_factory(FacturasDetalleEditForm, formset=PresupuestoCustomDetailForm, extra=0)
    else:
        # template_name = 'facturas_recibidas/facturas_create.html'

        edit = str(0)
        print("entra")
        main = FacturasRecibidasProveedor()
        ConceptoFormset = inlineformset_factory(FacturasRecibidasProveedor, FacturasProveedorConceptos, form=FacturasRecibidasConceptosForm, extra=1)
        CuotaFormset = inlineformset_factory(FacturasRecibidasProveedor, FacturasProveedorDetalle, form=FacturasDetalleForm, extra=1)
    if request.method == "POST":
        form = FacturasRecibidasForm(request.POST, prefix='main')
        form_concepto = ConceptoFormset(request.POST, prefix='conceptos')
        form_cuota = CuotaFormset(request.POST, prefix='cuotas')
        print ("form", form.errors)
        print ("form_concepto", form_concepto.errors)
        print("form_concepto", form_concepto)
        print ("form_cuota", form_cuota.errors)
        if form.is_valid() and form_concepto.is_valid() and form_cuota.is_valid():
            if pk:
                print("okey", )
                saldo = 0.00
                # print(Sede.object.get(id=form['sede'].value()).id)
                departamento = None
                a = form.save(commit=False)
                a.factura = main.factura
                a.usuario_modificacion = request.user
                a.fecha_modificacion = timezone.now()
                a.saldo = saldo
                a.save()
                
                for form in form_concepto:
                    # print(form['factura'].value(), form['item'].value())
                    con_obj = FacturasProveedorConceptos.objects.get(factura=pk, item=form['item'].value())
                    print("holaaaaa: ", con_obj)
                    # print( con_obj.factura.factura)
                    # print( con_obj.item)
                    con_obj.monto = form['monto'].value()
                    con_obj.monto_impuesto = form['monto_impuesto'].value()
                    con_obj.porcentaje_impuesto = form['porcentaje_impuesto'].value()
                    con_obj.item_producto_id = form['item_producto'].value()
                    con_obj.orden_id = form['orden'].value()
                    con_obj.cantidad = form['cantidad'].value()
                    con_obj.monto_unitario = form['monto_unitario'].value()
                    con_obj.usuario_modificacion = request.user
                    con_obj.fecha_modificacion = timezone.now()
                    # print( con_obj.factura.factura)
                    # print( con_obj.item)
                    # con_obj.save()
                    departamento = get_grupo_usuario(con_obj.orden.solicitud.creado_por)
                for form in form_concepto.deleted_forms:
                    con_obj = FacturasProveedorConceptos.objects.get(factura=pk, item=form['item'].value())
                    con_obj.delete()
                for form in form_cuota:
                    cuo_obj = FacturasProveedorDetalle.objects.get(factura=pk, cuota=form['cuota'].value())
                    print("holaaaaa2: ", con_obj)
                    cuo_obj.fecha_vencimiento = form['fecha_vencimiento'].value()
                    cuo_obj.monto = form['monto'].value()
                    cuo_obj.saldo = form['saldo'].value()
                    cuo_obj.observacion = form['observacion'].value()
                    cuo_obj.usuario_modificacion = request.user
                    cuo_obj.fecha_modificacion = timezone.now()
                    # print( type(saldo))
                    # print(type(cuo_obj.saldo))
                    saldo = saldo + float(cuo_obj.saldo)
                    cuo_obj.save()
                for form in form_cuota.deleted_forms:
                    cuo_obj = FacturasProveedorDetalle.objects.get(factura=pk, cuota=form['cuota'].value())
                    cuo_obj.delete()
                if not departamento == None:
                    a.departamento = departamento
                a.saldo = saldo
                a.save()
            else:
                print("form_concepto2", form_concepto)
                print("ola ke")
                saldo = 0.00
                factura_obj = form.save(commit=False)
                nro_factura = num_factura()
                factura_obj.factura = nro_factura
                factura_obj.usuario_insercion = request.user
                factura_obj.fecha_insercion = timezone.now()
                factura_obj.saldo = saldo
                factura_obj.save()
                print("c: ", form_concepto)
                for form in form_concepto:
                    print("si")
                    print("cantidad: ", form['cantidad'].value())
                    if form['cantidad'].value():
                        if float(form['cantidad'].value()) > 0.00:
                            concepto = form.save(commit=False)
                            concepto.factura = factura_obj
                            # concepto.item = int(numero_item(str(factura_obj.pk)))
                            concepto.usuario_insercion = request.user
                            concepto.fecha_insercion = timezone.now()
                            concepto.save()
                            departamento = get_grupo_usuario(concepto.orden.solicitud.creado_por)
                for form in form_cuota:
                    print("entraaaaa")
                    if not form["cuota"].value() == None:
                        cuota = form.save(commit=False)
                        cuota.factura = factura_obj
                        cuota.usuario_insercion = request.user
                        cuota.fecha_insercion = timezone.now()
                        saldo = saldo + float(cuota.saldo)
                        print("saldo: ", saldo)
                        cuota.save()
                if not departamento == None:
                    factura_obj.departamento = departamento
                print("saldo: ", saldo)
                factura_obj.departamento = departamento
                factura_obj.saldo = saldo
                factura_obj.save()
            
            return HttpResponseRedirect('/App/compras/facturas/proveedor/listado/')

    else:
        if pk:
            form = FacturasRecibidasForm(instance=main, prefix='main')
            form_concepto = ConceptoFormset(prefix='conceptos', initial=[{
                                                                        'factura': x.factura.factura,
                                                                        'item': x.item,
                                                                        'cantidad': x.cantidad,
                                                                        'item_producto': x.item_producto_id,
                                                                        'item_nombre': x.item_producto.nombre_corto,
                                                                        'monto': x.monto,
                                                                        'monto_unitario': x.monto_unitario,
                                                                        'monto_impuesto': x.monto_impuesto,
                                                                        'porcentaje_impuesto': int(x.porcentaje_impuesto),
                                                                        'orden': x.orden_id,
                                                                        'orden_nombre': str(x.orden.numero)+"/"+str(x.orden.periodo)
                                                                      } for x in conceptos
                                                                      ])
            form_cuota = CuotaFormset(prefix='cuotas', initial=[{
                                                                'factura': x.factura.factura,
                                                                'cuota': x.cuota,
                                                                'fecha_vencimiento': x.fecha_vencimiento,
                                                                'monto': x.monto,
                                                                'saldo': x.saldo,
                                                                'observacion': x.observacion
                                                              } for x in cuotas
                                                              ])
        else:
            form = FacturasRecibidasForm(instance=main, prefix='main')
            form_concepto = ConceptoFormset(instance=main, prefix='conceptos')
            form_cuota = CuotaFormset(instance=main, prefix='cuotas')
       
    return render(request,template_name
                ,
                {
                    'form':form,
                    'form_concepto':form_concepto,
                    'form_cuota':form_cuota,
                    'edit': edit
                })  


class FacturaRecibidaDetalle(LoginRequiredMixin,  generic.DetailView): 
    template_name = 'Facturas_Proveedor/factura_detalle.html'
    context_object_name = "object"
    success_url = reverse_lazy('FacturasProveedorList')
    login_url = 'login'
    model = FacturasRecibidasProveedor

    def get_context_data(self, **kwargs):
        context = super(FacturaRecibidaDetalle, self).get_context_data(**kwargs)
        conceptos = FacturasProveedorConceptos.objects.filter(factura=self.object)
        cuotas = FacturasProveedorDetalle.objects.filter(factura=self.object)

        context["conceptos"] = FacturasProveedorConceptos.objects.filter(factura=self.object)
        context["cuotas"] = FacturasProveedorDetalle.objects.filter(factura=self.object)

        return context


class FacturaRecibidaPoveedorDelete(LoginRequiredMixin, generic.DeleteView):
    template_name = 'Facturas_Proveedor/factura_delete.html'
    context_object_name = "obj"
    success_url = reverse_lazy("FacturasProveedorList")
    login_url = 'login'
    model = FacturasRecibidasProveedor

    # def get_context_data(self, **kwargs):
    #     context = super(FacturaRecibidaPoveedorDelete, self).get_context_data(**kwargs)
    #     # context['check_libro'] = check_libro_iva_compra(self.object.factura_fisica)
    #     # context['check_orden'] = verificar_orden(str(self.object))
    #     context['request'] = self.request
    #     return context


    def delete(self,  request, *args, **kwargs):
        print("entro prro")
        self.object = self.get_object()
        # FacturasProveedorConceptos.objects.filter(factura=self.get_object()).delete()
        # FacturasProveedorDetalle.objects.filter(factura=self.get_object()).delete()
        self.object.delete()
        print("Guardo")   
        return HttpResponseRedirect(self.success_url)


def ListadodeCuentasApagar(request):
    query = CuentasaPagar.objects.all()
    print(query)

    return render(request,
                    'CuentasAPagar/cuentas_list.html',
                    {
                        "query":query
                    })  


def aviso_cuentas_apagar(request):
    template_name = 'Controles_avisos/aviso_cuentas.html'

    return render(request, template_name)

def CuentasPagarCreate(request):

    if request.method == 'POST':
        form = CuentasPagarForm(request.POST)
        print ("form", form.errors)
        if form.is_valid():
            consulta_o = CuentasaPagar.objects.filter(mes =request.POST.get("mes"), 
                                                       anho=request.POST.get("anho"))
            print("consulta_o: ", consulta_o)
            mes = request.POST.get("mes")
            print("mes: ", mes)
            anho=str(mes[:4])
            mes_x= str(mes[5]+mes[6])
            print("mes: ",anho, mes_x, mes, mes[-6])
            verificacion = FacturasProveedorDetalle.objects.filter(fecha_vencimiento__year=anho, 
                                                        fecha_vencimiento__month=mes_x)
            print("fechas: ", verificacion)
            if verificacion:
                if not consulta_o:
                    form = form.save(commit=False)
                    print("entra")
                    form.creado_por = request.user
                    form.creado_el = timezone.now()
                    form.save()
                    ultimo = CuentasaPagar.objects.last()
                    print(" ultimo: ", ultimo)

                    

                    cuotas = FacturasProveedorDetalle.objects.filter(fecha_vencimiento__year=anho, 
                                                        fecha_vencimiento__month=mes_x)
                    for cv in cuotas:
                        print(" entro uno: ", cv.id)
                    print("bb: ", cuotas, len(cuotas))
                    # x = FacturasDetalle.objects.all().values_list('fecha_vencimiento', flat=True)
                    # print("v ", x)
                    
                    if cuotas:
                        for cuentas in cuotas:
                            print(cuentas.factura.id)
                            print(cuentas.cuota)
                            ordenes = FacturasProveedorConceptos.objects.filter(factura=cuentas.factura.id)
                            for o in ordenes:
                                orden = o.orden.id
                                print("orden: ", orden)
                            solicitud = OrdenCompra.objects.get(id=orden)
                            detalles_cuentas = CuentasaPagardet()
                            detalles_cuentas.factura= cuentas.factura
                            detalles_cuentas.proveedor= cuentas.factura.proveedor
                            detalles_cuentas.factura_fisica= cuentas.factura.factura_fisica
                            detalles_cuentas.saldo= cuentas.factura.saldo
                            detalles_cuentas.tipo_de_factura = cuentas.factura.tipo_de_factura
                            detalles_cuentas.total = cuentas.factura.saldo
                            detalles_cuentas.monto_gravado5 = cuentas.factura.monto_gravado5
                            detalles_cuentas.monto_impuesto5 = cuentas.factura.monto_impuesto5
                            detalles_cuentas.monto_total10 = cuentas.factura.monto_total10
                            detalles_cuentas.monto_total5 = cuentas.factura.monto_total5
                            detalles_cuentas.orden = solicitud
                            detalles_cuentas.solicitud = solicitud.solicitud
                            detalles_cuentas.fecha_vencimiento = cuentas.fecha_vencimiento
                            detalles_cuentas.cuota = cuentas.cuota
                            detalles_cuentas.CuentaPagar = ultimo
                            detalles_cuentas.save()
                            print("guardo uno")

                    return HttpResponseRedirect('/App/compras/cuentas/pagar/listado/')
                else:
                    return HttpResponseRedirect('/App/compras/cuentas/pagar/alerta/')
            else:
                return HttpResponseRedirect('/App/compras/cuentas/pagar/listado/')


    else:
        form = CuentasPagarForm()



    return render(request,
                    'CuentasAPagar/cuentas_create.html',
                    {
                        'form':form

                    })  



class CuentasPagarDelete(LoginRequiredMixin, generic.DeleteView):
    template_name = 'CuentasAPagar/CuentasAPagar_delete.html'
    context_object_name = "obj"
    success_url = reverse_lazy("ListadodeCuentasApagar")
    login_url = 'login'
    model = CuentasaPagar

    # def delete(self,  request, *args, **kwargs):
    #     self.object = self.get_object()
    #     CuentasaPagardet.objects.filter(CuentaPagar=self.get_object()).delete()
    #     self.object.delete()
    #     return HttpResponseRedirect(self.success_url)



class CuentasPagarDetalle(LoginRequiredMixin,  generic.DetailView): 
    template_name = 'CuentasAPagar/CuentasAPagar_detail.html'
    context_object_name = "object"
    success_url = reverse_lazy('ListadodeCuentasApagar')
    login_url = 'login'
    model = CuentasaPagar 
    def get_context_data(self, **kwargs):
        context = super(CuentasPagarDetalle, self).get_context_data(**kwargs)
        context["detalles"] = CuentasaPagardet.objects.filter(CuentaPagar=self.object)
        return context



def LibroIvaEgresosList(request):
    query = LibroIvaEgresos.objects.all()
    print(query)

    return render(request,
                    'IVA_Egresos/libroivaegresos_list.html',
                    {
                        "query":query
                    })  



def aviso_libro_iva(request):
    template_name = 'alerta/aviso_iva.html'

    return render(request, template_name)


def LibroIvaEgresosCreate(request, pk=None):


    if request.method == 'POST':
       
        form = LibroEgresosForm(request.POST)
        print ("form", form.errors)
        if form.is_valid():
            mes = request.POST.get("mes")
            fiscal=request.POST.get("ejercicio_fiscal")
            consulta = LibroIvaEgresos.objects.filter(mes=mes, ejercicio_fiscal=fiscal)
            print("hola: ", consulta)
            if not consulta:
                
                anho=str(mes[:4])
                mes_x= str(mes[5]+mes[6])
                c = str(request.POST.get("mes"))
                print(mes, fiscal)
                print(mes_x, anho, fiscal, c[6])

                form = form.save(commit=False)
                print("entra")
                form.creado_por = request.user
                form.creado_el = timezone.now()
                form.save()

                facturas = FacturasRecibidasProveedor.objects.filter(fecha__year=anho,
                                                            fecha__month=mes_x)
                print("facturas: ", facturas)
            
            
                if facturas:
                    ultimo = LibroIvaEgresos.objects.last()
                    print(" ultimo: ", ultimo)
                    for f in facturas:
                        print(f.formato_comprobante)
                        libro = LibroIvaEgresosDetalles()
                        libro.tipoidentificacion = 1
                        libro.ruc = str(str(f.proveedor.ruc)+'-'+str(f.proveedor.dv))
                        libro.razon_social = f.proveedor.razon_social
                        libro.tipocomprobante = f.formato_comprobante
                        libro.nrotimbrado=f.nrotimbrado
                        libro.factura_fisica=f.factura_fisica
                        libro.tipo_de_factura=f.tipo_de_factura
                        libro.fecha=f.fecha
                        libro.saldo=f.saldo
                        libro.monto_gravado=f.monto_gravado
                        libro.monto_gravado5=f.monto_gravado5
                        libro.monto_impuesto5=f.monto_impuesto5
                        libro.monto_total10=f.monto_total10
                        libro.monto_total5=f.monto_total5
                        libro.factura=f
                        libro.monto_exento=f.monto_exento
                        libro.monto_impuesto= f.monto_impuesto
                        libro.libro = ultimo
                        libro.save()
                        print("guardo el primer libro")

                return HttpResponseRedirect('/App/compras/iva/egresos/listado/')
            else:
                return HttpResponseRedirect('/App/compras/iva/egresos/alerta/')

    else:
        print("hola")
        form = LibroEgresosForm()

    return render(request,
                    'IVA_Egresos/libroiva_create.html',
                    {
                       'form':form     
                    })  


class libroIvaDelete(LoginRequiredMixin, generic.DeleteView):
    template_name = 'IVA_Egresos/libroiva_delete.html'
    context_object_name = "obj"
    success_url = reverse_lazy("LibroIvaEgresosList")
    login_url = 'login'
    model = LibroIvaEgresos

    # def delete(self,  request, *args, **kwargs):
    #     print("entro prro")
    #     self.object = self.get_object()
    #     LibroIvaEgresosDetalles.objects.filter(libro=self.get_object()).delete()
    #     self.object.delete()
    #     print("Guardo")   
    #     return HttpResponseRedirect(self.success_url)



class libroIvaEgresosDetalle(LoginRequiredMixin,  generic.DetailView): 
    template_name = 'IVA_Egresos/libroiva_detalle.html'
    context_object_name = "obj"
    success_url = reverse_lazy('LibroIvaEgresosList')
    login_url = 'login'
    model = LibroIvaEgresos 
    def get_context_data(self, **kwargs):
        context = super(libroIvaEgresosDetalle, self).get_context_data(**kwargs)
        context["detalles"] = LibroIvaEgresosDetalles.objects.filter(libro=self.object)
        return context



def NotaCreditoList(request):
    query = NotaCreditoProveedor.objects.all()
    print(query)

    return render(request,
                    'nota_credito/ncredito_list.html',
                    {
                        "query":query
                    })  


def NotaCreditoCreate(request, id=None):
    main = NotaCreditoProveedor()
    main.fecha = timezone.now().date()
    
    detalle = inlineformset_factory(NotaCreditoProveedor, NotaCreditoProveedordet,
                                    form=NotaCreditoFormdet,
                                        extra=1,  can_delete=True)
    if request.method == 'POST':
        form = NotaCreditoForm(request.POST, request.FILES, instance=main,  prefix='mains')
        formset = detalle(request.POST, instance=main, prefix='detalle')
        print(form.errors)
        print(formset.errors)
        if form.is_valid() and formset.is_valid():
            print("hola")
            main.creado_por = request.user
            main.creado_el = timezone.now()
            main.save()
            form.save()
            print("guarda cabecera")
            formset.save()
            print("guarda delles")

            return HttpResponseRedirect('/App/compras/nota/credito/listado/')

    else:
        form = NotaCreditoForm(instance=main, prefix='mains')
        formset = detalle(instance=main, prefix='detalle')     

    return render(request,
                    'nota_credito/ncredito_create.html',
                    {
                        'form':form,
                        'formset':formset
                    }) 


class NCProveedorDelete(LoginRequiredMixin, generic.DeleteView):
    template_name = 'nota_credito/ncredito_delete.html'
    context_object_name = "obj"
    success_url = reverse_lazy("NotaCreditoList")
    login_url = 'login'
    model = NotaCreditoProveedor

    def delete(self,  request, *args, **kwargs):
        print("entro prro")
        self.object = self.get_object()
        NotaCreditoProveedordet.objects.filter(nota_credito=self.get_object()).delete()
        self.object.delete()
        print("Guardo")   
        return HttpResponseRedirect(self.success_url)
    


class NCProveedorDetalle(LoginRequiredMixin,  generic.DetailView): 
    template_name = 'nota_credito/ncredito_detalle.html'
    context_object_name = "obj"
    success_url = reverse_lazy('NotaCreditoList')
    login_url = 'login'
    model = NotaCreditoProveedor 
    def get_context_data(self, **kwargs):
        context = super(NCProveedorDetalle, self).get_context_data(**kwargs)
        context["detalles"] = NotaCreditoProveedordet.objects.filter(nota_credito=self.object)
        return context


def NotaDebitoList(request):
    query = NotaDebitoProveedor.objects.all()
    print(query)

    return render(request,
                    'nota_debito/nd_list.html',
                    {
                        "query":query
                    })                    


def NDCreateUpdate(request, id=None):
    main = NotaDebitoProveedor()
    main.fecha = timezone.now().date()
    
    detalle = inlineformset_factory(NotaDebitoProveedor, NotaDebitoProveedordet,
                                    form=NotaDebitoFormdet,
                                        extra=1,  can_delete=True)
    if request.method == 'POST':
        form = NotadebitoForm(request.POST, instance=main,  prefix='mains')
        formset = detalle(request.POST, instance=main, prefix='detalle')
        print(form.errors)
        print(formset.errors)
        if form.is_valid() and formset.is_valid():
            print("hola")
            main.creado_por = request.user
            main.creado_el = timezone.now()
            main.save()
            form.save()
            print("guarda cabecera")
            formset.save()
            print("guarda delles")

            return HttpResponseRedirect('/App/compras/nota/debito/listado/')

    else:
        form = NotadebitoForm(instance=main, prefix='mains')
        formset = detalle(instance=main, prefix='detalle')     

    return render(request,
                    'nota_debito/nd_create.html',
                    {
                        'form':form,
                        'formset':formset
                    }) 


class NDProveedorDelete(LoginRequiredMixin, generic.DeleteView):
    template_name = 'nota_debito/nd_delete.html'
    context_object_name = "obj"
    success_url = reverse_lazy("NotaDebitoList")
    login_url = 'login'
    model = NotaDebitoProveedor

    def delete(self,  request, *args, **kwargs):
        print("entro prro")
        self.object = self.get_object()
        NotaDebitoProveedordet.objects.filter(nota_debito=self.get_object()).delete()
        self.object.delete()
        print("elimino chiki")   
        return HttpResponseRedirect(self.success_url)
    

class NDProveedorDetalle(LoginRequiredMixin,  generic.DetailView): 
    template_name = 'nota_debito/nd_detalle.html'
    context_object_name = "obj"
    success_url = reverse_lazy('NotaDebitoList')
    login_url = 'login'
    model = NotaDebitoProveedor 
    def get_context_data(self, **kwargs):
        context = super(NDProveedorDetalle, self).get_context_data(**kwargs)
        context["detalles"] = NotaDebitoProveedordet.objects.filter(nota_debito=self.object)
        return context