from dal import autocomplete

from compras.models import *
from django.db.models import Q



class MarcaAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated:
            print("entra")
            return Marca.objects.none()

        qs = Marca.objects.all()

        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs

# class ConceptoAutocomplete(autocomplete.Select2QuerySetView):
#     def get_queryset(self):
#         # Don't forget to filter out results depending on the visitor !
#         if not self.request.user.is_authenticated:
#             print("entra")
#             return ConceptosOrdenes.objects.none()
        
#         qs = ConceptosOrdenes.objects.all().exclude(concepto_padre=None)

#         if self.q:
#             qs = qs.filter(descripcion__istartswith=self.q)

#         return qs

class CategoriaAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated:
            print("entra")
            return CategoriasStock.objects.none()

        qs = CategoriasStock.objects.all()

        if self.q:
            qs = qs.filter(nombre__istartswith=self.q)

        return qs


class ItemsComprasAutocomplete(autocomplete.Select2QuerySetView):

    autocomplete_js_attributes = {'placeholder': 'Producto...'}

    print("enttro")
    def get_queryset(self):
        q = self.request.GET.get('q', '')
        print (q)
        
        choices = Item.objects.all()
        
        if q:
            choices = choices.filter(nombre_corto__icontains=q, estado="A")
        return (choices)



class DepartamentoAutocomplete(autocomplete.Select2QuerySetView):
    
    def get_queryset(self):
        print(self.request.user.id)
        qs = Departamentos.objects.filter(usuarios=self.request.user.id, estado="A")
        if not qs:
            qs = Departamentos.objects.filter(responsable=self.request.user.id, estado="A")

        print("centro: ", qs)

        if self.q:
            qs = qs.filter(usuarios=self.request.user, nombre__icontains=self.q, estado="A")
        return qs


# class SedesComprasAutocomplete(autocomplete.Select2QuerySetView):
#     def get_queryset(self):
#         print(self.request.user.id)
#         qs = Sede.objects.filter(usuarios=self.request.user.id, estado="A")
#         print("centro: ", qs)

#         if self.q:
#             qs = qs.filter(usuarios=self.request.user, nombre__icontains=self.q, estado="A")
#         return qs


class ProveedorComprasAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        print(self.request.user.id)
        qs = Proveedores.objects.filter(estado="A").exclude(razon_social__in=["None",""])
        print("centro: ", qs)

        if self.q:
            qs = qs.filter(razon_social__icontains=self.q, estado="A")
        return qs
    
class BancoAutocomplete(autocomplete.Select2QuerySetView):
     def get_queryset(self):
        print(self.request.user.id)
        qs = Bancos.objects.filter( estado="A")
        print("centro: ", qs)

        if self.q:
            qs = qs.filter(descripcion__icontains=self.q, estado="A")
        return qs
     