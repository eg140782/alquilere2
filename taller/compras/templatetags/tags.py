from django import template

from compras.api import separador_de_miles, separador_de_miles_entero 
register = template.Library()
from compras.templatetags.numbers_to_letters import numero_a_letras
from compras.models import PresupuestoDetalleCompra, PresupuestoCompra, OrdenCompra, \
   PreOrdenCompra
from num2words import num2words

@register.simple_tag(name='total')
def total(a,b):
    return separador_de_miles(str(a*b))

@register.simple_tag(name='separar')
def separar(value):
   return separador_de_miles(value)

def separar_entero(value):
   return separador_de_miles_entero(value)

@register.simple_tag(name='moneda_orden')
def moneda_orden(value):
   v = OrdenCompra.objects.get(id=value)
   print(v)
   c = PresupuestoDetalleCompra.objects.filter(presupuesto=v.presupuesto).values_list('moneda', flat=True)[0]
   print(c)
   return c


@register.simple_tag(name='numero_letras')
def numero_letras(numero):
   return num2words(numero, lang='es')
