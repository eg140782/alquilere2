from django import forms
from compras.models import *
from django.forms import Textarea
from dal import autocomplete
from django.forms.formsets import BaseFormSet

tipo_empresa = (
    ('', '-----------'),
    ('Unipersonal', 'Unipersonal'),
    ('Sociedad', 'Sociedad'),
)
MONEDA2 = (
    ('GS', 'GUARANIES'),
)
class Marcas_Form(forms.ModelForm):
    class Meta: 
        model = Marca
        exclude = {'estado', 'creado_por', 'modificado_por', 'creado_el', 'modificado_el'}
        fields = {'nombre'}
    field_order = ['nombre']

    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)     
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class':'form-control'
            })


class ProveedorForm(forms.ModelForm):
    class Meta: 
        model = Proveedores
        # fields = {'usuario', 'rol'}
        exclude = {'poderFirmantes', 'estatutosSociales','usuario_modificacion','fecha_modificacion','usuario_insercion', 'fecha_insercion', 'estado'}
    # field_order = ['ciudad', 'distrito']
        widgets = {
                'observaciones':  Textarea(attrs={'cols': 50, 'rows': 3, 'placeholder': 'Max. 250 caracteres...'}),
                'tipo_empresa': forms.Select(choices=tipo_empresa,attrs={'class': 'form-control'}),

                }

    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)     
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class':'form-control'
            })
        # self.fields['distrito'] = forms.ModelChoiceField(
        #     queryset=Distritos.objects.filter(estado="A"),
        #     required=True )
    
class CuentasProveedorForm(forms.ModelForm):
    class Meta: 
        model = CuentasProveedor
        # fields = {'usuario', 'rol'}
        exclude = {'estado'}
        widgets = {
                'banco': autocomplete.ModelSelect2(url='BancoAutocomplete'),
                'mismo_titular':forms.Select(attrs={'class': 'form-control'}),
                'tipo': forms.Select(attrs={'class': 'form-control'}),
                'num_cuenta': forms.TextInput(attrs={'class': 'form-control', 'type':'number', 'min':'1'}),
                'moneda': forms.Select(attrs={'class': 'form-control'}),
                'titular': forms.TextInput(attrs={'class': 'form-control'}),
                'num_ci': forms.TextInput(attrs={'class': 'form-control'}),


                

            }
        
class ItemForm(forms.ModelForm):
    class Meta:
        model = Item
        exclude = {'estado', 'creado_por', 'modificado_por', 'creado_el', 'modificado_el','stock_maximo','stock_minimo', 'utiles', 'imagen'}
        widgets = {"marca": autocomplete.ModelSelect2(url='MarcaAutocomplete'),
                   "concepto": autocomplete.ModelSelect2(url='ConceptoAutocomplete'),
                   "categoria": autocomplete.ModelSelect2(url='CategoriaAutocomplete'),
                   "nombre_largo": Textarea(attrs={'cols': 20, 'rows': 5, 'placeholder': 'Max. 250 caracteres...'})}

    def clean_imagen(self):
        imagen = self.cleaned_data.get('imagen')
        if imagen:
            if not imagen.name.endswith(".jpg") or imagen.name.endswith(".jpeg"):
                raise forms.ValidationError("Solo archivos .jpg y .jpeg son aceptados")
            return imagen
    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)     
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class':'form-control'
            })

class Categorias_Form(forms.ModelForm):
    class Meta: 
        model = CategoriasStock
        fields = {'nombre', 'nombre_corto'}
    field_order = ['nombre', 'nombre_corto']

    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)     
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class':'form-control'
            })


class DepartamentoForm(forms.ModelForm):
    class Meta:
        model = Departamentos
        exclude = {'estado','usuarios', 'creado_por',        'modificado_por', 'creado_el', 'modificado_el'}

class SolicituComprasForm(forms.ModelForm):
    class Meta:
        model = SolicitudCompra
        exclude = {'estado',
        'creado_por',
        'modificado_por',  'modificado_el','adjunto'
        }        
        fields = {'motivo','creado_el'}
        widgets = {
            'creado_el':forms.TextInput(attrs={'class': 'form-control','readonly': 'readonly'}),
            'motivo': Textarea(attrs={'cols': 20, 'rows': 5, 'placeholder': 'Max. 250 caracteres...'}),
        }

    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)     
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class':'form-control'
            })
class SolicituComprasDetalleForm(forms.ModelForm):
    class Meta:
        model = SolicitudCompraDetalle
        exclude = {'item_nombre',
        # 'creado_por',
        # 'modificado_por', 'creado_el', 'modificado_el',
        }        
        # fields = {'adjunto'}
        widgets = {
            'item': autocomplete.ModelSelect2(url='ItemsComprasAutocomplete',attrs={
                    'data-placeholder': 'Item..'
                    },),

            'cantidad':forms.TextInput(attrs={'class': 'form-control', 'type':'number', 'min':'1'}),
            'departamento': autocomplete.ModelSelect2(url='DepartamentoAutocomplete'),
            

        }


class RolesUsuarioForm(forms.ModelForm):
    class Meta: 
        model = RolesUsuarios
        fields = {'usuario', 'rol', 'valor_numero'}
        exclude = {'estado', 'creado_por', 'modificado_por', 'creado_el', 'modificado_el'}

    field_order = ['usuario', 'rol', 'valor_numero']

    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)     
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class':'form-control'
            })


class AprobacionUnoResponsableAreaForm(forms.Form):
    aprobado = forms.TypedChoiceField(
        choices=(('', 'Seleccione una opcion'), (True, 'SI'), (False, 'NO')),
        required=True,
        widget=forms.Select(attrs={'class': 'form-control'}),
        label="Aprobar p/ Revision por Encargado de Compra"
    )
    motivo = forms.CharField(widget=forms.Textarea, required=False)



class AprobacionEncargadoCompraForms(forms.Form):
    aprobado = forms.TypedChoiceField(
        choices=(('', 'Seleccione una opcion'),(True, 'SI'), (False, 'NO')),
        required=True,
        widget=forms.Select(attrs={'class': 'form-control'}),
        label="Aprobacion para la Carga de Presupuestos"
    )
    motivo = forms.CharField(widget=forms.Textarea, required=False)



class PresupuestoDetalleCompraCustomForm(forms.Form):
    # proveedor = forms.IntegerField(widget=
    # autocomplete.ListSelect2(url='ProveedorComprasAutocomplete'))

    proveedor = forms.ModelChoiceField(
        widget=forms.Select(attrs={'class': 'form-control proveedor'}),
        queryset=Proveedores.objects.all().exclude(estado="I"))
    
    costo_unitario = forms.CharField(widget=forms.TextInput(attrs={'class': 'auto form-control',
                                                                   'style': 'text-align: right;',
                                                                   'size': '7px',
                                                                   'onkeyup': 'subtotal(this);'}))
    moneda = forms.TypedChoiceField(choices=MONEDA2, required=True,
                                    widget=forms.Select(
                                        attrs={'class': 'form-control', 'onchange': 'check_coti(this);'}))
    cantidad = forms.CharField(widget=forms.TextInput(attrs={
        'readonly': 'readonly',
        'class': 'cantidad form-control cantidad',
        'style': 'text-align: right;',
        'size': '7px',
        'onkeyup': 'subtotal(this);'}))


class PresupuestoDetalleCompraCustomDetailForm(BaseFormSet):
    def clean(self):
        
        """
        Adds validation to check that no two links have the same anchor or URL
        and that all links have both an anchor and URL.
        """
        if any(self.errors):
            return

        proveedores = []
        duplicados = False

        for form in self.forms:
            if form.cleaned_data and 'proveedor' in form.cleaned_data:
                proveedor = form.cleaned_data['proveedor']

                if proveedor:
                    if proveedor in proveedores:
                        duplicados = True
                    proveedores.append(proveedor)

        if len(proveedores) == 0:
            raise forms.ValidationError(
                'No se han cargado proveedores.',
                code='empty pictures'
            )

        if duplicados:
            raise forms.ValidationError(
                'Los proveedores no pueden estar repetidos.',
                code='proveedores_duplicados'
            )


class PresupuestoCompraCustomForm(forms.ModelForm):
    class Meta:
        model = PresupuestoCompra
        exclude = {'solicitud', 'proveedor', 'moneda', 'estado', 'creado_por', 'modificado_por',
                   'creado_el', 'modificado_el'}

    proveedor_id = forms.IntegerField(widget=forms.HiddenInput())
    proveedor_nombre = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control',
                                                                     'readonly': 'readonly'}))
    fecha = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control ', 'type':'date',
                                                          'size': '7px'}))
    validez = forms.TypedChoiceField(choices=VALIDEZ, required=True,
                                     widget=forms.Select(attrs={'class': 'form-control'}))
    forma_pago = forms.TypedChoiceField(choices=FORMA_PAGO, required=True,
                                        widget=forms.Select(attrs={'class': 'form-control'}))
    disponibilidad = forms.TypedChoiceField(choices=DISPONIBILIDAD, required=True,
                                            widget=forms.Select(attrs={'class': 'form-control'}))


class PresupuestoCompraCustomDetailForm(BaseFormSet):
    def clean(self):
        """
        Adds validation to check that no two links have the same anchor or URL
        and that all links have both an anchor and URL.
        """
        if any(self.errors):
            return


class RechazoPreOrden(forms.Form):
    motivo = forms.CharField(widget=forms.Textarea, required=True)



class OpcionesAnularOrdenForm(forms.Form):
    
    observacion1 = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'style': 'width: 500px;height: 100px;', 'placeholder':'Agregar Motivo.......'}),
        required=False, label="Observacion Anulada:")



class Banco_Form(forms.ModelForm):
    class Meta: 
        model = Bancos
        exclude = {'estado', 'usuario_modificacion', 'fecha_modificacion', 'usuario_insercion', 'fecha_insercion'}
        # fields = {'descripcion', 'fecha_cierre', 'ruc'}
        widgets = {
            'descripcion' : forms.TextInput(attrs={'class': 'form-control'}),
            'fecha_cierre' : forms.TextInput(attrs={'class': 'form-control', 'type':'date'}),
            'ruc' : forms.TextInput(attrs={'class': 'form-control'}),

        }



class FiltrolistadoCompras(forms.Form):
    desde = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'date'}), required=False)
    hasta = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'date'}), required=False)


class FacturasRecibidasConceptosEditForm(forms.Form):
    factura = forms.CharField(widget=forms.HiddenInput())
    item = forms.CharField(widget=forms.HiddenInput(), required=False)
    item_producto = forms.CharField(widget=forms.HiddenInput())
    orden = forms.CharField(widget=forms.HiddenInput())
    cantidad = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control auto', 'style': 'width: 90px;', 'onkeyup': 'subtotal(this);sumar_totales();'}), )
    monto = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control auto total desa'}), )
    monto_unitario = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control auto desa'}), )
    monto_impuesto = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control auto desa', 'style': 'width: 130px;'}), )
    porcentaje_impuesto = forms.MultipleChoiceField(
        widget=forms.Select(attrs={'class': 'form-control impuestos', 'style': 'width: 80px;'}))
    item_nombre = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control desa'}), required=False)
    orden_nombre = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control desa', 'style': 'width: 70px;'}), required=False)
    cantidad_maxima = forms.CharField(widget=forms.HiddenInput(), required=False)

    def __init__(self, *args, **kwargs):
        super(FacturasRecibidasConceptosEditForm, self).__init__(*args, **kwargs)
        self.fields["porcentaje_impuesto"] = forms.ChoiceField(
            widget=forms.Select(attrs={'class': 'form-control impuestos', 'style': 'width: 80px;'}),
            choices=[(0, "0"), (5, "5"), (10, "10")])


class PresupuestoCustomDetailForm(BaseFormSet):
    def clean(self):
        """
        Adds validation to check that no two links have the same anchor or URL
        and that all links have both an anchor and URL.
        """
        if any(self.errors):
            return


class FacturasDetalleEditForm(forms.Form):
    factura = forms.CharField(widget=forms.HiddenInput())
    cuota = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control desa', 'style': 'width: 90px;'}), )
    fecha_vencimiento = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control fecha', 'style': 'width: 130px;'}), )
    monto = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control auto monto_cuota', 'style': 'width: 130px;', 'onkeyup': 'saldo(this);'}), )
    saldo = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control auto desa saldo', 'style': 'width: 130px;'}), )
    observacion = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'style': 'width: 130px;'}),
                                  required=False)
    

class FacturasRecibidasConceptosForm(forms.ModelForm):
    class Meta:
        model = FacturasProveedorConceptos
        exclude = {'fecha_insercion', 'fecha_modificacion', 'usuario_insercion', 'usuario_modificacion', 'item',
                   'factura'}
        widgets = {
                   'item_producto': forms.HiddenInput(),
                   'orden': forms.HiddenInput(),
                   'cantidad': forms.TextInput(attrs={'class': 'form-control auto', 'style': 'width: 90px;',
                                                      'onkeyup': 'subtotal(this);sumar_totales();'}),
                   'monto': forms.TextInput(attrs={'class': 'form-control auto total desa'}),
                   'monto_unitario': forms.TextInput(attrs={'class': 'form-control auto desa'}),
                   'porcentaje_impuesto': forms.Select(
                       attrs={'class': 'form-control impuestos', 'style': 'width: 80px;'}),
                   'monto_impuesto': forms.TextInput(
                       attrs={'class': 'form-control auto desa', 'style': 'width: 130px;'}),
                   # 'porcentaje_impuesto': forms.Select(attrs={'class': 'form-control', 'style': 'width: 80px;', }), #'onchange': 'impuesto(this);'
                   }

    # monto_impuesto = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control desa', 'style': 'width: 130px;'}))
    item_nombre = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control desa'}), required=False)
    orden_nombre = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control desa', 'style': 'width: 70px;'}), required=False)
    cantidad_maxima = forms.CharField(widget=forms.HiddenInput())



class FacturasDetalleForm(forms.ModelForm):
    class Meta:
        model = FacturasProveedorDetalle
        exclude = {'fecha_insercion', 'fecha_modificacion', 'usuario_insercion', 'usuario_modificacion', 'factura'}
        widgets = {'cuota': forms.TextInput(attrs={'class': 'form-control desa', 'style': 'width: 90px;'}),
                   'fecha_vencimiento': forms.TextInput(attrs={'class': 'form-control', 'style': 'width: 130px;', 'type':'date'}),
                   'monto': forms.TextInput(attrs={'class': 'form-control auto monto_cuota', 'style': 'width: 130px;',
                                                   'onkeyup': 'saldo(this);'}),
                   'saldo': forms.TextInput(attrs={'class': 'form-control auto desa saldo', 'style': 'width: 130px;'}),
                   'observacion': forms.TextInput(attrs={'class': 'form-control', 'style': 'width: 130px;'}),
                   }



class FacturasRecibidasForm(forms.ModelForm):
    cantidad_cuotas = forms.IntegerField(
        widget=forms.NumberInput(attrs={'class': 'form-control', 'style': 'width: 126px;', 'min': '1', }), initial=1)
    dias_entre_cuotas = forms.IntegerField(
        widget=forms.NumberInput(attrs={'class': 'form-control', 'style': 'width: 126px;', 'min': '1', }))
    busca_oc = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'style': 'width: 126px;'}),
                               required=False)
    saldo_fake = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control totales desa', 'style': 'width: 126px;'}), )

    class Meta:
        model = FacturasRecibidasProveedor
        exclude = {'reposicion', 'responsable', 'recibido_docfisico', 'asiento_generado', 'numero_presupuesto',
                   'fecha_presupuesto',
                   'proceso', 'factura', 'saldo', 'departamento', 'fecha_recepcion', 'usuario_recepion',
                   'imagen',
                   'forma_pago', 'idsegsifen', 'id_timbrado', 'path_sinfirmar', 'path_firmado', 'banco',
                   'numero_documento', 'factor_cambio'}
        widgets = {
                   'moneda': forms.Select(attrs={'class': 'form-control', 'style': 'width: 160px;',
                                                 'onchange': 'factor_cambio(this.value);'}),
                   'forma_pago': forms.Select(attrs={'class': 'form-control', 'style': 'width: 160px;'}),
                   'tipo_de_factura': forms.Select(attrs={'class': 'form-control', 'style': 'width: 160px;',
                                                              'onchange': 'select_tipo_factura(this);'}),
                   'formato_comprobante': forms.Select(attrs={'class': 'form-control', 'style': 'width: 160px;',
                                                              'onchange': 'select_comprobante(this);'}),
                #    'factor_cambio': forms.TextInput(attrs={'class': 'cambio form-control', 'style': 'width: 126px;'}),
                   'fecha': forms.TextInput(attrs={'class': 'form-control fecha', 'style': 'width: 126px;', 'type':'date'}),
                   'proveedor':autocomplete.ModelSelect2(url='ProveedorComprasAutocomplete',attrs={
                    'data-placeholder': 'Nombre de la proveedor ...',}),
                   'factura_fisica': forms.TextInput(attrs={'class': 'form-control'}),
                   'nrotimbrado': forms.TextInput(attrs={'class': 'form-control', 'style': 'width: 126px;', 'onchange':'nro_timbradoadd(this);'}),
                   'nrotimbrado_vencimiento': forms.TextInput(
                       attrs={'class': 'form-control fecha_2', 'style': 'width: 126px;', 'type':'date'}),
                   'comentario': Textarea(attrs={'class': 'form-control ', 'cols': 20, 'rows': 5,
                                                 'placeholder': 'Max. 250 caracteres...'}),
                   'monto_total5': forms.TextInput(
                       attrs={'class': 'form-control totales desa', 'style': 'width: 126px;'}),
                   'monto_gravado5': forms.TextInput(
                       attrs={'class': 'form-control totales desa', 'style': 'width: 126px;'}),
                   'monto_impuesto5': forms.TextInput(
                       attrs={'class': 'form-control totales desa', 'style': 'width: 126px;'}),
                   'monto_total10': forms.TextInput(
                       attrs={'class': 'form-control totales desa', 'style': 'width: 126px;'}),
                   'monto_gravado': forms.TextInput(
                       attrs={'class': 'form-control totales desa', 'style': 'width: 126px;'}),
                   'monto_impuesto': forms.TextInput(
                       attrs={'class': 'form-control totales desa', 'style': 'width: 126px;'}),
                   'monto_exento': forms.TextInput(
                       attrs={'class': 'form-control totales desa', 'style': 'width: 126px;'}),
                   'orden': forms.HiddenInput(),
                   'fecha_insercion': forms.HiddenInput(),
                   'fecha_modificacion': forms.HiddenInput(),
                   'usuario_insercion': forms.HiddenInput(),
                   'usuario_modificacion': forms.HiddenInput(),
                   }



    def __init__(self, *args, **kwargs):
        super(FacturasRecibidasForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance and hasattr(instance, 'factura'):
            if not instance.factura == None:
                self.fields['cantidad_cuotas'].initial = len(
                    FacturasProveedorDetalle.objects.filter(factura=self.instance.factura))
                self.fields['dias_entre_cuotas'].initial = 0
                self.fields['saldo_fake'].initial = instance.saldo



class CuentasPagarForm(forms.ModelForm):
    class Meta: 
        model = CuentasaPagar
        # fields = {'usuario', 'rol'}
        exclude = {'creado_por', 'modificado_por', 'creado_el', 'modificado_el','fecha','total_mes'}
    # field_order = ['ciudad', 'distrito']
        widgets = {
                'anho':  forms.TextInput(attrs={'class': 'form-control', 'type':'year'}),
                # 'fecha':  forms.TextInput(attrs={'class': 'form-control', 'type':'date'}),
                'mes':  forms.TextInput(attrs={'class': 'form-control', 'type':'month'}),

                }

    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)     
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class':'form-control'
            })


class LibroEgresosForm(forms.ModelForm):
    class Meta: 
        model = LibroIvaEgresos
        # fields = {'usuario', 'rol'}
        exclude = {'creado_por', 'modificado_por', 'creado_el', 'modificado_el'}
    # field_order = ['ciudad', 'distrito']
        widgets = {
                'ejercicio_fiscal':  forms.TextInput(attrs={'class': 'form-control', 'type':'number', 'min':'2022'}),
                'mes':  forms.TextInput(attrs={'class': 'form-control', 'type':'month'}),
                }

    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)     
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class':'form-control'
            })

class NotaCreditoForm(forms.ModelForm):
    
    class Meta: 
        model = NotaCreditoProveedor
        exclude = {'creado_por', 'modificado_por', 'creado_el', 'modificado_el', 'recibido_de'}
        widgets = {
                'proveedor':autocomplete.ModelSelect2(url='ProveedorComprasAutocomplete',attrs={
                    'data-placeholder': 'Nombre de la proveedor ...',}),
                'fecha':  forms.TextInput(attrs={'class': 'form-control', 'reandonly':'reandonly'}),
                'numero_credito_fisico':  forms.TextInput(attrs={'class': 'form-control', 'type':'number'}),
                'total':  forms.TextInput(attrs={'class': 'form-control total', 'reandonly':'reandonly'}),

                }  

class NotaCreditoFormdet(forms.ModelForm):
    class Meta: 
        model = NotaCreditoProveedordet
        exclude = {'creado_por', 'modificado_por', 'creado_el', 'modificado_el'}
        widgets = {
                'orden':  forms.TextInput(attrs={'class': 'form-control', 'reandonly':'reandonly'}),
                'item':  forms.TextInput(attrs={'class': 'form-control', 'reandonly':'reandonly'}),
                'item_num':  forms.TextInput(attrs={'class': 'form-control', 'reandonly':'reandonly'}),
                'cantidad':  forms.TextInput(attrs={'class': 'form-control', 'reandonly':'reandonly'}),
                'monto_unitario':  forms.TextInput(attrs={'class': 'form-control', 'reandonly':'reandonly'}),
                'total':  forms.TextInput(attrs={'class': 'form-control total', 'reandonly':'reandonly'}),
                }


class NotadebitoForm(forms.ModelForm):
    
    class Meta: 
        model = NotaDebitoProveedor
        # fields = {'usuario', 'rol'}
        exclude = {'creado_por', 'modificado_por', 'creado_el', 'modificado_el', 'recibido_de'}
    # field_order = ['ciudad', 'distrito']
        widgets = {
                'proveedor':autocomplete.ModelSelect2(url='ProveedorComprasAutocomplete',attrs={
                    'data-placeholder': 'Nombre de la proveedor ...',}),
                'fecha':  forms.TextInput(attrs={'class': 'form-control', 'reandonly':'reandonly'}),
                'numero_debito_fisico':  forms.TextInput(attrs={'class': 'form-control', 'type':'number'}),
                # 'recibido_de':  forms.TextInput(attrs={'class': 'form-control'}),
                'total':  forms.TextInput(attrs={'class': 'form-control total', 'reandonly':'reandonly'}),

                }


class NotaDebitoFormdet(forms.ModelForm):
    class Meta: 
        model = NotaDebitoProveedordet
        # fields = {'usuario', 'rol'}
        exclude = {'creado_por', 'modificado_por', 'creado_el', 'modificado_el'}
    # field_order = ['ciudad', 'distrito']
        widgets = {
                'orden':  forms.TextInput(attrs={'class': 'form-control', 'reandonly':'reandonly'}),
                'item':  forms.TextInput(attrs={'class': 'form-control', 'reandonly':'reandonly'}),
                'item_num':  forms.TextInput(attrs={'class': 'form-control', 'reandonly':'reandonly'}),
                'cantidad':  forms.TextInput(attrs={'class': 'form-control', 'reandonly':'reandonly'}),
                'monto_unitario':  forms.TextInput(attrs={'class': 'form-control', 'reandonly':'reandonly'}),
                'total':  forms.TextInput(attrs={'class': 'form-control total', 'reandonly':'reandonly'}),
                }